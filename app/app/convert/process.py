import logging
import os

from deprecated import deprecated
from flask import url_for

from app import database, util, file_system
from app.convert import imager, interface, latex, markdown


def process_v2(request: interface.ArticleFetchRequest):
    '''
    Main processing pipeline.
    - Set article status to RENDERING (optional)
    - Process latex sourcecode
        - generate .tex file
        - generate .bib file if needed
    - Convert latex to markdown
    - Postprocess markdown
    - Upload to storage (optional)
    - Prepare the html file with images, optionally async call image processor
    :param request:
    :return: Nothing. Actual html file will be returned by renderer
    '''
    article_name = request.article_name
    if request.use_database:
        database.ArticleStatusQuery(article_name).set(database.ArticleStatus.RENDERING)
    meta = interface.ArticleMeta.read(article_name)
    if not meta.main_files or not meta.tex_files:
        raise interface.NotInLatexError('https://arxiv.org/pdf/%s.pdf' % article_name)
    if util.FLAGS_USE_PROCESS_V2:
        process_latex_v2(meta)
    else:
        process_latex(meta)
    process_markdown(meta)
    file_system.upload_all(article_name, extensions=[".md", ".html", ".txt", ".bib"])
    if not request.use_database:  # Force single session processing
        if request.no_image_requests:
            return
        post_process(article_name)
    elif request.preview_mode:
        util.insomniac_call(url_for("convert_blueprint.post_process", article_name=article_name, _external=True))


def process_pdf(request: interface.ArticleFetchRequest):
    pass


def post_process(article_name):
    '''
    Invoked by insomniac_call through '/helper/post_process/<article_name>'
    :param article_name:
    :return:
    '''
    # Files may be missing in production at this stage
    meta = interface.ArticleMeta.read(article_name)
    job_list = []
    job_list.extend(create_direct_image_convert_jobs(meta))
    job_list.extend(create_latex_image_convert_jobs(meta))

    # recover .sty files
    article_folder = util.get_path(article_name)
    '''Pre-processing files, generating meta file'''
    for (path, _, files) in os.walk(article_folder):
        for file in files:
            if file.endswith('.sty.bak'):
                filepath = os.path.join(path, file)
                os.rename(filepath, filepath[:-4])
    imager.ImageConvertManager(job_list).convert()
    for (path, _, files) in os.walk(article_folder):
        for file in files:
            if file.endswith('.sty'):
                filepath = os.path.join(path, file)
                os.rename(filepath, filepath + '.bak')
    file_system.upload_all(article_name, extensions=[".png", ".log"])
    if database.ArticleStatusQuery(article_name).get()['status'] == database.ArticleStatus.RENDERING.name:
        database.ArticleStatusQuery(article_name).set(database.ArticleStatus.SUCCESS)

@deprecated
def process_latex(meta):
    '''
    Process all .tex, .bib and .bbl files. bibliography text within the tex files and bbl files will be combined
    into a generated bib file.
    :param meta:
    :return:
    '''
    latex_processor = latex.LatexProcessor(meta)

    if len(meta.bbl_files) == 0:
        logging.info("No bbl file found.")
    else:
        logging.info("Found %s bbl files." % len(meta.bbl_files))
        for bbl_file in meta.bbl_files:
            latex_processor.process_bbl_file(bbl_file)

    for tex_file in meta.tex_files:
        filepath = os.path.join(meta.article_folder, tex_file)
        latex_processor.process(filepath)

    if latex_processor.has_bbl_text_or_file():
        latex_processor.generate_bib_file()

    latex_processor.generate_image_convert_requests()


def process_latex_v2(meta):
    """
    Combine all tex files together first and process one single file
    :param meta:
    :return:
    """
    latex_processor = latex.LatexProcessorV2(meta)

    if len(meta.bbl_files) == 0:
        logging.info("No bbl file found.")
    else:
        logging.info("Found %s bbl files." % len(meta.bbl_files))
        for bbl_file in meta.bbl_files:
            latex_processor.process_bbl_file(bbl_file)

    latex_processor.process_meta()

    if latex_processor.has_bbl_text_or_file():
        latex_processor.generate_bib_file()

    latex_processor.generate_image_convert_requests()

def process_markdown(meta):
    article_name = meta.article_name
    markdown.convert_to_md(article_name)
    markdown.clean_md_file(article_name)


def create_direct_image_convert_jobs(meta):
    image_jobs = []
    for filename in meta.image_files:
        image_jobs.append(
            imager.ImageConvertJob(type=imager.ImageCovertJobType.DIRECT,
                                   info=os.path.join(util.get_path(meta.article_name), filename)
                                   )
        )
    return image_jobs


def create_latex_image_convert_jobs(meta):
    image_jobs = []
    for relative_filepath in meta.latex_image_requests:
        path, filename = os.path.split(os.path.join(meta.article_folder, relative_filepath))
        image_jobs.append(
            imager.ImageConvertJob(type=imager.ImageCovertJobType.LATEX,
                                   info={
                                       'path': path,
                                       'filename': filename
                                   },
                                   output_path=os.path.join(meta.article_folder, util.LATEX_GENERATED_IMAGE_FOLDER)
                                   )
        )
    return image_jobs
