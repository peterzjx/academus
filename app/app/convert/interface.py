import inspect
import json
import logging
import os
from enum import Enum
from functools import wraps
from dataclasses import dataclass

from app import util


def initializer(func):
    """
    Automatically assigns the parameters.
     class process:
    ...     @initializer
    ...     def __init__(self, cmd, reachable=False, user='root'):
    ...         pass
     p = process('halt', True)
     p.cmd, p.reachable, p.user
    ('halt', True, 'root')
    """
    argspec = inspect.getfullargspec(func)

    @wraps(func)
    def wrapper(self, *args, **kargs):
        for name, arg in list(zip(argspec.args[1:], args)) + list(kargs.items()):
            setattr(self, name, arg)

        for name, default in zip(reversed(argspec.args), reversed(argspec.defaults)):
            if not hasattr(self, name):
                setattr(self, name, default)

        func(self, *args, **kargs)

    return wrapper


@dataclass
class ArticleFetchRequest:
    article_name: str  # article name, currently arXiv article id
    article_source: str = "arxiv"
    use_database: bool = True  # If false, render article and image convert in a single session
    force_refresh: bool = False  # force re-download from original source website
    reset_local: bool = False  # reset local files to pre-convert condition and rerun conversion
    preview_mode: bool = True
    no_image_requests: bool = False
    process_pdf: bool = False  # Bypass the convert module
    external_metadata: bool = True  # TODO:query external page to obtain e.g. article title or abstract
    evaluation_mode: bool = False  # TODO:run evaluation after the page is rendered
    test_mode: bool = False  # integration test mode

@dataclass
class ImageConvertLatexSnippet:
    id: str
    content: str
    path: str
    preamble: str

@dataclass
class ArticleMeta:
    article_name: str
    article_folder: str
    main_files: list
    tex_files: list
    image_files: list
    meta_version: str
    bib_files: list
    bbl_files: list
    title: str
    authors: list
    update_time: str
    subject: dict
    latex_image_requests: str

    @initializer
    def __init__(self,
                 article_name="",
                 article_folder="",
                 main_files=(),
                 tex_files=(),
                 image_files=(),
                 meta_version="",
                 bib_files=(),
                 bbl_files=(),
                 title="",
                 authors=(),
                 update_time="",
                 subject={},
                 latex_image_requests=(),
                 ):
        '''
        Define Member variables here.
        :param article_name: str, arxiv id
        :param article_folder: str, relative path to current python file
        :param main_files: tex files with documentclass header
        :param tex_files: all .tex files
        :param image_files: all original images files
        :param meta_version: datetime string as YYYYmmDDHHMMSS
        :param bib_files: all original .bib files and those generated from .bbl files
        :param bbl_files: all original .bbl files and those extracted from .tex files
        :param title: string, read from arxiv article page
        :param authors: list of string, read from arxiv article page
        :param update_time: datetime string, last version updated on arxiv article page
        :param subject: {'primary_subject': string, 'secondary_subject': [list_of_strings]}, read from arxiv article
                 page
        :param latex_image_requests: [filename].rqs where the generated pdf file should be named [filename].pdf
        '''
        pass

    def update(self, args):
        for key in args:
            if hasattr(self, key):
                setattr(self, key, args[key])
            else:
                logging.error("ArticleMeta updates with unknown attribute %s" % key)
        return self

    def store(self):
        meta_filename = os.path.join(self.article_folder, util.META_FILE_NAME)
        with open(meta_filename, 'w') as outfile:
            json.dump(self.__dict__, outfile, sort_keys=True, indent=4)

    @staticmethod
    def read(article_name):
        article_folder = util.get_path(article_name)

        meta_file = os.path.join(article_folder, util.META_FILE_NAME)
        if not os.path.exists(meta_file):
            raise CacheExpiredError()
        with open(meta_file, 'r') as outfile:
            data = json.load(outfile)
        if "meta_version" not in data or data['meta_version'] < util.FIRST_SUPPORTED_META_VERSION:
            raise CacheExpiredError()
        return ArticleMeta().update(data)


class ArticleError(Exception):
    def __init__(self, url=""):
        self.url = url

    def __str__(self):
        return "We have met an error. You may view the <a href='%s'>original pdf</a> on arXiv." % self.url


class NotInLatexError(ArticleError):
    def __str__(self):
        return "The article is either not written in LaTeX, or does not have an available source code. You may view " \
               "the <a href='%s'>original pdf</a> on arXiv." % self.url


class RenderError(ArticleError):
    def __str__(self):
        return "We could not render the article. There might be compiling errors or unrecognizable LaTeX commands.<br>" \
               "You may view the <a href='%s'>original pdf</a> on arXiv." % self.url


class CacheExpiredError(ArticleError):
    def __str__(self):
        return "Cache of the article is expired."


class ArticleNotFoundError(ArticleError):
    def __str__(self):
        return "We can't find the article. Did you type the correct article id? You may try the <a href='%s'>original pdf</a> on arXiv." % self.url


class ArticleConnectionError(ArticleError):
    def __str__(self):
        return "There is a connection error. Please try again later."


class FavouriteDatabaseStatus(Enum):
    SUCCESS = 0
    NOT_FOUND = 1
    ALREADY_EXISTED = 2
    OVERFLEW = 3
