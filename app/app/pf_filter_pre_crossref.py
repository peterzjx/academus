import panflute as pf


def prepare(doc):
    doc.all_refs = set()


def reattach_subfigure_identifier(elem, doc):
    #  Subfigure is represented as the following in pandoc 3.x
    #     [ Div
    #         ( "sublabel1" , [ "figure" ] , [] )
    #         [ Para
    #             [ Image
    #                 ( "" , [] , [ ( "width" , "24%" ) ] )
    #                 []
    #                 ( "figures/pretraing_all_weights" , "" )
    #             ]
    #         , Div
    #             ( "" , [ "caption" ] , [] )
    #             [ Para [ Str "sub" , Space , Str "1" ] ]
    #         ]
    #
    # For crossref to work, the Image inside Para should have the label same as the outside Div
    if isinstance(elem, pf.Div) and "figure" in elem.classes and isinstance(elem.parent,
                                                                            pf.Div) and "figure" in elem.parent.classes:
        id = elem.identifier
        for c in elem.content:
            if isinstance(c, pf.Para):
                for d in c.content:
                    if isinstance(d, pf.Image):  # Div(Figure) > Div(Figure) > Para > Image
                        if d.identifier == "":
                            d.identifier = id


def multicell_table(elem, doc):
    def extract_multicell_info(elem, doc):
        nonlocal tag, num
        if isinstance(elem, pf.Str) and elem.text.startswith("<multicolumn,"):
            pos = elem.text.find(">")
            tagstr = elem.text[:pos].strip("<").strip(">")
            elem.text = elem.text[pos + 1:]
            tag, num = tagstr.split(",")
            num = int(num)

    if isinstance(elem, pf.TableCell):
        tag = ""
        num = 0
        elem = elem.walk(extract_multicell_info)
        if tag != "":
            elem.colspan = num
            cur = elem
            for i in range(num - 1):
                cur = cur.next
                cur.colspan = 0

        return elem

def remove_empty_cell(elem, doc):
    if isinstance(elem, pf.TableCell):
        if elem.colspan == 0:
            return []
    return elem


def main(doc=None):
    return pf.run_filters([
        reattach_subfigure_identifier,
        multicell_table,
        remove_empty_cell,
    ], prepare=prepare, doc=doc)


if __name__ == '__main__':
    main()
