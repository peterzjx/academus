import unittest
from app import url

class UrlTest(unittest.TestCase):
    def test_arxiv_id_pattern(self):
        self.assertTrue(url.ARXIV_ID_RE.match("2001.00001"))
        self.assertTrue(url.ARXIV_ID_RE.match("hep-ex/0505037v1"))
        self.assertTrue(url.ARXIV_ID_RE.match("cond-mat/0403531"))

        self.assertFalse(url.ARXIV_ID_RE.match("0403531"))

if __name__ == '__main__':
    unittest.main()