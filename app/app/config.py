import os

from app.util import PRODUCTION


class FlaskConfig(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'just-a-secret-key'
    PRODUCTION = PRODUCTION
    MONGO_URI = "mongodb+srv://peterzjx:peterzjxtest@cluster0-qpx2c.gcp.mongodb.net/academus?retryWrites=true&w=majority" if PRODUCTION else "mongodb://localhost:27017/AcademusDatabase"
    MONGO_DBNAME = "AcademusDatabase"
