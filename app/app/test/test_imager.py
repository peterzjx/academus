import unittest
from app.convert import imager


class TikzImagerTest(unittest.TestCase):
    def generate_image_job_from_tex(self, tex, id):
        return imager.ImageConvertJob(type=imager.ImageCovertJobType.TIKZ,
                                      info=tex,
                                      output_path='/tmp/%s.png' % id
                                      )

    def test_tikz_imager_0(self):
        tex = r'''
\begin{tikzpicture}
\tikzstyle{process}=[draw,rectangle, rounded corners, fill=yellow!20, minimum width=3cm, minimum height=1cm]
\tikzstyle{decision}=[draw, diamond, fill=red!20, minimum width=4cm, aspect=2]
\tikzstyle{a}=[thick,->,>=stealth]

\node[process] (waking) at (0,0) {Wake up};

\node[decision] (day) at (0,-3) {Is it a weekday?};

\node[process] (up) at (0,-6) {Get out of bed};
\node[process] (sleep) at (5,-3) {Go back to sleep};

\draw[a] (waking) -- (day);
\draw[a] (day) -- node[anchor=east] {Yes} (up);
\draw[a] (day) --node[anchor=south] {No}  (sleep);
\draw[a] (sleep) |- (waking);

\end{tikzpicture}
'''
        image_jobs = [
            self.generate_image_job_from_tex(tex, 0)
        ]
        imager.ImageConvertManager(image_jobs).convert()

    def test_tikz_imager_1(self):
        tex = r'''
\begin{minipage}[c]{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[scale=0.75]
    \begin{axis}[
        xlabel=$x$,
        ylabel=$y$
        ]
        \addplot {x^2};
    \end{axis}
\end{tikzpicture}
\end{center}
\end{minipage}
\begin{minipage}[c]{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[scale=0.75]
\pgfplotstableread[row sep=\\,col sep=&]{
    Person & Cats & Dogs \\
    Mira & 10 & 7 \\
    Jasleen & 5 & 0 \\
    Nick & 3 & 2 \\
    }\mydata
    \begin{axis}[
            ybar,
            symbolic x coords={Mira, Jasleen, Nick},
            xtick=data,
            %bar width = 1 cm,
            x = 1.5 cm,
            enlarge x limits=0.5,
            ymin = 0,
        ]
        \addplot table[x=Person,y=Cats]{\mydata};
        \addplot table[x=Person,y=Dogs]{\mydata};
        \legend{Cats,Dogs}
    \end{axis}



  \end{tikzpicture}
\end{center}
\end{minipage}
    '''
        image_jobs = [
            self.generate_image_job_from_tex(tex, 1)
        ]
        imager.ImageConvertManager(image_jobs).convert()


if __name__ == '__main__':
    unittest.main()
