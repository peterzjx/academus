from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, HiddenField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email
from markupsafe import Markup

class LoginForm(FlaskForm):
    email = EmailField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RenameCollectionForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    current_collection_id = HiddenField('current_collection_id', validators=[DataRequired()])
    submit = SubmitField(Markup('<i class="fa fa-caret-right" aria-hidden="true"></i>'))