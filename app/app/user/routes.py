from flask import Blueprint
from flask import render_template, flash, redirect, url_for
from flask_login import current_user, login_user, logout_user, login_required

from app import util
from app.user.favorites import format_article_display
from app.user.forms import LoginForm, RenameCollectionForm
from app.db_model import User, Article

user_blueprint = Blueprint('user_blueprint', __name__)


@user_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('convert_blueprint.index_page'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.find_one(email=form.email.data)
        if user is None or not user.check_password(form.password.data):
            flash('Invalid email or password')
            return redirect(url_for('user_blueprint.login'))
        login_user(user, remember=form.remember_me.data)
        flash('Welcome, {}.'.format(current_user['username']))
        return redirect(url_for('convert_blueprint.index_page'))
    return render_template('login.html', title='Sign In', form=form)


@user_blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('convert_blueprint.index_page'))


# Collection management
@user_blueprint.route('/dashboard')
@login_required
def dashboard():
    form = RenameCollectionForm()
    collections = current_user.get_all_collections()  # ensures at least one collection for current user
    display_collections = {}
    for collection in collections:
        articles = []
        for pointer in collection['articles']:
            article = Article.get_from_id(pointer['id'])
            article_display = format_article_display(article, pointer['time_added'])
            articles.append(article_display)
        display_name = "Read Later" if collection['is_default'] else collection['name']
        if len(articles):
            display_name += " (%s)" % len(articles)
        display_collections[collection.get_string_id()] = {'name': display_name,
                                                           'is_default': collection['is_default'],
                                                           'articles': articles}
    return render_template('dashboard.html', title_text="Dashboard" + util.TITLE_SUFFIX,
                           collections=display_collections, form=form)


@user_blueprint.route('/favorites/add/<article_name>')
@login_required
def add_article_to_default_collection(article_name):
    article = Article.find_one(article_name=article_name)
    if not article:
        raise FileNotFoundError("Missing article in database")
    collection = current_user.get_default_collection()
    if collection.add_article(article):
        flash('Added {} to Read Later.'.format(article_name))
    else:
        flash('{} already in Read Later.'.format(article_name))
    return redirect(url_for('convert_blueprint.article_v2', article_name=article_name))


@user_blueprint.route('/favorites/remove/<article_name>/<collection_id>')
@login_required
def remove_from_collection(article_name, collection_id):
    article = Article.find_one(article_name=article_name)
    if not article:
        raise FileNotFoundError("Missing article in database")
    collection = current_user.get_collection(collection_id)
    if not collection:
        flash('Cannot find collection')
    if not collection.remove_article(article):
        flash('Cannot find {} in collection'.format(article_name))
    return redirect(url_for('user_blueprint.dashboard'))


@user_blueprint.route('/favorites/move/<article_name>/<from_id>/<to_id>')
@login_required
def move_from_collection(article_name, from_id, to_id):
    article = Article.find_one(article_name=article_name)
    if not article:
        raise FileNotFoundError("Missing article in database")
    if from_id != to_id:
        collection_from = current_user.get_collection(from_id)
        collection_to = current_user.get_collection(to_id)
        if not collection_from or not collection_to:
            flash('Cannot find collection')
        if not collection_from.remove_article(article):
            flash('Cannot find {} in collection'.format(article_name))
        if not collection_to.add_article(article):
            flash('Error in adding article')
    return redirect(url_for('user_blueprint.dashboard'))


# method: post
# def remove_from_collection()

@user_blueprint.route('/favorites/create_collection')
@login_required
def create_collection():
    current_user.create_collection("New collection")
    return redirect(url_for('user_blueprint.dashboard'))


@user_blueprint.route('/favorites/delete_collection/<collection_id>')
@login_required
def delete_collection(collection_id):
    current_user.remove_collection(collection_id)
    return redirect(url_for('user_blueprint.dashboard'))


@user_blueprint.route('/favorites/rename_collection', methods=['POST'])
@login_required
def rename_collection():
    form = RenameCollectionForm()
    if form.validate_on_submit():
        current_user.rename_collection(string_id=form.current_collection_id.data,
                                       new_name=form.name.data)
    return redirect(url_for('user_blueprint.dashboard'))
