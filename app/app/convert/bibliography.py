import codecs
import logging
import os
import re

from TexSoup import TexSoup, data

from app import util
from app.convert import latex
from app.convert.bib_filter import BibClass

maintain = ["[tex]", "emph", "textbf"]
takefirst = ["citenamefont", "bibfnamefont", "bibnamefont", "url", "urlprefix", "Eprint", "translation", "href"]
takesecond = ["bibinfo", "bibfield", "hrefatnoop"]
ignore = ["newblock", "BibitemOpen", "selectlanguage", "bibitemStop", "bibitemNoStop", "BibitemShut", "EOS"]

debug = lambda *args: None
# debug = print

def iterate_node(node):
    # TODO: handle TexSoup consume extra Args, not only for bibitem but in general.
    if isinstance(node, data.TexNode):
        debug("Node:", node.expr.name)
        if node.expr.name == 'bibitem':
            rargs = []
            for arg in node.args:
                if isinstance(arg, data.BraceGroup):
                    rargs.append(arg.string)
            if len(rargs) > 0:
                replaced = rargs[0]
                if len(rargs) > 1:
                    node.parent.insert(1, TexSoup(" ".join(['{%s}' % arg for arg in rargs[1:]])))
                latex.replace_texnode_with(node, replaced)
            return

        # depth first traverse of the grammar tree
        if node.children:
            for c in node.children:
                iterate_node(c)

        # process the root node
        if node.expr.name in maintain:
            pass
        elif node.expr.name == 'enquote':
            if len(node.args) >= 1:
                replaced_to = "``" + node.args[0].string + "''"
                debug("try replace", node, "to", replaced_to)
                latex.replace_texnode_with(node, replaced_to)
        elif node.expr.name in takefirst:
            if len(node.args) >= 1:
                replaced_to = node.args[0].string
                debug("try replace", node, "to", replaced_to)
                latex.replace_texnode_with(node, replaced_to)
            else:
                node.delete()
        elif node.expr.name in takesecond:
            if len(node.args) >= 2:
                replaced_to = node.args[1].string
                debug("try replace", node, "to", replaced_to)
                latex.replace_texnode_with(node, replaced_to)
            else:
                node.delete()
        elif node.expr.name in ignore:
            debug("delete", node.expr.name)
            node.delete()
        # TODO: process math


def bbl_file_to_bib_text(meta, bbl_file):
    try:
        logging.info("Loading bbl file %s" % bbl_file)
        input_filename = os.path.join(meta.article_folder, bbl_file)
        with codecs.open(input_filename, 'rU', encoding="utf-8", errors='ignore') as file:
            content = file.read()
        # TODO: url contains % will be wrongly cut off
        return bbl_text_to_bib_text(content)
    except Exception as e:
        logging.error(str(e))
        return ""


def bib_text_to_bib_file(meta, bib_text):
    logging.info("Generating %s" % util.GENERATED_BIB_FILE_NAME)
    output_filename = os.path.join(meta.article_folder, util.GENERATED_BIB_FILE_NAME)
    with open(output_filename, 'w') as f:
        f.write(bib_text)
    meta.bib_files.append(util.GENERATED_BIB_FILE_NAME)
    meta.store()


def filter_bibliography(meta):
    try:
        bib_text_buffer = []
        # bbl generated bib file will be appended at last. Process them first, so they appear first in
        # the final generated file and will possibly be overridden if it also appear in one of the original bib file.
        for bib_file in reversed(meta.bib_files):
            logging.info("Filtering bib file %s" % bib_file)
            input_filename = os.path.join(meta.article_folder, bib_file)
            with codecs.open(input_filename, 'rU', encoding="utf-8", errors='ignore') as file:
                content = file.read()
            content = BibClass(content).extract()
            bib_text_buffer.append(content)
        logging.info("Removing %d bib files from meta" % len(meta.bib_files))
        meta.bib_files = []
        bib_text_to_bib_file(meta, "\n".join(bib_text_buffer))
    except Exception as e:
        logging.error(str(e))


def bbl_text_to_bib_text(content):
    content = latex.LatexProcessorV2.remove_comments(content)
    content = content.replace("@noop", "atnoop")
    content = re.sub(r"\\\s", " ", content)
    content = re.sub(r"\\doibase\s", "http://dx.doi.org/", content)
    content = re.sub(r"{\\em\s", r"\\emph{", content)
    content = re.sub(r"{\\bf\s", r"\\textbf{", content)
    content = re.sub(r"\\providecommand.*?\n", "", content)
    content = re.sub(r"\\let.*?\n", "", content)
    content = content.replace("\\end{thebibliography}", "")
    if content.startswith('\\bibitem'):
        content = " " + content
    bibitems = re.split(r'\\bibitem', content)[1:]
    processed = []
    for item in bibitems:
        raw = '\\bibitem' + item
        try:
            soup = TexSoup(raw)
            debug("Original soup:", soup)
            iterate_node(soup)
            debug("Processed soup:", soup)
            soup_contents = list(soup.contents)
            body = "".join([str(x) for x in soup_contents[1:]])
            body = re.sub(r"\s+", " ", body, flags=re.MULTILINE).strip()
            bibitem = f'@article{{{soup_contents[0]},\n    note = {{{body}}}\n}}'
            processed.append(bibitem)
        except Exception as e:
            logging.error(str(e))
    return '\n'.join(processed)


if __name__ == '__main__':
    pass
