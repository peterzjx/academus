
-- Function to parse raw LaTeX blocks into Pandoc AST
function ParseLatexBlock(elem)
    if elem.tag == "RawBlock" and elem.format == "latex" then
        -- Using Pandoc's built-in latex parser to parse the raw LaTeX block
        local elem_text = elem.text:gsub("\\begin{DONOTPARSE}", "")
	elem_text = elem_text:gsub("\\end{DONOTPARSE}", "")
        local parsed_latex = pandoc.read(elem_text, "latex", { standalone = true })
        if parsed_latex then
            return parsed_latex.blocks
        else
            -- If parsing fails, return the original raw block
            return elem
        end
    end
end

return {
    {RawBlock = ParseLatexBlock}
}
