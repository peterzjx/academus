import unittest
from app.convert.latex import BracketMatcher, LatexProcessor, FilenameMatcher

class BracketMatcherTest(unittest.TestCase):
    def test_basic(self):
        self.assertEqual((1, 1), BracketMatcher("{}").match())
        self.assertEqual((1, 3), BracketMatcher("{{}}").match())
        self.assertEqual((5, 15), BracketMatcher("abcd{1234{abcd}}").match())

    def test_prefix(self):
        self.assertEqual((8, 8), BracketMatcher(r"\prefix{}").match(prefix=r'\prefix'))
        self.assertEqual((8, 10), BracketMatcher(r"\prefix{{}}").match(prefix=r'\prefix'))

    def test_unmatch(self):
        self.assertEqual((-1, -1), BracketMatcher(r"\prefix{}").match(prefix=r'\a'))
        self.assertEqual((-1, -1), BracketMatcher(r"\prefix{").match(prefix=r'\prefix'))

    def test_environment(self):
        content = r'\begin{tabular}\end{tabular}'
        self.assertEqual((15, 15), BracketMatcher(content).match(left=r'\begin{tabular}', right=r'\end{tabular}'))
        content = r'\begin{tabular}0\end{tabular}'
        self.assertEqual((15, 16), BracketMatcher(content).match(left=r'\begin{tabular}', right=r'\end{tabular}'))
        content = r'\begin{tabular}\begin{tabular}0\end{tabular}\end{tabular}'
        start, end = BracketMatcher(content).match(left=r'\begin{tabular}', right=r'\end{tabular}')
        self.assertEqual(r'\begin{tabular}0\end{tabular}', content[start:end])

    def test_substitution(self):
        content = r'Lorem ipsum{dolor {s}it} amet, consectetur ipsum{adipiscing elit}, sed do eiusmod tempor ipsum{}.'
        def _upper(matched_pattern, left, inner, right):
            return matched_pattern[::-1] + left + inner.upper() + right
        result = LatexProcessor.substitude_with_bracket_matching(r'ipsum', content, _upper)
        expected = r'Lorem muspi{DOLOR {S}IT} amet, consectetur muspi{ADIPISCING ELIT}, sed do eiusmod tempor muspi{}.'
        self.assertEqual(expected, result)


class SimpleTextReplaceTest(unittest.TestCase):
    def test_section_label(self):
        # Simple case
        self.assertEqual(r'\section{0} \label{sec:1}',
                         LatexProcessor(None).simple_text_replace(r'\section{0}\label{1}'))
        # Spaces between section and label
        self.assertEqual(r'\section{0} \label{sec:1}',
                         LatexProcessor(None).simple_text_replace(r'\section{0} \label{1}'))
        self.assertEqual(r'\section{0} \label{sec:1}',
                         LatexProcessor(None).simple_text_replace(r'\section{0}  \label{1}'))
        self.assertEqual(r'\section{0} \label{sec:1}',
                         LatexProcessor(None).simple_text_replace('\\section{0} \n \\label{1}'))
        # label inside section
        self.assertEqual(r'\section{0} \label{sec:1}',
                         LatexProcessor(None).simple_text_replace(r'\section{0\label{1}}'))
        self.assertEqual(r'\section{A{0}} \label{sec:1}',
                         LatexProcessor(None).simple_text_replace(r'\section{A{0}\label{1}}'))
        self.assertEqual(r'\section{A{0}{2}} \label{sec:1}',
                         LatexProcessor(None).simple_text_replace(r'\section{A{0}\label{1}{2}}'))

    def test_def(self):
        # TexSoup will incorrectly parse \def\command to \def{\}{command}
        original = r"\def\arraystretch{1.1}"
        self.assertEqual(original, LatexProcessor(None).simple_text_replace(original))


class MockMeta:
    def __init__(self, image_files):
        self.image_files = image_files


class FilenameMatcherTest(unittest.TestCase):
    def test_filename_matcher(self):
        mock_meta = MockMeta(['a.b', 'c.eps', 'D.JPG', 'e/f.png', 'g.png'])
        matcher = FilenameMatcher(mock_meta)
        self.assertEqual('a.b.png', matcher.search('a.b'))
        self.assertEqual('c.png', matcher.search('c.eps'))
        self.assertEqual('D.png', matcher.search('d.jpg'))
        self.assertEqual('e/f.png', matcher.search('e/f.png'))
        self.assertEqual('e/f.png', matcher.search('e/f'))
        self.assertEqual('e/f.png', matcher.search('f'))
        self.assertEqual('g.png', matcher.search('g'))

    def test_name_conflict(self):
        mock_meta = MockMeta(['a.b.png', 'e/a.b.png', 'c.eps', 'C.eps', 'e/c.eps'])
        matcher = FilenameMatcher(mock_meta)
        self.assertEqual('a.b.png', matcher.search('a.b'))
        self.assertEqual('e/a.b.png', matcher.search('e/a.b'))
        self.assertEqual('c.png', matcher.search('c.eps'))
        self.assertEqual('e/c.png', matcher.search('e/c.eps'))

if __name__ == '__main__':
    unittest.main()
