import panflute as pf

from app.util import FIG_PREFIX, TAB_PREFIX, EQ_PREFIX, SEC_PREFIX

def prepare(doc):
    doc.all_refs = set()

def remove_force_black_color(elem, doc):
    if isinstance(elem, pf.Span) and elem.attributes.get('style') == 'color: black':
        elem.attributes = {}  # TODO: check if bold or italic are affected

def collect_all_ref(elem, doc):
    if isinstance(elem, pf.Figure) or isinstance(elem, pf.Table):
        # identifier has been processed to contain the prefix
        doc.all_refs.add(elem.identifier)
    if isinstance(elem, pf.Header):
        # Need to update prefix of the header
        elem.identifier = SEC_PREFIX + elem.identifier
        doc.all_refs.add(elem.identifier)
    if isinstance(elem, pf.Str) and elem.text.startswith(" {#" + EQ_PREFIX):
        # math with identifier is represented by math following a Str in pandoc 3.1
        identifier = elem.text[3: -2]  # strip " {# ... }", could be {#eq: sublabel=eq:...}
        if identifier.startswith("eq: sublabel="):
            sublabel_string = identifier[len("eq: sublabel="):]
            doc.all_refs.update(sublabel_string.strip().split(','))
        else:
            doc.all_refs.add(identifier)
    return elem

def change_reference_style(elem, doc):
    if isinstance(elem, pf.Link):
        if 'reference' not in elem.attributes:
            return elem
        ref_id = elem.attributes['reference']
        if EQ_PREFIX + ref_id in doc.all_refs:
            s = EQ_PREFIX + ref_id
            return [pf.Cite(pf.Str("@%s" % s), citations=[pf.Citation(id="%s" % s, mode="AuthorInText")]), pf.SoftBreak]
            # without a trailing space \ref{A}B will be converted to @eq:AB which will not point to the correct ref.
            # Appending the first pf.Str with a " " has no effect.
        else:
            if FIG_PREFIX + ref_id in doc.all_refs:
                s = FIG_PREFIX + ref_id
            elif TAB_PREFIX + ref_id in doc.all_refs:
                s = TAB_PREFIX + ref_id
            elif SEC_PREFIX + ref_id in doc.all_refs:
                s = SEC_PREFIX + ref_id
            elif ref_id in doc.all_refs:
                s = ref_id
            else:
                return elem
                # For theorem and other format, do not change the format besides special char
                # TODO: handle actual numbering of environments
            ret = [pf.Cite(pf.Str("[-@%s]" % s), citations=[pf.Citation(id="%s" % s, mode="SuppressAuthor")])]
            # [-@] usually is not ambiguous, unless followed by () which can be interpreted as a link []()
            if isinstance(elem.next, pf.Str) and elem.next.text.startswith("("):
                ret.append(pf.SoftBreak)
            return ret


def main(doc=None):
    return pf.run_filters([
        remove_force_black_color,
        collect_all_ref,
        change_reference_style,
        ], prepare=prepare, doc=doc)


if __name__ == '__main__':
    main()
