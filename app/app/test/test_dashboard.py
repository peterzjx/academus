import unittest
from datetime import datetime, timedelta

from app.util import utc_time_to_timeago_string

class DashboardTest(unittest.TestCase):
    def test_timeago(self):
        base = datetime.strptime('2021/08/17', '%Y/%m/%d')

        t = base - timedelta(minutes=1)
        self.assertEqual(utc_time_to_timeago_string(t, base), "1 minute ago")

        t = base - timedelta(hours=1)
        self.assertEqual(utc_time_to_timeago_string(t, base), "1 hour ago")

        t = base - timedelta(days=1)
        self.assertEqual(utc_time_to_timeago_string(t, base), "1 day ago")

        t = base - timedelta(days=10)
        self.assertEqual(utc_time_to_timeago_string(t, base), "2021/08/07 00:00")

if __name__ == '__main__':
    unittest.main()