import logging
import os
import shutil
from datetime import datetime

from flask import Blueprint
from flask import redirect, request

from app import database, util
from app import file_system
from app import url
from app.convert import interface, process, render, spider, arxiv
from app.database import ArticleStatusQuery, ArticleStatus, ArticleErrorInfo
from app.nocache import nocache

convert_blueprint = Blueprint('convert_blueprint', __name__)


@convert_blueprint.route('/')
def index_page():
    # TODO: move offline processor somewhere else
    # Uncomment the following line to obtain the latest fields dictionary
    # fields = arxiv.arxiv_index()
    fields = sorted(util.homepage_fields(), key=lambda item: item['field'])
    return render.render_template('home.html', fields=fields)


@convert_blueprint.route('/list/<category_name>/')
@nocache
def listings(category_name):
    try:
        article_list = database.ListingCacher().get_article_list(category_name)
        category_display_name = util.field_lookup_table.get(category_name)
        title_text = category_display_name + util.TITLE_SUFFIX
        return render.render_template('listing.html', articles=article_list, category_name=category_name,
                                      category_display_name=category_display_name, title_text=title_text)
    except Exception as e:
        if util.PRODUCTION:
            return render.render_error_message(e)
        else:
            raise e


@convert_blueprint.route('/search_result/')
def search_result():
    """Access from the homepage html"""
    query = request.args.get("query")

    # Direct arxiv id
    matched = url.ARXIV_ID_RE.match(query)
    if matched:
        id = url.article_name_convention(matched.group(), 'url')
        return redirect('/article/%s/' % id)

    # url
    matched = url.ARXIV_URL_RE.search(query)  # search because url can be prepended with http(s)://
    if matched:
        id = url.article_name_convention(matched.group(1), 'url')
        return redirect('/article/%s/' % id)

    # TOdo: try catch
    article_list = arxiv.query_page(search_query=query)
    return render.render_template('search.html', articles=article_list, title_text=query)


@convert_blueprint.route('/article/<article_name>/<path:filename>')
@convert_blueprint.route('/test/<article_name>/<path:filename>')
@nocache
def resources(article_name, filename):
    return file_system.send_resource(article_name, filename)


@convert_blueprint.route('/test/<article_name>/c')
@nocache
def test_article_convert(article_name):
    """
    :param article_name:
    :return:
    """
    if not util.PRODUCTION:
        article_request = interface.ArticleFetchRequest(article_name=article_name,
                                                        test_mode=True,
                                                        reset_local=True)
        return article_main(article_request)
    raise FileNotFoundError("Testing environment unavailable.")


@convert_blueprint.route('/test/<article_name>/')
@nocache
def test_article(article_name):
    '''
    For local testing, target article has to present
    :param article_name:
    :return:
    '''
    if not util.PRODUCTION:
        article_request = interface.ArticleFetchRequest(article_name=article_name,
                                                        test_mode=True,
                                                        preview_mode=True,
                                                        reset_local=False,
                                                        use_database=False)
        return article_main(article_request)
        # return render.render_pandoc(article_name)
    raise FileNotFoundError("Testing environment unavailable.")


@convert_blueprint.route('/article/<article_name>/')
@nocache
def article_v2(article_name):
    article_request = interface.ArticleFetchRequest(article_name=article_name)
    return article_main(article_request)


@convert_blueprint.route('/ajax_download')
@nocache
def article_download():
    """
    Ajax call from loading page, invoked in two-session loading. Process article and populate the status to ArticleStatusQuery,
    :return: empty json
    """
    article_name = str(request.args.get('article', '0'))
    is_preview = str(request.args.get('preview', '0')) == 'True'
    try:
        download_and_process_article(article_name, is_preview)
    except interface.NotInLatexError as e:
        ArticleStatusQuery(article_name).set(ArticleStatus.FAILED, ArticleErrorInfo.NOTINLATEX)
    except interface.RenderError as e:
        ArticleStatusQuery(article_name).set(ArticleStatus.FAILED, ArticleErrorInfo.RENDER)
    except interface.ArticleNotFoundError as e:
        ArticleStatusQuery(article_name).set(ArticleStatus.FAILED, ArticleErrorInfo.NOTFOUND)
    except interface.ArticleConnectionError as e:
        ArticleStatusQuery(article_name).set(ArticleStatus.FAILED, ArticleErrorInfo.CONNECTION)
    except:
        ArticleStatusQuery(article_name).set(ArticleStatus.FAILED)
    return {}


def download_and_process_article(article_name, is_preview=False):
    """
    Helper function, raises Error if process is not successful
    :param is_preview: if True, return after tex files are processed. Otherwise spawning
    :param article_name:
    :return: Nothing
    """
    spider.download_source(article_name)
    article_request = interface.ArticleFetchRequest(article_name=article_name, preview_mode=is_preview)
    process.process_v2(article_request)


@convert_blueprint.route('/helper/post_process/<article_name>')
def post_process(article_name):
    """Indirectly invoked by process.py"""
    try:
        process.post_process(article_name)
        return {}
    except FileNotFoundError:  # Files disappear in PRODUCTION
        return redirect('/article/%s/r' % article_name)


@convert_blueprint.route('/article/<article_name>/status')
def article_status(article_name):
    return ArticleStatusQuery(article_name).get()


@convert_blueprint.route('/article/<article_name>/r')
def article_redownload_v2(article_name):
    request = interface.ArticleFetchRequest(article_name=article_name, force_refresh=True)
    return article_main(request)


@convert_blueprint.route('/article/<article_name>/c')
def article_reconvert_v2(article_name):
    request = interface.ArticleFetchRequest(article_name=article_name, reset_local=True)
    return article_main(request)


def check_article_status_and_load_or_show(request: interface.ArticleFetchRequest):
    """
    Check article status from ArticleStatusQuery (prerequisite: using database),
    - If never downloaded: invoking loading page
    - If text is ready, show article (show in preview mode if images are not ready),
    - Fallback to pdf if NOTINLATEX or RENDER error but pdf exists
    - raise error otherwise
    :param request:
    :return: rendered page
    """
    article_name = request.article_name
    response = ArticleStatusQuery(article_name).get()
    if response['status'] in (ArticleStatus.UNKNOWN.name, ArticleStatus.DOWNLOADING.name):
        logging.info("%s does not exist, invoke loading page." % article_name)
        content = {"article_name": article_name, "is_preview": True}
        return render.render_template('loading.html', **content)
    elif response['status'] in (ArticleStatus.SUCCESS.name, ArticleStatus.RENDERING.name):
        request.preview_mode = response['status'] == ArticleStatus.RENDERING.name
        if response['expire_at'] < datetime.strptime(util.FIRST_SUPPORTED_META_VERSION, '%Y%m%d%H%M%S'):
            raise interface.CacheExpiredError()
        return show_article(request)
    elif response['status'] == ArticleStatus.FAILED.name:
        if response['info'] == ArticleErrorInfo.NOTINLATEX.name:
            request.process_pdf = True
            return article_main(request)
        elif response['info'] == ArticleErrorInfo.RENDER.name:
            request.process_pdf = True
            return article_main(request)
        elif response['info'] == ArticleErrorInfo.CONNECTION.name:
            raise interface.ArticleConnectionError()
        else:
            raise interface.ArticleNotFoundError('https://arxiv.org/pdf/%s.pdf' % article_name)


def show_article_in_one_session(request: interface.ArticleFetchRequest):
    if not file_system.access(request.article_name):
        download_and_process_article(request.article_name)
    # check if meta is expired
    interface.ArticleMeta.read(request.article_name)
    return render.render_pandoc_v2(request.article_name, False)


def show_article(request: interface.ArticleFetchRequest):
    if not file_system.access(request.article_name):
        request.force_refresh = True
        return article_main(request)
    # check if meta is expired
    interface.ArticleMeta.read(request.article_name)
    return render.render_pandoc_v2(request.article_name, request.preview_mode)


def reset_local_files(request: interface.ArticleFetchRequest):
    article_name = request.article_name
    file_system.access(request.article_name)  # Download necessary bak files
    util.remove_files([article_name + '.md',
                       article_name + '.orig.md',
                       article_name + '.html',
                       util.GENERATED_BIB_FILE_NAME], util.get_path(article_name))
    image_folder = os.path.join(util.get_path(article_name), util.LATEX_GENERATED_IMAGE_FOLDER)
    if os.path.exists(image_folder):
        shutil.rmtree(image_folder)
    if request.test_mode:
        spider.generate_mock_meta(request.article_name)
    meta = interface.ArticleMeta.read(article_name)
    util.recover_bak_files(meta.tex_files, meta.article_folder)
    if util.GENERATED_BIB_FILE_NAME in meta.bib_files:
        logging.info("Removing generated bib file.")
        meta.bib_files.remove(util.GENERATED_BIB_FILE_NAME)
    meta.latex_image_requests = ()
    meta.store()


def article_main(request: interface.ArticleFetchRequest):
    try:
        if request.test_mode:
            if request.force_refresh or request.reset_local:  # testing mode does not have the ability to re-download
                reset_local_files(request)
                process.process_v2(request)
            return show_article(request)  # does not reset url by design
        if request.force_refresh:
            if request.use_database:
                database.ArticleStatusQuery(request.article_name).set(database.ArticleStatus.UNKNOWN)
            file_system.delete_all(request.article_name)
            return redirect('/article/%s' % request.article_name)  # reset url
        if request.process_pdf:  # need to be after force_refresh to handle meta file missing
            if not file_system.access(request.article_name):
                request.force_refresh = True
                return article_main(request)
            file_system.upload_all(request.article_name, extensions=[".txt"])
            return render.render_raw(request.article_name)  # TODO: do we need a separate "raw" url?
        if request.reset_local:
            reset_local_files(request)
            process.process_v2(request)
            return redirect('/article/%s' % request.article_name)  # reset url
        if request.use_database:
            return check_article_status_and_load_or_show(request)
        else:
            assert (not request.preview_mode)
            return show_article_in_one_session(request)
    except interface.CacheExpiredError:
        logging.info("Cache expired or missing, redownloading...")
        request.force_refresh = True
        return article_main(request)
    except interface.NotInLatexError as e:
        if request.use_database:
            database.ArticleStatusQuery(request.article_name).set(database.ArticleStatus.FAILED,
                                                                  database.ArticleErrorInfo.NOTINLATEX)
        if request.test_mode:
            raise e
        request.process_pdf = True
        return article_main(request)
    except interface.RenderError as e:
        if request.test_mode:
            raise e
        if request.use_database:
            database.ArticleStatusQuery(request.article_name).set(database.ArticleStatus.FAILED,
                                                                  database.ArticleErrorInfo.RENDER)
        request.process_pdf = True
        return article_main(request)
    except Exception as e:
        if not util.PRODUCTION or util.EXPLICIT_ERROR:
            raise e
        return render.render_error_message(e)
