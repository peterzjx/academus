import os
import unittest

from app.convert import interface
from app.convert.latex import LatexProcessorV2


class LatexFileInputTest(unittest.TestCase):
    def test_input(self):
        cwd = os.getcwd()
        print(cwd)
        os.chdir("../../")
        print(os.getcwd())
        meta = interface.ArticleMeta.read("test_latex_v2")
        processor = LatexProcessorV2(meta)
        filepath = os.path.join(meta.article_folder, "main.tex")
        content = processor.recursive_read(filepath, set())
        print(content)
        self.assertTrue(content.find("macro") >= 0)
        self.assertTrue(content.find("content of 1") >= 0)
        os.chdir(cwd)

    def test_expect_fail_cyclical_include(self):
        cwd = os.getcwd()
        print(cwd)
        os.chdir("../../")
        print(os.getcwd())
        meta = interface.ArticleMeta.read("test_latex_v2")
        processor = LatexProcessorV2(meta)
        filepath = os.path.join(meta.article_folder, "cyclical.tex")
        content = processor.recursive_read(filepath, set())
        print(content)
        self.assertTrue(content.find("content of 1") >= 0)
        self.assertTrue(content.find("content of 2") >= 0)
        os.chdir(cwd)


if __name__ == '__main__':
    unittest.main()
