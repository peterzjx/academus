
-- Function to parse raw LaTeX blocks into Pandoc AST
function ParseCodeBlock(elem)
    if elem.tag == "CodeBlock" then
        if elem.text:sub(1, 9) == "%NOPARSE\n" then
            -- If it starts with "%NOPARSE", return the original raw block
            return pandoc.RawBlock("latex", elem.text:sub(10))
	else
            return elem
	end
    end
end

return {
    {CodeBlock = ParseCodeBlock}
}
