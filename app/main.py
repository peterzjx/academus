from app import init_app

app = init_app()
if __name__ == '__main__':
    app.config.update(
        DEBUG=True,
        TESTING=True,
        TEMPLATES_AUTO_RELOAD=True
    )
    app.run(host='127.0.0.1', port=5000)
