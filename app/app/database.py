import logging
from datetime import datetime, timedelta
from enum import Enum

from app.convert import arxiv
from app import util
from app import db_interface
from app import login_manager
from app.db_model import User


class ListingCacher():
    def get_article_list(self, category_name):
        db = db_interface.db.listings
        cache = db.find_one({'name': category_name})
        if cache and cache['expire_at'] > datetime.now():
            logging.info("Retrieve %s from %s database expiring on %s" % (
                category_name, "PRODUCTION" if util.PRODUCTION else "DEV", cache['expire_at']))
            return cache['content']
        else:
            content = arxiv.arxivList(category_name).get_listing_dict()
            db.replace_one({'name': category_name},
                           {'name': category_name,
                            'expire_at': datetime.now() + timedelta(hours=12),
                            'content': content
                            }, upsert=True)
            logging.info("Update %s to %s database at %s" % (
                category_name, "PRODUCTION" if util.PRODUCTION else "DEV", datetime.now()))
            return content


class ArticleStatus(Enum):
    UNKNOWN = 0
    SUCCESS = 1
    FAILED = 2
    DOWNLOADING = 3
    RENDERING = 4


class ArticleErrorInfo(Enum):
    DEFAULT = 0
    NOTINLATEX = 1
    RENDER = 2
    NOTFOUND = 3
    CONNECTION = 4


class ArticleStatusQuery():
    def __init__(self, article_name):
        self.article_name = article_name
        self.db = db_interface.db.article_status

    def get(self):
        status = self.db.find_one({'article_name': self.article_name})
        if status:
            if ArticleStatus(status['status']) in (ArticleStatus.SUCCESS, ArticleStatus.FAILED) \
                    or status['expire_at'] > datetime.utcnow():
                response = {"status": ArticleStatus(status['status']).name,
                            "info": ArticleErrorInfo(status['info']).name,
                            "expire_at": status['expire_at']}
            else:
                response = {"status": ArticleStatus.UNKNOWN.name, "info": ArticleErrorInfo.DEFAULT.name,
                            'expire_at': datetime.utcnow()}  # timeout
        else:
            response = {"status": ArticleStatus.UNKNOWN.name, "info": ArticleErrorInfo.DEFAULT.name,
                        'expire_at': datetime.utcnow()}
        logging.info("Query %s, status: %s, info: %s, expire_at: %s" % (
        self.article_name, response['status'], response['info'], response['expire_at']))
        return response

    def set(self, status, info=ArticleErrorInfo.DEFAULT):
        '''
        :param status: Enum ArticleStatus
        :param info: Enum ArticleErrorInfo
        :return: None
        '''
        logging.info("Set %s, status: %s, info: %s" % (self.article_name, status.name, info.name))
        # allow retry in 30s when it's a connection error.
        if info.value == ArticleErrorInfo.CONNECTION:
            expiration_time = timedelta(seconds=30)
        else:
            expiration_time = timedelta(minutes=6)
        self.db.replace_one({'article_name': self.article_name},
                            {'article_name': self.article_name,
                             'status': status.value,
                             'expire_at': datetime.utcnow() + expiration_time,
                             'info': info.value
                             }, upsert=True)


@login_manager.user_loader
def load_user(id):
    if id == 'None':
        id = None
    return User.get_from_id(id)


if __name__ == '__main__':
    u = User(username='RuaRua', email='tz@academ.us')
    u.set_password('123456')
    u.save()
    # pass
