import datetime
import logging
import os
import subprocess

from bs4 import BeautifulSoup
from deprecated import deprecated
from flask import Markup, render_template

from app.convert import bibliography, interface
from app import database
from app import util
from ..db_model import Article


def get_meta_info(article_name):
    meta_info = {}
    meta = interface.ArticleMeta.read(article_name)
    meta_info['update_time'] = datetime.datetime.strptime(meta.update_time, "%Y%m%d%H%M%S").strftime('%B %d, %Y')
    meta_info['academus_time'] = datetime.datetime.strptime(meta.meta_version, "%Y%m%d%H%M%S").strftime(
        '%Y/%m/%d %H:%M')
    meta_info['subject'] = meta.subject or ""
    meta_info['article_name'] = article_name or ""
    meta_info['title_text'] = meta.title or ""
    meta_info['authors'] = meta.authors or []
    return meta_info


def render_error_message(message):
    return render_template('error.html', message=message), 403


def update_database_article(article_name):
    # TODO: move to somewhere else
    meta_info = get_meta_info(article_name)
    Article.update_or_create_article(
        Article(title=meta_info['title_text'],
                article_name=article_name,
                url=util.get_path(article_name),
                authors=meta_info['authors'])
        )


def render_raw(article_name):
    # TODO: non-latex does not have meta?
    content = get_meta_info(article_name)
    content['author'] = Markup(", ".join(content['authors']))
    content['title'] = Markup(content['title_text'])
    content['title_text'] = "[%s] " % article_name + content['title_text']
    content['url'] = 'https://arxiv.org/pdf/%s.pdf' % article_name

    update_database_article(article_name)  # "database" for article management
    return render_template('raw_article.html', **content)


def parse_html(html, meta, is_preview):
    soup = BeautifulSoup(html, "html.parser")
    # Extract Title, author, body
    authors = [""]
    title = ""
    title_text = ""
    try:
        author_tags = soup.findAll(attrs={"class": "author"})
        if author_tags:
            for author in author_tags:
                author.name = 'a'
            authors = [str(author) for author in author_tags]
        if soup.body.header:
            header = soup.body.header.extract()
            title = "".join([str(x) for x in header.h1.contents])
            title_text = header.h1.get_text().replace("\n", " ")
        if title is None or title == "":
            if meta.title:
                default_title = meta.title
            else:
                default_title = "No title"
            title = default_title
            title_text = default_title
        title_text += util.TITLE_SUFFIX
        abstract = soup.find(attrs={"class": "article-abstract"})
        abstract = abstract.extract() if abstract else ""
        toc = soup.find(attrs={"id": "TOC"})
        toc = toc.extract() if toc else ""

        # Move single image id to figure wrapper
        for f in soup.find_all('figure'):
            if f.img and f.img.get('id'):
                f['id'] = f.img['id']
                f.img['id'] = ""

        # Handles sub/multifigure caption
        sfs = soup.find_all('div', 'subfigures')
        for sf in sfs:
            sf.name = 'figure'
            tags = sf.find_all('p')
            if not tags:
                continue
            tags[-1].name = 'figcaption'

        for a in soup.find_all('a', href=True):
            if a['href'].startswith('#fig:'):
                a['onclick'] = 'modal_figure_popup(this)'
            if a['href'].startswith('#eq:'):
                a['onclick'] = 'modal_equation_popup(this)'

        # Button to return to TOC
        for span in soup.find_all(['h1', 'h2', 'h3']):
            return_to_toc = soup.new_tag('a', href="#TOC", **{'class': 'return_to_toc'})
            return_to_toc.string = "↩︎"
            span.insert(len(span.contents), return_to_toc)

        if is_preview:
            for img in soup.find_all('img'):
                img['asrc'] = img['src']
                img['src'] = "/static/assets/loading.gif"

        return {'title': Markup(str(title)), 'title_text': str(title_text), 'author': Markup(", ".join(authors)),
                'body': Markup(str(soup.body)),
                'toc': Markup(toc), 'abstract': Markup(abstract)}
    except Exception as e:
        util.log_or_raise_error(e)


def render_pandoc_v2(article_name, is_preview=False):
    '''
    Render .md file into html, including graphics and bibliography
    :param article_name:
    :param is_preview: When set true, all graphics will be replaced by the loading animation and will hand over
           to javascript for updating
    :return:
    '''
    def render_helper(level=0):
        """
        Level 0: Render with full bibliography
        Level 1: Filter and merge bibliography, render with generated bib
        Level 2: Ignore all bibs
        :param level: integer
        :return: rendered:Flask rendered page or None if not success
        """

        # pwd is academus/app/
        core_cmd = ['pandoc',
                    '--lua-filter', os.path.join(util.HOME_PATH, 'raw_table_parser.lua'),
                    '--filter', '../lib/pandoc-eqnos/pandoc_eqnos.py',
                    '--filter', os.path.join(util.HOME_PATH, 'pf_filter_pre_crossref.py'),
                    '--filter', 'pandoc-crossref',
                    '-M', 'eqnos-warning-level=1']
        options = ['-t', 'html',
                   '-f', 'markdown+yaml_metadata_block+multiline_tables+tex_math_double_backslash-fancy_lists',
                   '--template=template.html5',
                   '--number-sections',
                   '--toc',
                   input_path, '-o', output_path,
                   '--mathjax',
                   '-s',
                   '--columns=256',
                   '--metadata', 'linkReferences=true',
                   '--metadata', 'link-citations=true',
                   '--metadata', "reference-section-title=References",
                   ]

        if level == 0 or level == 1:
            cmd = core_cmd + \
                  citeproc_cmd + bib_cmd + \
                  options
        elif level == 2:
            cmd = core_cmd + options
        else:
            raise ValueError("Level must be an integer from 0 to 2")

        try:
            return_value = subprocess.run(cmd, capture_output=True)
            logging.info(return_value.stderr.decode().strip())
            html = util.read_file(output_path)
            content = get_meta_info(article_name)
            content.update(parse_html(html, meta, is_preview))
            content['title_text'] = "[%s] " % article_name + content['title_text']
            content['is_preview'] = is_preview
            rendered = render_template('article.html', **content)  # render a template
            return rendered
        except Exception as e:
            logging.error(e)
            return None

    input_path = os.path.join(util.get_path(article_name), article_name + '.md')
    output_path = os.path.join(util.get_path(article_name), article_name + '.html')
    if os.path.exists(output_path):
        os.remove(output_path)
    meta = interface.ArticleMeta.read(article_name)
    bib_cmd = []
    citeproc_cmd = []
    # TODO: read markdown header meta, if there is already a bib file field, use that one instead
    if meta.bib_files:
        citeproc_cmd = ['--citeproc', '--csl', 'ieee.csl']
        for bib_file in meta.bib_files:
            bib_cmd.extend(['--bibliography', os.path.join(util.get_path(article_name), bib_file)])

    # Level 0
    rendered = render_helper(level=0)
    if rendered:
        return post_render(rendered, article_name, is_preview)

    # Level 1
    logging.error("Render with bibliography has error, try fixing...")
    # TODO: Merge all bib files
    bibliography.filter_bibliography(meta)
    bib_cmd = ['--bibliography', os.path.join(util.get_path(article_name), meta.bib_files[0])]
    rendered = render_helper(level=1)
    if rendered:
        return post_render(rendered, article_name, is_preview)

    # Level 2
    logging.error("Render with bibliography has error, try rendering without bibliography...")
    rendered = render_helper(level=2)
    if rendered:
        return post_render(rendered, article_name, is_preview)

    # Still error
    return post_render(None, article_name, is_preview)


def post_render(rendered, article_name, is_preview):
    """
    Update datebase and Status query
    :param is_preview:
    :param article_name:
    :param rendered: if None, render error page
    :return: rendered or error page
    """
    if not rendered:
        database.ArticleStatusQuery(article_name).set(database.ArticleStatus.FAILED,
                                                      database.ArticleErrorInfo.RENDER)
        return render_error_message(
                    "We could not render the article. There might be compiling errors or unrecognizable LaTeX commands.")
    else:
        if not is_preview:
            database.ArticleStatusQuery(article_name).set(database.ArticleStatus.SUCCESS)
        update_database_article(article_name)
        return rendered
