import logging

import re
import panflute as pf

# (optional arguments, required arguments)
param_dict = {
    '\\frac': (0, 2),
    '\\nicefrac': (0, 2),
    '\\sfrac': (0, 2),
    '\\mathop': (0, 1),
    '\\boldsymbol': (0, 1),
    '\\boldmath': (0, 1),
    '\\bf': (0, 1),
    '\\bm': (0, 1),
    '\\bold': (0, 1),
    '\\mathds': (0, 1),
    '\\mathbb': (0, 1),
    '\\mathbbm': (0, 1),
    '\\mathcal': (0, 1),
    '\\vec': (0, 1),
    '\\hat': (0, 1),
    '\\tilde': (0, 1),
    '\\widetilde': (0, 1),
    '\\overline': (0, 1),
    '\\dot': (0, 1),
    '\\slashed': (0, 1),
    '\\protect': (0, 0),
    '\\mathbf': (0, 1),
    '\\textbf': (0, 1),
    '\\text': (0, 1),
}

# (token, index_of_param, index_of_opt)
command_replacement_rule = {
    '\\nicefrac': ('\\frac', [0, 1], []),
    '\\sfrac': ('\\frac', [0, 1], []),
    '\\ensuremath': ('NULL', [0], []),
    # '\\mbox': ('SPACE', [0], []),
    # '\\hbox': ('SPACE', [0], []),
    '\\mbox': ('NULL', [0], []),
    '\\hbox': ('NULL', [0], []),
    '\\resizebox': ('NULL', [2], []),
    '\\protect': ('NULL', [0], []),
    '\\allowbreak': ('NULL', [0], []),
    '\\tex': ('NULL', [0], []),
    '\\hspace*': ('NULL', [], []),
    '\\hspace': ('NULL', [], []),
    '\\xspace': ('NULL', [], []),
    '\\vspace': ('NULL', [], []),
    '\\centering': ('NULL', [], []),
    '\\lefteqn': ('NULL', [], []),
    '\\footnotesize': ('NULL', [], []),
    '\\bgroup': ('{', [], []),
    '\\egroup': ('}', [], []),
    '\\aftergroup': ('NULL', [], []),
    '\\dag': ('\\dagger', [], []),
    '\\degree': ('°', [], []),
    '\\degr': ('°', [], []),
    '\\Tilde': ('\\tilde', [0], []),
    '\\textgreater': ('>', [], []),
    '\\textless': ('<', [], []),
    '\\textnormal': ('\\textrm', [0], []),
    '\\textbf': ('\\boldsymbol', [0], []),
    # '\\text': ('\\mathrm', [0], []),  # mathrm removes the space between words
    '\\bf': ('\\boldsymbol', [0], []),
    '\\bm': ('\\boldsymbol', [0], []),
    '\\bold': ('\\boldsymbol', [0], []),
    '\\boldmath': ('\\boldsymbol', [0], []),
    '\\mathds': ('\\mathbb', [0], []),
    '\\mathbbm': ('\\mathbb', [0], []),
    # '\\textit': ('NULL', [0], []),  # temp hack within math
    '\\slashed': ('\\cancel', [0], []),
    '\\hdots': ('\\ldots', [0], []),
}
# TODO: process marcos
macro = {
}

package_hint = {
    "upgreek": ['\\upalpha', '\\upbeta', '\\upchi', '\\updelta', '\\Updelta', '\\upepsilon',
                '\\upeta', '\\upgamma', '\\Upgamma', '\\upiota', '\\upkappa', '\\uplambda',
                '\\Uplambda', '\\upmu', '\\upnu', '\\upomega', '\\Upomega', '\\upomicron',
                '\\upphi', '\\Upphi', '\\uppi', '\\Uppi', '\\uppsi', '\\Uppsi', '\\uprho',
                '\\upsigma', '\\Upsigma', '\\uptau', '\\uptheta', '\\Uptheta', '\\upupsilon',
                '\\Upupsilon', '\\upvarepsilon', '\\upvarphi', '\\upvarpi', '\\upvarrho',
                '\\upvarsigma', '\\upvartheta', '\\upxi', '\\Upxi', '\\upzeta'],
    "physics": ['\\qty', '\\Tr', '\\pdv', '\\eval', '\\expval', '\\dd', '\\differential']
}

def generate_package_lookup_table():
    lookup = {}
    for package, command_list in package_hint.items():
        for command in command_list:
            lookup[command] = package
    return lookup

package_lookup_table = generate_package_lookup_table()


def generate_node(tok, param, opt):
    '''
    :param tok: string, name of the root token
    :param param: list of Nodes
    :param opt: list of Nodes
    :return: Node
    '''
    if tok in command_replacement_rule:
        replacement_token, index_of_param, index_of_opt = command_replacement_rule[tok]
        logging.info("Regularized equation token %s -> %s" % (tok, replacement_token))
        return Node(replacement_token, [param[i] for i in index_of_param if i < len(param)],
                    [opt[i] for i in index_of_opt if i < len(opt)])
    else:
        return Node(tok, param, opt)


class Node:
    def __init__(self, root, param=[], opt=[]):
        self.root = root
        self.param = param
        self.opt = opt

    def __repr__(self):
        s = self.root
        if s == "NULL" or s == "SPACE":
            s = "" if s == "NULL" else " "
            for param in self.param:
                s += "".join(str(x) for x in param)
        else:
            if self.opt:
                for opt in self.opt:
                    s += "[" + "".join(str(x) for x in opt) + "]"
            if self.param:
                for param in self.param:
                    s += "{" + "".join(str(x) for x in param) + "}"
        return s


class Peekorator(object):
    # Wrapper over an iterable to add support for peek
    def __init__(self, generator):
        self.empty = False
        self.peek = None
        self.generator = generator
        try:
            if hasattr(self.generator, 'next'):
                self.peek = self.generator.next()
            else:
                self.peek = next(self.generator)
        except StopIteration:
            self.empty = True

    def __iter__(self):
        return self

    def next(self):
        """
        Return the self.peek element, or raise StopIteration
        if empty
        """
        if self.empty:
            raise StopIteration()
        to_return = self.peek
        try:
            if hasattr(self.generator, 'next'):
                self.peek = self.generator.next()
            else:
                self.peek = next(self.generator)
        except StopIteration:
            self.peek = None
            self.empty = True
        return to_return


class MathParser:
    def __init__(self, raw):
        self.load_packages = set()
        self.raw = raw
        self.chars = Peekorator(iter(self.raw))
        self.in_command = False
        self.verbatim = False
        self.tokens = Peekorator(self.get_token())
        self.nodes = self.read_until()

    def to_string(self):
        math = ""
        if self.load_packages:
            math += "".join([r"\require{%s}" % package for package in self.load_packages]) + "\n"
        # for x in self.nodes:
        #     if len(math) > 0 and math[-1] != " ":
        #         math += " "
        #     math += str(x)
        math += "".join([str(x) for x in self.nodes])
        return math

    def read_one(self):
        while not self.tokens.empty:
            tok = self.tokens.next()
            if self.verbatim:
                return Node(tok)
            if tok == '':
                continue
            elif tok.isspace():
                if self.tokens.peek is None:
                    continue
                if self.tokens.peek.isspace():
                    continue
                else:
                    return Node(tok)
            elif tok.startswith('\\') or tok in (">", "<"):
                if tok in (">", "<"):
                    self.verbatim = True
                if tok in param_dict:
                    opt_num, param_num = param_dict[tok]
                else:
                    # if the command is followed by '{', we assume it's the only parameter.
                    # Otherwise assume there are 0 parameters.
                    if self.tokens.peek == '{':
                        opt_num, param_num = (0, 1)
                    else:
                        opt_num, param_num = (0, 0)
                if tok in package_lookup_table:
                    self.load_packages.add(package_lookup_table[tok])
                opt = []
                param = []
                for i in range(opt_num):
                    while self.tokens.peek and self.tokens.peek.isspace():
                        self.tokens.next()
                    if self.tokens.peek == '[':
                        while self.tokens.peek and self.tokens.peek.isspace():
                            self.tokens.next()
                        self.tokens.next()
                        opt.append(self.read_until(']'))
                for i in range(param_num):
                    while self.tokens.peek and self.tokens.peek.isspace():
                        self.tokens.next()
                    if self.tokens.peek == '{':
                        while self.tokens.peek and self.tokens.peek.isspace():
                            self.tokens.next()
                        self.tokens.next()
                        param.append(self.read_until('}'))
                    else:  # Regularize \Operator x to \Operator{x}
                        param.append([self.read_one()])
                if tok in (">", "<"):
                    self.verbatim = False
                return generate_node(tok, param, opt)
            elif tok == '{':  # pure {} pairs, tex grammar
                return Node("", [self.read_until('}')])
            elif tok == '$':  # mixed math and text commands
                return Node("NULL", [self.read_until('$')])
            else:
                return generate_node(tok, [], [])
        return

    def read_until(self, until="\0"):
        buf = []
        while not self.tokens.empty:
            tok = self.tokens.peek
            if tok == until:
                self.tokens.next()  # consume the until char
                return buf
            node = self.read_one()
            buf.append(node)
        return buf

    def get_token(self):
        buf = ""
        while not self.chars.empty:
            ch = self.chars.next()
            if ch == '\\':
                yield buf
                self.in_command = True
                buf = ch
                if self.chars.peek in ';:,\\{}#&$%~_^\'\"`=. !()*+-/<>@[]|':
                    buf += self.chars.next()
                    self.in_command = False
            elif ch.isalpha() or ch == '*':
                if self.in_command:
                    buf += ch
                else:
                    yield buf
                    buf = ch
            else:
                self.in_command = False
                yield buf
                buf = ch
        yield buf


if __name__ == '__main__':
    pass