import unittest
from app import db_interface, app
from app.config import FlaskConfig

app.config.from_object(FlaskConfig)
db_interface.init_app(app)

from app.db_model import DatabaseModel

class DerivedModel(DatabaseModel):
    def __init__(self, **kwargs):
        super().__init__(kwargs)

    schema = {
        'name': str,
        'value': float,
    }

class UrlTest(unittest.TestCase):
    def test_initialize(self):
        DerivedModel(name='peter', value=1.0)  # should not have error
        self.assertRaises(KeyError, lambda: DerivedModel(names='peter'))
        self.assertRaises(TypeError, lambda: DerivedModel(name=1.0))

if __name__ == '__main__':
    unittest.main()
