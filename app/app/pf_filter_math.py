import re

import panflute as pf

from app.convert import mathparser
from app import util


def wrap_label(main_label, sublabels):
    sublabel_attributes = ["%s%s" % (util.EQ_PREFIX, label) for label in sublabels]
    return r"{#%s sublabel=%s}" % (util.EQ_PREFIX + main_label, ",".join(sublabel_attributes))


def process_subequation_environment(elem):
    # TODO: move the label to the attribute of the environment
    inner_labels = []
    outer_label = ""
    composed_label = ""

    def get_label(elem, doc):
        nonlocal outer_label, inner_labels
        if isinstance(elem, pf.Span) and elem.identifier:
            outer_label = elem.identifier
            return []
        if isinstance(elem, pf.Math):
            elem.text, inner_labels = process_multiline_alignment(elem.text)
            elem.text = mathparser.MathParser(elem.text).to_string()

    def attach_label_to_math(elem, doc):
        nonlocal composed_label
        if isinstance(elem, pf.Math):
            return [elem, pf.Str(" " + composed_label)]

    elem = elem.walk(get_label)
    if inner_labels:
        composed_label = wrap_label(outer_label, inner_labels)
    else:
        if outer_label:
            composed_label = "{#%s%s}" % (util.EQ_PREFIX, outer_label)
    if composed_label:
        elem = elem.walk(attach_label_to_math)
    return elem


def process_multiline_alignment(content):
    '''
    :param content: string, without surrounding $$..$$, with potential \\begin{aligned}\\end{aligned} or "gathered"
    :return: content, label
    '''
    # Handles the following cases:
    # -eqnarray(and other similar envs) will be represented as aligned
    # -Collect labels within the env, attach them afterwards.
    # -align-aligned will be represented as multiple nested aligned environments
    # -in nested environments, the outermost label should be treated as the main label if there are also inner labels

    content = re.sub(r'\\tag{(.*?)}\n?', "", content)  # todo use bracket matcher
    # Remove the second &  # TODO: this mess up with table-like alignment where one cell contains one token (e.g. pmatrix), needs to find a better solution. reverted 2021/09/19
    # content = re.sub(r'(&[^\s]*)&', r'\1', content, flags=re.DOTALL)

    # Remove -ed env names. The -ed envs prevent \tag to be used in mathjax.
    content = re.sub(r'\\begin{split}', r'\\begin{align}', content)
    content = re.sub(r'\\begin{aligned}', r'\\begin{align}', content)
    content = re.sub(r'\\begin{gathered}', r'\\begin{gather}', content)
    content = re.sub(r'\\end{split}', r'\\end{align}', content)
    content = re.sub(r'\\end{aligned}', r'\\end{align}', content)
    content = re.sub(r'\\end{gathered}', r'\\end{gather}', content)
    # \begin{align}\begin{aligned} will be turned into a nested align.
    # stripping the outer one doesn't work as outer align may contain & which will be treated as misplaced in equation
    content = re.sub(r'\\begin{align}(.*?)\\begin{align}(.*?)\\end{align}(.*?)\\end{align}', r'\\begin{align}\1\\begin{aligned}\2\\end{aligned}\3\\end{align}', content,
                     flags=re.DOTALL)
    content = re.sub(r'\\begin{align}((.*?)\\begin{gather}(.*?)\\end{gather}(.*?))\\end{align}', r'\1', content,
                     flags=re.DOTALL)
    content = re.sub(r'\\begin{gather}(.*?)\\begin{align}(.*?)\\end{align}(.*?)\\end{gather}', r'\\begin{align}\1\2\3\\end{align}', content,
                     flags=re.DOTALL)

    content = re.sub(r'\s{2}', '', content)  # remove empty lines
    labels = re.findall(r'\\label{(.*?)}', content)
    return content, labels


def math(elem, doc):
    # subequation is now treated as a regular equation set, no identifier to collect them

    if isinstance(elem, pf.Math):
        elem.text = mathparser.MathParser(elem.text).to_string()
        '''Workaround for displaying $-$3 as math'''
        # TODO: this will break table alignment format. Need a better solution. Changed to space 2020/11/10
        # Testing only adding space if the next element is also an inline math. 2021/1/11
        if elem.format == 'InlineMath':
            if isinstance(elem.next, pf.Math) and elem.next.format == 'InlineMath':
                return [elem, pf.Space]
            elif isinstance(elem.next, pf.Str) and elem.next.text[0].isdigit():
                return [elem, pf.Str(u'\u200b')]
            else:
                return elem
        else:  # DisplayMath
            elem.text, labels = process_multiline_alignment(elem.text)
            if labels:
                # only wrap label if there is at least a label. Otherwise do not append empty attribute
                if len(labels) == 1:  # Only one
                    label = "{#%s%s}" % (util.EQ_PREFIX, labels[0])
                else:
                    label = wrap_label("", labels)
                return [elem, pf.Str(" " + label + " ")]
            else:
                return elem
    #
    # if isinstance(elem, pf.Div) and "subequations" in elem.classes:
    #     return process_subequation_environment(elem)


def main(doc=None):
    return pf.run_filters([math], doc=doc)


if __name__ == '__main__':
    main()
