import logging
import os
import re
import subprocess

from app.convert import interface
from app import util


def convert_to_md(article_name):
    meta = interface.ArticleMeta.read(article_name)
    article_path = util.get_path(article_name)
    input_filename = meta.main_files[0]
    md_filename = article_name + '.orig.md'
    try:
        logging.info("Converting %s to %s" % (input_filename, md_filename))
        cmd = ['pandoc',
               '--filter', os.path.join(util.HOME_PATH, 'pf_filter_subelements.py'),
               '--filter', os.path.join(util.HOME_PATH, 'pf_filter_math.py'),
               '--filter', os.path.join(util.HOME_PATH, 'pf_filter_markdown.py'),
               '--lua-filter', os.path.join(util.HOME_PATH, 'code_block_to_raw_tex.lua'),
               '-f', 'latex',
               os.path.join(util.CONVERT_PATH, 'header-includes.yaml'),
               # '-t', 'native', input_filename,
               '-t', 'markdown-raw_html-simple_tables-multiline_tables-grid_tables', input_filename,
               '-o', md_filename,
               '-s', '--mathjax']
        # pf_filter_subelements needs to have META directly at working directory.
        return_value = subprocess.run(cmd, cwd=article_path, capture_output=True)
        logging.error(return_value.stderr.decode().strip())
        if not os.path.exists(os.path.join(article_path, md_filename)):
            raise interface.RenderError('https://arxiv.org/pdf/%s.pdf' % article_name)
    except Exception as e:
        raise e


def clean_md_file(article_name):
    path = os.path.join(util.get_path(article_name), str(os.path.basename(article_name)) + '.orig.md')
    output_path = os.path.join(util.get_path(article_name), str(os.path.basename(article_name)) + '.md')
    try:
        with open(path, 'rU', encoding='utf-8') as f:
            content = f.read()
            # extract_meta_info(article_name, content) # TODO: handles this later
            content = clean_markdown(content)
        with open(output_path, 'w', encoding='utf-8') as f:
            f.write(content)
        logging.info("Cleaned original markdown file")
    except FileNotFoundError:
        raise interface.RenderError('https://arxiv.org/pdf/%s.pdf' % article_name)


def regularize_markdown_label_style(content):
    pattern = r'\[\]{#(.*?)(?:\s.*?)?}'  # table [\[tbl:SO2\]]{#tbl:SO2 label="tbl:SO2"}
    content = re.sub(pattern, r' {#\1}', content, flags=re.DOTALL)

    pattern = r'\\#%s(.*?)' % util.EQ_PREFIX  # display math {\#eq:eq1}
    content = re.sub(pattern, r'#%s\1' % util.EQ_PREFIX, content, flags=re.DOTALL)
    return content


def regularize_markdown_ref_style(content):
    '''
        Handles figures and multifigures references
        TODO: Currently also affects section and table references
        TODO: Use a better attibute parser
    '''
    allrefs_pattern = re.compile(r'{#(.*?)(?:\s.*?)?}', flags=re.DOTALL)
    allrefs = allrefs_pattern.findall(content)
    allrefs = set(allrefs)
    sublabel_strings = re.findall(r'sublabel=([^ }]*)', content)
    for sublabel_string in sublabel_strings:
        allrefs.update(sublabel_string.strip().split(','))
    logging.debug("All references found in content:" + str(allrefs))

    def restore_crossref(m):
        '''Recover reference. Add bracket around ref number for all except equation'''
        key = util.change_special_char(m.group(1))
        if util.EQ_PREFIX + key in allrefs:
            s = util.EQ_PREFIX + key
            return r"@%s" % s
        else:
            if util.FIG_PREFIX + key in allrefs:
                s = util.FIG_PREFIX + key
            elif util.TAB_PREFIX + key in allrefs:
                s = util.TAB_PREFIX + key
            elif util.SEC_PREFIX + key in allrefs:
                s = util.SEC_PREFIX + key
            else:
                return m.group(0).replace(m.group(1), key)
                # For theorem and other format, do not change the format besides special char
                # TODO: handle actual numbering of environments
            return r"[-@%s] " % s

    # TODO: clean up the following regex
    content = re.sub(r'(?<!!)\[(?:\\\[)?[^[]+?(?:\\\])?\]\(#(.*?)\){.*?}', lambda m: restore_crossref(m),
                     content, flags=re.DOTALL)
    return content


def simple_text_replace(content):
    '''Math related. Not done in latex module because we want to apply the macros'''
    # TODO: move this to math parser
    rule = {r"\\hbox{\$(.*?)\$}": r"\\tex{\1}",
            r"\\textsc{": r"\\mathrm{\\small ",
            r"\\textup": r"\\mathrm",
            r"\\coloneqq": r"\\mathrel{\\vcenter{:}}=",
            # Todo: move the following to some post-processing filter
            r"Theorem \[Theorem": r"[Theorem",
            r"Definition \[Definition": r"[Definition",
            r"Lemma \[Lemma": r"[Lemma",
            r"Example \[Example": r"[Example",
            r"Assumption \[Assumption": r"[Assumption",
            r"Corollary \[Corollary": r"[Corollary"
            }
    for key, val in rule.items():
        content = re.sub(key, val, content)
    return content


def clean_markdown(content):
    # content = handle_math(content)
    content = simple_text_replace(content)
    # content = regularize_markdown_label_style(content)
    # content = regularize_markdown_ref_style(content)
    # content = re.sub(r'\[(.*?)\]{style="color: black"}', r'\1', content)  # handles black text (move to filter)

    # content = re.sub(r'\[(.+?)\]{.*?}', r'  {#\1}', content) # Handles the labeling of headers
    return content


if __name__ == "__main__":
    # print (renderArticle(sys.argv[-1]))
    arxiv_id = input()
    # convert_to_md(arxiv_id)
    clean_md_file(arxiv_id)
