/* Show and hide tab pills */
$(".collection-tab").click(function(e) {
   var collection_id = $(e.target).data("collection-id");
   $(".collection-tab").removeClass("active");
   $(".collection-article-list").hide();
   $("#" + collection_id).show();
   $(e.target).addClass("active");
});
$(".all-articles-tab").click(function(e) {
   $(".collection-tab").removeClass("active");
   $(".collection-article-list").show();
   $(e.target).addClass("active");
});

// Highlight last displayed tab when loaded, if there is no last tab, highlight default tab
$(document).ready(function(){
    $('.collection-tab').on('click', function(e) {
        localStorage.setItem('activeTab', $(e.target).data('collection-id'));
    });
    var active_tab_id = localStorage.getItem('activeTab');
    if(active_tab_id){
      var active_tab = $(".collection-tab[data-collection-id*='" + active_tab_id + "']");
      if (active_tab.length) {
        active_tab.trigger('click');
        return;
      }
    }
    $(".collection-tab[data-collection-is-default*='True']").trigger('click');
});


/* Rename collection tab*/
$(".rename-submenu").click(function(e) {
   $("#rename-form").show();
   $(this).closest(".btn-group").hide();
   $("#rename-form").appendTo($(this).closest(".collection-container"));
   $("#rename-form").find("#name").focus();
   $("#rename-form").find("#name").val($(this).closest(".btn-group").find(".nav-link").text());
   $("#current_collection_id").val($(this).data("collection-id"));
});
// Cancel rename when lost focus
$("#rename-form").focusout(function(e) {
  if($(e.relatedTarget).prop('type') === 'submit') {
    e.preventDefault();
  } else {
    $(this).parent().find(".btn-group").show();
    $("#rename-form").hide();
  }
});

/* Prevent Move to... menu click event*/
$(".move-to-collection-item").click(function(e){
   e.stopPropagation();
})