import panflute as pf

from app.util import FIG_PREFIX, TAB_PREFIX

MULTITABLES_PREFIX = "multitables-"


def prepare(doc):
    doc.figure_dict = {}
    doc.table_dict = {}
    pf.debug("Panflute filter running...")


def element_with_label(elem, doc):
    '''tables'''
    if isinstance(elem, pf.Table):
        if isinstance(elem.parent, pf.Div) and elem.parent.identifier:
            elem.parent.identifier = TAB_PREFIX + elem.parent.identifier
            return elem
    '''figures'''
    if isinstance(elem, pf.Figure) and elem.identifier != "":
        elem.identifier = FIG_PREFIX + elem.identifier
        return elem


def merge_multiple_subelements(elem, doc):
    if isinstance(elem, pf.Table):
        if isinstance(elem.parent, pf.Div) and elem.parent.identifier:
            id = elem.parent.identifier
            if id not in doc.table_dict:
                doc.table_dict[id] = []
            doc.table_dict[id].append(elem)
            return pf.Para(pf.Str(MULTITABLES_PREFIX + id + str(len(doc.table_dict[id]) - 1)))


def recover_subelements(doc):
    '''table'''
    for id, elem_list in doc.table_dict.items():
        for i, elem in enumerate(elem_list):
            if i != len(elem_list) - 1:
                elem.caption = pf.table_elements.Caption(pf.Para(pf.Str("")))
                # http://scorreia.com/software/panflute/code.html
                # Table caption cannot be deleted, but can be set to empty.
            doc = doc.replace_keyword(MULTITABLES_PREFIX + id + str(i), elem)


def main(doc=None):
    # TODO: waiting for support of pandoc 2.10 https://github.com/sergiocorreia/panflute/issues/142
    return pf.run_filters([
        element_with_label,
        merge_multiple_subelements,
        ],
        prepare=prepare, finalize=recover_subelements, doc=doc)


if __name__ == '__main__':
    main()
