from datetime import datetime, timedelta
import os
import random
import string
import time
from threading import Thread
import requests
import subprocess
import logging

import timeago

PRODUCTION: bool = False  # Default value for Unittest and __main__, updated at run
USE_REMOTE_CACHE: bool = False  # Default value for Unittest and __main__, updated at run
EXPLICIT_ERROR = False  # Do not use fallback, raise error
ARTICLE_PATH = "../article"
TEST_PATH = os.path.join(ARTICLE_PATH, "tests")
ASSETS_PATH = "static/assets"
META_FILE_NAME = "META.txt"
GENERATED_BIB_FILE_NAME = "academus_generated.bib"
TITLE_SUFFIX = " - Academus scientific article reader"
ARXIV_WEBSITE_URL = "http://export.arxiv.org"
ARXIV_ARCHIVE_URL = ARXIV_WEBSITE_URL + "/e-print/"
HOME_PATH = ""  # Updated at run, do not change here
CONVERT_PATH = ""  # Updated at run, do not change here
IMAGE_EXTENSIONS = {".eps", ".pdf", ".jpg", ".ps", ".png"}
DELETE_EXTENSIONS = {".sty"}
MINIMUM_RENDER_EXTENSIONS = {".md", ".bib", ".txt"}
EQ_PREFIX = "eq:"
FIG_PREFIX = "fig:"
TAB_PREFIX = "tbl:"
SEC_PREFIX = "sec:"
MAX_IMAGER_NUM = 4
STORAGE_BUCKET = "academus-261323"
LATEX_GENERATED_IMAGE_FOLDER = "_pdflatex"
FIGURE_ENVIRONMENT_DRAWING_TRIGGERS = ("\\begin{picture}", "tikz", "\\begin{forest}", "\\begin{tabular}", "\\begin{tabularx}", "\\begin{verbatim}", "\\begin{overpic}", "\\begin{minipage}", "\\begin{algorithm}")
TABLE_ENVIRONMENT_DRAWING_TRIGGERS = ("\\begin{picture}", "tikz", "\\begin{forest}")
FIRST_SUPPORTED_META_VERSION = "20230326000000"

FLAGS_USE_PROCESS_V2 = True

tikz_template = r'''
\documentclass{article}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric,arrows,trees,positioning}
\usepackage{pgfplots}
\usepackage[ruled,linesnumbered]{algorithm2e}
\begin{document}
\pagestyle{empty}
%s
\end{document} 
'''

latex_template = r'''
%s
\begin{document}
\pagestyle{empty}
\thispagestyle{empty}
%s
\end{document} 
'''
def log_or_raise_error(prefix: str, exception: Exception):
    if not PRODUCTION or EXPLICIT_ERROR:
        raise exception
    logging.error(prefix, "Error message:", str(exception))


def read_file(path):
    try:
        with open(path, 'rU', encoding='utf-8') as f:
            content = f.read()
            return content
    except UnicodeDecodeError:
        try:
            with open(path, 'rU', encoding='latin-1') as f:
                content = f.read()
                return content
        except Exception as e:
            raise e

def remove_files(files, article_folder):
    for file in files:
        if os.path.exists(os.path.join(article_folder, file)):
            os.remove(os.path.join(article_folder, file))

def recover_bak_files(tex_files, article_folder):
    for tex_file in tex_files:
        filepath = os.path.join(article_folder, tex_file)
        if os.path.exists(filepath) and os.path.exists(filepath + '.bak'):
            os.remove(filepath)
            os.rename(filepath + '.bak', filepath)

def ensure_existance(file_path, timeout=5):
    wait_time = 0
    time_step = 0.5
    while wait_time <= timeout:
        if os.path.exists(file_path):
            return True
        time.sleep(time_step)
        wait_time += time_step
    return False

def get_path(article_name: str) -> str:
    return os.path.join(ARTICLE_PATH, article_name)


def random_name():
    return ''.join(random.sample(string.ascii_lowercase, 8))

def change_special_char(name):
    # Can't use _ as section title does not allows
    # Can't have :- as :- has special meaning in markdown
    # Can't have spaces as markdown would truncate the reference label after the space
    name = name.replace(":-", "-")
    return "".join([c if c.isalnum() or c == '-' or c == ':' else "%s" % ord(c) for c in name])

def change_number(name):
    # DeclareRobustCommand cannot have number (and other chars) in the command.
    transform = lambda c: "@" + chr(int(c) + ord('a')) if c.isdigit() else c
    return "".join(map(transform, name))

def current_meta_version():
    datestr = datetime.utcnow().strftime("%Y%m%d%H%M%S")
    return datestr

def homepage_fields():
    # Todo: move to offline scripts
    fields = [{'field': 'Astrophysics', 'abbreviation': 'astro-ph'},
              {'field': 'Condensed Matter', 'abbreviation': 'cond-mat'},
              {'field': 'General Relativity and Quantum Cosmology', 'abbreviation': 'gr-qc'},
              {'field': 'High Energy Physics - Experiment', 'abbreviation': 'hep-ex'},
              {'field': 'High Energy Physics - Lattice', 'abbreviation': 'hep-lat'},
              {'field': 'High Energy Physics - Phenomenology', 'abbreviation': 'hep-ph'},
              {'field': 'High Energy Physics - Theory', 'abbreviation': 'hep-th'},
              {'field': 'Mathematical Physics', 'abbreviation': 'math-ph'},
              {'field': 'Nonlinear Sciences', 'abbreviation': 'nlin'},
              {'field': 'Nuclear Experiment', 'abbreviation': 'nucl-ex'},
              {'field': 'Nuclear Theory', 'abbreviation': 'nucl-th'},
              {'field': 'Physics', 'abbreviation': 'physics'},
              {'field': 'Quantum Physics', 'abbreviation': 'quant-ph'},
              {'field': 'Mathematics', 'abbreviation': 'math'},
              {'field': 'Quantitative Biology', 'abbreviation': 'q-bio'},
              {'field': 'Quantitative Finance', 'abbreviation': 'q-fin'},
              {'field': 'Statistics', 'abbreviation': 'stat'},
              {'field': 'Electrical Engineering and Systems Science', 'abbreviation': 'eess'},
              {'field': 'Economics', 'abbreviation': 'econ'},
              {'field': 'Computer Science', 'abbreviation': 'cs'},
              ]
    return fields

def run_cmd(cmd):
    result = subprocess.run(cmd.split(), stdout=subprocess.PIPE)
    return result.stdout.decode()

def insomniac_call(url):
    '''
        Use url_for("endpoint", arg=arg, _external=True) to self request.
        This is a workaround to keep cloud run instance alive after the previous request being returned.
    '''
    def requester():
        requests.get(url)
    thread = Thread(target=requester)
    thread.daemon = True
    thread.start()

def utc_time_to_timeago_string(t: datetime, base: datetime):
    if timedelta(0) < base - t < timedelta(days=7):
        return timeago.format(t, base)
    else:
        return t.strftime('%Y/%m/%d %H:%M')


# TODO: move to start_up. Homepage link processing
field_lookup_table = {}
for field in homepage_fields():
    field_lookup_table[field['abbreviation']] = field['field']
