import logging
import os
from abc import ABC, abstractmethod
from enum import Enum
from queue import Queue
from threading import Thread

from app import util

jobs_queue = Queue()


class ImageCovertJobType(Enum):
    DIRECT = 0
    TIKZ = 1  # deprecated
    LATEX = 2


class ImageConvertJob():
    def __init__(self, type: ImageCovertJobType, info, output_path=""):
        self.type = type
        self.info = info
        self.output_path = output_path


class ImageConvertManager():
    def __init__(self, jobs):
        self.job_list = jobs

    def add(self, job):
        self.job_list.append(job)

    def append(self, jobs):
        self.job_list += jobs

    def convert(self):
        self._multithread_convert(self.job_list)

    @staticmethod
    def _worker_convert(worker_id, job: ImageConvertJob):
        if job.type == ImageCovertJobType.DIRECT:
            DirectImager(worker_id, job).convert()
        elif job.type == ImageCovertJobType.LATEX:
            LatexImager(worker_id, job).convert()

    @staticmethod
    def _multithread_convert(job_list):
        threads = []
        logging.info("Convert: %s workers ready for %s jobs" % (util.MAX_IMAGER_NUM, len(job_list)))
        for job in job_list:
            jobs_queue.put(job)
        for i in range(util.MAX_IMAGER_NUM):
            thread = Thread(target=ImageConvertManager._retrieve_job, args=(jobs_queue, i))
            threads.append(thread)
            thread.start()
        for i in range(util.MAX_IMAGER_NUM):
            threads[i].join(timeout=90)

    @staticmethod
    def _retrieve_job(queue, worker_id):
        while not queue.empty():
            job = queue.get()
            ImageConvertManager._worker_convert(worker_id, job)


class Imager(ABC):
    def __init__(self, worker_id, job: ImageConvertJob):
        self.worker_id = worker_id
        self.job = job
        logging.info("Worker %d (%s) Converting %s" % (worker_id, self.__class__.__name__, str(job.info)))

    @abstractmethod
    def convert(self):
        pass


class DirectImager(Imager):
    def __init__(self, worker_id, job: ImageConvertJob):
        super().__init__(worker_id, job)

    def convert(self):
        filename, file_extension = os.path.splitext(os.path.basename(self.job.info))
        if file_extension == '.png':
            return
        if file_extension == '.PNG':
            input_name = os.path.join(os.path.dirname(self.job.info), filename) + '.PNG'
            target_name = os.path.join(os.path.dirname(self.job.info), filename) + '.png'
            os.rename(input_name, target_name)
            return
        input_name = os.path.basename(self.job.info)
        target_name = filename + '.png'
        self.job.output_path = os.path.join(os.path.dirname(self.job.info), target_name)
        try:
            cmd = 'convert -density 300 "%s" -trim "%s"' % (
                os.path.join(os.path.dirname(self.job.info), input_name), self.job.output_path)
            os.system(cmd)
            # If pdf file contains multiple pages, the output filename will be "filename-0.png", "filename-1.png" etc
            if not os.path.exists(self.job.output_path):
                path, extension = os.path.splitext(self.job.output_path)
                long_name = path + '-0' + extension
                if os.path.exists(long_name):
                    os.rename(long_name, self.job.output_path)
        except Exception as e:
            logging.error("Error in converting image" + str(input_name))
            logging.error(e)


class LatexImager(Imager):
    def __init__(self, worker_id, job: ImageConvertJob):
        super().__init__(worker_id, job)

    def convert(self):
        try:
            filename: str = self.job.info.get('filename')
            fallback_filename = filename + '_fallback'
            path: str = self.job.info.get('path')
            output_path = self.job.output_path
            pdf_path = os.path.join(path, filename + '.pdf')
            cmd = f"cwd=$(pwd); cd '{path}'; pdflatex -interaction=batchmode -output-directory='.' -jobname='{filename}' '{filename}'; cd $cwd"
            logging.info(cmd)
            os.system(cmd)
            png_path = os.path.join(path, filename + '.png')
            if not os.path.exists(pdf_path):
                logging.info("LaTeX engine dies, trying fallback")
                cmd = f"cwd=$(pwd); cd '{path}'; pdflatex -interaction=batchmode -output-directory='.' -jobname='{filename}' '{fallback_filename}'; cd $cwd"
                logging.info(cmd)
                os.system(cmd)
            if not os.path.exists(pdf_path):
                raise FileNotFoundError(pdf_path)
            cmd = 'convert -density 300 "%s" -trim "%s"' % (pdf_path, png_path)
            os.system(cmd)
            # Move the files
            if not os.path.exists(output_path):
                os.mkdir(output_path)
            for ext in ('', '.log', '.png', '_fallback'):
                os.system(
                    "mv '%s' '%s'" % (os.path.join(path, filename + ext), os.path.join(output_path, filename + ext)))
            for ext in ('.aux', '.pdf', '.out'):
                os.system("rm '%s'" % os.path.join(path, filename + ext))
        except Exception as e:
            logging.error("Error in generating image " + str(e))
