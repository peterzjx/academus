from app import db_interface
from app.config import FlaskConfig

app.config.from_object(FlaskConfig)
db_interface.init_app(app)

from app.db_model import User

if __name__ == '__main__':
    if User.create_new_user(username="Test User 3",
                            email="test3@academ.us",
                            password="123456"):

        print("success")
