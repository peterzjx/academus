from datetime import datetime, timedelta

import timeago


def format_article_display(article, time_added: datetime):
    """

    :param article: Article db model
    :param time_added: datetime obj
    :return: dictionary where:
    article_name: article_name
    url: relative url for the href, e.g. "../article/article_name"
    title: truncated
    author_str: concatenated string for all authors, truncated
    time_added: string formatted with timeago, or string for datetime if older than a threshold
    """
    TITLE_MAX_LEN = 200
    AUTHOR_MAX_LEN = 100
    TIMEAGO_FORMAT_MAX_DAYS = 7

    article_display = {'article_name': article['article_name'],
                       'url': article['url']}
    if len(article['title']) > TITLE_MAX_LEN:
        article_display['title'] = article['title'][:TITLE_MAX_LEN] + '...'
    else:
        article_display['title'] = article['title']

    author_str = ", ".join(article['authors'])
    if len(author_str) > AUTHOR_MAX_LEN:
        article_display['author_str'] = author_str[:AUTHOR_MAX_LEN] + '...'
    else:
        article_display['author_str'] = author_str

    if datetime.utcnow() - time_added < timedelta(days=TIMEAGO_FORMAT_MAX_DAYS):
        article_display['time_added'] = timeago.format(time_added, datetime.utcnow())
    else:
        article_display['time_added'] = time_added.strftime('%Y/%m/%d %H:%M')
    return article_display