from bs4 import BeautifulSoup
import urllib.request
from urllib.parse import urlencode
from app import url
import re
from app import util
from datetime import datetime
from flask import Markup


def arxiv_index():
    '''One-time use for generating the utils.homepage_fields()'''
    with urllib.request.urlopen(util.ARXIV_WEBSITE_URL) as response:
        html = response.read()
    soup = BeautifulSoup(html, "html.parser")
    all_fields = []
    for item in soup.find_all('a'):
        url_string = item.get('href')
        match_archive = re.match("/archive/(.*)", url_string)
        if match_archive:
            abbr = match_archive.group(1)
            all_fields.append({"field": item.text, "abbreviation": abbr})
    return all_fields

class arxivList:
    def __init__(self, category_name):
        self.url = util.ARXIV_WEBSITE_URL + '/list/%s/new'
        self.article_list = []
        self.article_list_len = 0
        url = self.url % category_name
        with urllib.request.urlopen(url) as response:
            html = response.read()
        soup = BeautifulSoup(html, "html.parser")
        article_name_elements = soup.findAll("span", {"class": "list-identifier"})
        article_meta_elements = soup.findAll("div", {"class": "meta"})
        assert (len(article_name_elements) == len(article_meta_elements))
        self.article_list = [dict() for _ in range(len(article_name_elements))]
        self.populate_article_metas(article_meta_elements)
        self.populate_article_names(article_name_elements)
        self.article_list = self.article_list[:self.article_list_len - 1]  # remove resubmitted or empty articles

    def populate_article_metas(self, article_meta_elements):
        for i, elem in enumerate(article_meta_elements):
            title = elem.findAll("div", {"class": "list-title"})[0].contents[-1]
            title = title.strip().replace("  ", " ")
            authors_elements = elem.findAll("div", {"class": "list-authors"})[0].findAll("a")
            author = [x.text for x in authors_elements]
            if elem.p is None:
                self.article_list_len = i + 1
                break
            abstract = elem.p.text.replace("\n", " ")

            self.article_list[i]["author"] = author
            self.article_list[i]["title"] = title
            self.article_list[i]["abstract"] = abstract

    def populate_article_names(self, article_name_elements):
        for i, elem in enumerate(article_name_elements):
            article_name = re.match(r"/abs/(.*)", elem.a.get('href')).group(1)
            self.article_list[i]["article_name"] = article_name

    def get_listing_dict(self):
        for i, article in enumerate(self.article_list):
            article['id'] = i + 1
            article['article_name'] = Markup(str(article['article_name']))
            article['abstract'] = Markup(str(article['abstract']))
        return self.article_list


def arxiv_page(article_name, return_value):
    abstract_url = util.ARXIV_WEBSITE_URL + '/abs/' + url.article_name_convention(article_name, 'display')
    with urllib.request.urlopen(abstract_url) as response:
        html = response.read()
    soup = BeautifulSoup(html, "html.parser")

    primary_subject = ""
    secondary_subject = []
    table = soup.find(
        lambda tag: tag.name == 'table' and tag.has_attr('summary') and tag['summary'] == "Additional metadata")
    for line in table.findAll('tr'):
        for l in line.findAll('td'):
            if l.find('span', {"class", "primary-subject"}):
                primary_subject = l.find('span', {"class", "primary-subject"}).extract().text
                secondary_subject = l.text.split(";")
                secondary_subject = [tmp.strip() for tmp in secondary_subject if tmp]
    author_name_node = soup.find("div", {"class": "authors"})
    author_name = [] if author_name_node is None else author_name_node.text.replace("Authors:", "").replace("\n", "").split(',')
    author_name = [name.strip() for name in author_name]
    title_node = soup.find("h1", {"class": "title mathjax"})
    title = "" if title_node is None else title_node.text.replace("Title:", "").replace("\n", "")
    title = ' '.join(title.split())  # replace multiple spaces to one
    submission_hist_node = soup.find("div", {"class": "submission-history"})
    update_time = None
    search_date = None
    if submission_hist_node:
        search_date = re.search(r"\[.*?\] (.*?)  \(", submission_hist_node.text, flags=re.MULTILINE)
    if search_date:
        #  Wed, 15 Apr 2020 18:00:01 GMT
        date_str = search_date.group(1)
        update_time = datetime.strptime(date_str, '%a, %d %b %Y %H:%M:%S %Z')
    return_value.update({"title": title,
            "author_name": author_name,
            "update_time": update_time,
            "subject": {"primary_subject": primary_subject, "secondary_subject": secondary_subject}})

def convert_entry_to_paper(entry):
    """
    Convert an <entry> into a dictionary to initialize a paper
    with.
    """
    d = {}
    article_name_with_version = url.ARXIV_ID_RE.search(entry.id.text).group()  # TODO:handle None
    article_name, _ = url.remove_version_from_arxiv_id(article_name_with_version)
    d["article_name"] = url.article_name_convention(article_name, 'display')
    d["title"] = entry.title.text.replace("\n", "").replace("  ", " ")
    # d["published"] = dateutil.parser.parse(entry.find("published"))
    # d["updated"] = dateutil.parser.parse(entry.find("updated"))
    d["abstract"] = entry.summary.text.replace("\n", " ")
    d["author"] = []
    for author in entry.find_all("author"):
        # d["author"].append(
        #     {
        #         "name": author.find("name").text,
        #         "affiliation": [
        #             e.text for e in author.find_all("affiliation")
        #         ],
        #     }
        # )
        d["author"].append(author.find("name").text)
    # d["arxiv_url"] = entry.find("./atom:link[@type='text/html']", NS).attrib["href"]
    # d["pdf_url"] = entry.find("./atom:link[@type='application/pdf']", NS).attrib["href"]
    # d["primary_category"] = entry.find("arxiv:primary_category", NS).attrib["term"]
    # d["categories"] = [
    #     e.attrib["term"]
    #     for e in entry.findall("atom:category", NS)
    #     if len(e.attrib["term"]) < 25
    # ]
    # # Optional
    # d["comment"] = getattr(entry.find("arxiv:comment", NS), "text", None)
    # d["doi"] = getattr(entry.find("arxiv:doi", NS), "text", None)
    # d["journal_ref"] = getattr(entry.find("arxiv:journal_ref", NS), "text", None)
    #
    # # Remove version from everything
    # d["arxiv_id"], d["arxiv_version"] = remove_version_from_arxiv_id(d["arxiv_id"])
    # d["arxiv_url"] = remove_version_from_arxiv_url(d["arxiv_url"])
    # d["pdf_url"] = remove_version_from_arxiv_url(d["pdf_url"])
    return d

def query_page(search_query=None, id_list=None, start=0, max_results=100):
    url_args = {"start": start, "max_results": max_results}
    if search_query is not None:
        url_args["search_query"] = search_query
    if id_list is not None:
        url_args["id_list"] = ",".join(id_list)

    url = util.ARXIV_WEBSITE_URL + '/api/' + "query?" + urlencode(url_args)
    with urllib.request.urlopen(url) as response:
        html = response.read()
    soup = BeautifulSoup(html, features="lxml")  # xml
    article_list = []
    for entry in soup.find_all('entry'):
        if entry.find("id") is not None:
            article = convert_entry_to_paper(entry)
            if '/' not in article['article_name']:  # TODO: support legacy naming convention, e.g. cond-mat/0403531
                article_list.append(article)
    for i, entry in enumerate(article_list):
        entry["id"] = i + 1
    return article_list

if __name__ == '__main__':
    b = {}
    arxiv_page('2004.07245', b)
    print(b)
