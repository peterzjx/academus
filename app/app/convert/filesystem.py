import logging
import os
import shutil

from flask import send_from_directory

from app import util


class FileSystem:
    def __init__(self, store):
        # store is GoogleStorage
        self.store = store

    def send_resource(self, article_name, filename, mimetype='image/png'):
        file_path = os.path.join(util.get_path(article_name), filename)
        if os.path.exists(file_path):
            # current_app is one level below cwd
            return send_from_directory(os.path.join('../', util.get_path(article_name)), filename, mimetype=mimetype)
        if self.store.initialized:
            cached = self.store.get(file_path)
            if cached:
                return cached
        logging.error("File %s not found." % file_path)
        return send_from_directory(util.ASSETS_PATH, "dummy.png", mimetype=mimetype)

    def access(self, article_name):
        """
        Prepare the needed files for converting article_name, fetching from local and remote storage
        :param article_name:
        :return: True if article is accessible
        """
        meta_path = os.path.join(util.get_path(article_name), util.META_FILE_NAME)
        if not os.path.exists(meta_path):
            if self.store.initialized:
                if self.store.get(meta_path):
                    logging.info("Found in storage, downloading minimum render files")
                    self.download_all(article_name, extensions=util.MINIMUM_RENDER_EXTENSIONS)
                    return os.path.exists(meta_path)  # should not be false, where the downloaded file is missing
            return False
        return True

    def download_all(self, article_name, extensions=None):
        if not self.store.initialized:
            logging.error("Store is not available, please initialize FileSystem with a GoogleStorage")
        else:
            self.store.download_all(util.get_path(article_name), extensions=extensions)

    def upload_all(self, article_name, extensions=None):
        if not self.store.initialized:
            logging.error("Store is not available, please initialize FileSystem with a GoogleStorage")
        else:
            logging.info("Uploading {} to storage {}".format(str(extensions), util.get_path(article_name)))
            self.store.upload_all(util.get_path(article_name), extensions=extensions)

    def delete_all(self, article_name):
        article_folder = util.get_path(article_name)
        if os.path.exists(article_folder):
            shutil.rmtree(article_folder)
        if self.store.initialized:
            self.store.delete_all(article_folder)

