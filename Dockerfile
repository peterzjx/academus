FROM tiangolo/meinheld-gunicorn-flask:python3.7

RUN apt-get update && apt-get install -qy vim
RUN apt-get install -qy python3-pip

# latex
RUN apt-get install -qy ghostscript
RUN apt-get install -qy texlive-pictures texlive-science texlive-latex-extra
COPY ./lib /lib
COPY ./requirements.txt /tmp/

# python dependency
RUN pip3 install -r /tmp/requirements.txt
RUN pip3 install Flask gunicorn
# master branch
RUN pip3 install -e /lib/TexSoup

# ImageMagick
RUN apt-get install imagemagick
COPY ./policy.xml /etc/ImageMagick-6/

# pandoc
ENV PATH="/lib/pandoc-3.1.1/bin:$PATH"
# customized version
RUN pip3 install -e /lib/pandoc-xnos
RUN pip3 install -e /lib/pandoc-eqnos

# main app
COPY ./app /app

# port
ENV PORT 8080
EXPOSE 8080

ENV GOOGLE_APPLICATION_CREDENTIALS="/app/academus-1a417772618a.json"

# Run the web service on container startup
# Increase the number of workers to be equal to the cores available.
CMD exec cd ..
CMD exec gunicorn --bind :$PORT --workers 2 --threads 8 --timeout 300 main:app
