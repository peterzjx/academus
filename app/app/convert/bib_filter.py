import os
import string as str_lib
import re
import logging

PRINTABLES = list(str_lib.printable + '\n\t')
KEY_WORDS = ['author', 'year', 'month', 'pages', 'title', 'volume',
             'journal', 'doi', 'publisher', 'url', 'abstract',
             'number', 'issn', 'key', 'issue', 'numpages', 'urldate']

KEY_WORD_OBJS = list(map(lambda key: re.compile((key + r'[\s+=]'), re.IGNORECASE), KEY_WORDS))

SPECIAL_TYPES = ['string', 'control', 'footnote']
INVALID_NAME = ['|', ' ', '{', '}']


def parser(string):
    string = string.replace('\}', ']')
    string = string.replace('\{', '[')
    count = 0
    tnuoc = 0
    unmatched = 0
    str_ls = list(string)
    for i, ch in enumerate(str_ls):
        if ch == '{':
            count = count + 1
        elif ch == '}' and count > 0:
            count = count - 1
        elif ch == '}' and count <= 0:
            str_ls[i] = ' '
            unmatched = unmatched + 1

        if ch == '[':
            tnuoc = tnuoc + 1
        elif ch == ']' and tnuoc > 0:
            tnuoc = tnuoc - 1
        elif ch == ']' and tnuoc <= 0:
            str_ls[i] = ' '
            unmatched = unmatched + 1
    str_ls = str_ls + [']'] * tnuoc + ['}'] * count
    unmatched = unmatched + tnuoc + count
    output = ''.join(str_ls)
    output = output.replace('[', '\{')
    output = output.replace(']', '\}')
    # if unmatched > 0:
    #     logging.info('Unmatched curly brackets found. Auto-matched.')
    return output


def check_bibname(string):
    string = parser(string)
    str_ls = list(string.strip())
    for char in str_ls:
        if char in INVALID_NAME:
            str_ls.remove(char)
    output = ''.join(str_ls)
    return output


def special_entry(string):
    flag = False
    for word in SPECIAL_TYPES:
        match_obj = re.compile(word, re.IGNORECASE)
        if match_obj.search(string) != None:
            flag = True
    return flag


def pre_process(string):
    content = string
    # remove comments
    new_content = ""
    line_list = content.split("\n")
    for line in line_list:
        if not line.startswith("%"):
            new_content = new_content + '\n' + line
        new_content = re.sub('%', '', new_content)
    content = new_content
    # remove non-printable/ chars
    content = list(content)
    new_content = []
    for ith, ch in enumerate(content):
        if ch not in PRINTABLES:
            content[ith] = '?'
    # remove redundant spaces
    content = ''.join(content)
    content = re.sub(' +', ' ', content)
    content = re.sub('\t+', '\t', content)
    content = re.sub('\n+', '\n', content)
    return content


def after_process(string):
    # remove redundant spaces
    content = re.sub(' +', ' ', string)
    content = re.sub('\t+', '\t', content)
    content = re.sub('\n+', '\n', content)
    return content


def get_field(input_string):
    loc_list = []
    out_dict = {}

    for ith, obj in enumerate(KEY_WORD_OBJS):
        tmp = obj.search(input_string, re.DOTALL)
        if tmp:
            loc_list.append([tmp.start(), tmp])
    loc_list = (sorted(loc_list))
    for ith, item in enumerate(loc_list):
        item_name = re.findall(r'(\S+)[\s+=]', item[1].group(0))[0]
        if ith < len(loc_list) - 1:
            start = loc_list[ith][1].end()
            end = loc_list[ith + 1][1].start()
        else:
            start = loc_list[ith][1].end()
            end = len(input_string) - 1
        content = (input_string[start:end])
        content = content.replace('\n', ' ')
        content = content.strip()
        content = content.strip("=")
        content = content.strip()
        content = content.strip(',')
        content = content.strip('"')
        content = content.strip('{')
        content = content.strip('}')
        content = parser(content)
        content = list(content)
        content = ("".join(content))
        out_dict.update({item_name: content})
    return out_dict


class BibitemBase():
    def __init__(self, bib_type='article', bib_name='Nah2020'):
        self.bib_type = bib_type
        self.bib_name = bib_name
        self.out_dict = {}

    def format(self):
        content = ''
        for ith, field in enumerate(self.out_dict):
            content = content + field + ' = {' + self.out_dict[field] + '}'
            if ith < len(self.out_dict) - 1:
                content = content + ',\n'

        return '\n@' + self.bib_type + '{' + self.bib_name + ',\n' + content + '\n}'

    @property
    def bib_type(self):
        return self._bib_type

    @bib_type.setter
    def bib_type(self, value):
        self._bib_type = value

    @property
    def bib_name(self):
        return self._bib_name

    @bib_name.setter
    def bib_name(self, value):
        self._bib_name = value


class BibClass():
    def __init__(self, in_string):
        self.bib_list = []
        self.raw_content = ""
        self.raw_content = pre_process(in_string)

    def extract(self):
        content_raw = (self.raw_content).split('@')
        content = []
        for i in content_raw:
            if i.strip():
                content.append('@' + i.strip())

        self.bib_list = content
        out_bib = ''
        for bib_item in self.bib_list:
            bib_format = BibitemBase()

            if re.search(r'@(\w+)', bib_item):
                matched = re.search(r'@(\w+)(.*)', bib_item, re.DOTALL)
                bib_format.bib_type = matched.group(1)
                bib_body = matched.group(2)
                bib_item = bib_format.bib_type + bib_body
            else:
                logging.warning('Bib-type is missing')
                continue
            if re.search("({.+?),", bib_item):
                bib_format.bib_name = (re.search('{(.*?),', bib_item, re.DOTALL).group(1))
                bib_format.bib_name = check_bibname(bib_format.bib_name)
                if not bib_format.bib_name:
                    bib_format.bib_name = 'Undefined'
                    logging.warning('Bib-name is missing')
            if special_entry(bib_format.bib_type) or bib_format.bib_type == '':
                logging.warning('Undefined bib-type or invalid characters in bib-type. This bib-item will be skipped.')
                out_bib = out_bib + '\n'  # remove directly
                # out_bib=out_bib+bib_item+'\n'   #keep original
            else:
                out_dict = get_field(str(bib_item))
                bib_format.out_dict = out_dict
                out_bib = out_bib + bib_format.format()

        out_bib = after_process(out_bib)
        return out_bib


if __name__ == '__main__':
    TEST_DIR = "/home/peter/academus/article/2005.00536/"
    f_list = []

    for (root, dirs, files) in os.walk(TEST_DIR):
        for filename in files:
            name = (os.path.join(root, filename))
            if name[-4:] == '.bib':
                f_list.append(name)
    for f in f_list:
        with open(f) as dfile:
            fstream = dfile.read()
        bib_obj = BibClass(fstream)
        output = bib_obj.extract()
        print(output)
