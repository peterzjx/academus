if [[ $* == *--no-promote* ]]
then
  echo "Running with no promote mode"
  gcloud builds submit --tag gcr.io/academus-261323/academus --timeout=7200;
  gcloud run deploy academus --image gcr.io/academus-261323/academus --platform managed --project=academus-261323 --cpu 2 --tag alpha --no-traffic
else
  gcloud builds submit --tag gcr.io/academus-261323/academus --timeout=7200;
  gcloud run deploy academus --image gcr.io/academus-261323/academus --platform managed --project=academus-261323 --cpu 2;
  gcloud run services update-traffic academus --to-latest;
fi
