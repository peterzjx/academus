import unittest
from app.convert import mathparser

class MathParserTest(unittest.TestCase):
    def parse(self, tex):
        return mathparser.MathParser(tex).to_string()

    def test_basic_math(self):
        self.assertEqual(self.parse(r"x+y"), "x+y")
        self.assertEqual(self.parse(r"x^2"), "x^2")

    def test_auto_matching_param_number(self):
        # Auto zero/one param. \noparam and \autoparam are NOT defined before.
        self.assertEqual(self.parse(r"\noparam \noparam"), r"\noparam \noparam")
        self.assertEqual(self.parse(r"\autoparam{x}"), r"\autoparam{x}")

        # One param. \mathop and \mathrm are defined in math_parser to have exact 1 param
        self.assertEqual(self.parse(r"\mathop\mathrm{x}"), r"\mathop{\mathrm{x}}")
        self.assertEqual(self.parse(r"\mathop \noparam x"), r"\mathop{\noparam} x")
        self.assertEqual(self.parse(r"\mathop \mathop x"), r"\mathop{\mathop{x}}")
        print(self.parse(r"\dot \mathbf v_i"))


        # Two params
        self.assertEqual(self.parse(r"\frac a b c"), r"\frac{a}{b} c")

    def test_empty_brackets(self):
        self.assertEqual(self.parse(r"{\boldsymbol a}"), r"{\boldsymbol{a}}")
        self.assertEqual(self.parse(r"{\hat \boldsymbol a}"), r"{\hat{\boldsymbol{a}}}")
        self.assertEqual(self.parse(r"{\boldsymbol \hat {a}}"), r"{\boldsymbol{\hat{a}}}")

    def test_mbox_bold(self):
        self.assertEqual(self.parse(r"\mbox{\boldmath{$j$}}"), r"\boldsymbol{j}")
        self.assertEqual(self.parse(r"\mbox{\boldmath$ B$}"), r"\boldsymbol{ B}")
        self.assertEqual(self.parse(r"\mbox{\boldmath$a_{2}$}"), r"\boldsymbol{a_{2}}")

    def test_tabular_preamble(self):
        nodes = mathparser.MathParser(r"C{red!30}{4cm}C{blue!20}{3cm}C{green!20}{5cm}c|l|rcr@{1.2in}>{$}c<{$}").nodes
        gt = "[C, {red!30}, {4cm}, C, {blue!20}, {3cm}, C, {green!20}, {5cm}, c, |, l, |, r, c, r, @, {1.2in}, >{$}, c, <{$}]"
        self.assertEqual(str(nodes), gt)


if __name__ == '__main__':
    unittest.main()