import logging
import time
import os

from flask import Flask
from flask_login import LoginManager
from flask_pymongo import PyMongo

from app import util
from app.convert.filesystem import FileSystem
from .storage import GoogleStorage


def log_library_version():
    logging.info(util.run_cmd("convert --version"))
    logging.info(util.run_cmd("pandoc --version"))
    import TexSoup
    logging.info("TexSoup version " + str(TexSoup.__version__))
    import panflute
    logging.info("panflute version " + str(panflute.__version__))


def log_startup_info():
    # Logger
    logging.basicConfig(format='%(asctime)s %(levelname)s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d:%H:%M:%S',
                        level=logging.DEBUG)
    logging.Formatter.converter = time.gmtime
    logging.info("Running in %s mode" % ("PRODUCTION" if util.PRODUCTION else "DEV"))
    log_library_version()


login_manager = LoginManager()
login_manager.login_view = 'login'
db_interface = PyMongo()
store = GoogleStorage()
file_system = FileSystem(store)

def init_app():
    """Initialize the core application."""
    from app import util
    util.PRODUCTION = True if os.environ.get('DEV_ENVIRONMENT') is None else False
    # util.PRODUCTION = True  # Change this if debug PRODUCTION environment in local
    util.USE_REMOTE_CACHE = util.PRODUCTION  # Change this if debug storage in local
    util.HOME_PATH = os.path.join(os.getcwd(), 'app')
    util.CONVERT_PATH = os.path.join(util.HOME_PATH, 'convert')

    app = Flask(__name__)
    from app.config import FlaskConfig  # depends on util.PRODUCTION
    app.config.from_object(FlaskConfig)
    log_startup_info()

    # initialize interfaces
    login_manager.init_app(app)
    db_interface.init_app(app)
    if util.USE_REMOTE_CACHE:
        store.initialize()

    with app.app_context():
        # The following imports depend on an initialized db_interface
        from app.user.routes import user_blueprint
        app.register_blueprint(user_blueprint)
        from app.convert.routes import convert_blueprint
        app.register_blueprint(convert_blueprint)
        return app


