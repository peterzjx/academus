import tempfile
import os
import uuid
import random
import tqdm
import shutil

tex_template = r'''
\documentclass{article}
\begin{document}
\pagestyle{empty}
%s
\end{document} 
'''
alphabet = list("()*+,-./0123456789:;<=>ABCDEFGHIJKLMNOPQRSTUVWXYZ[]abcdefghijklmnopqrstuvwxyz")
alphabet.extend(
    ['\\Delta', '\\Gamma', '\\Lambda', '\\Omega', '\\Phi', '\\Pi', '\\Psi', '\\Rightarrow', '\\Sigma', '\\Theta',
     '\\Xi', '\\alpha', '\\approx', '\\beta', '\\cdots', '\\dagger', '\\delta', '\\ell', '\\epsilon', '\\equiv',
     '\\eta', '\\gamma', '\\geq', '\\hbar', '\\in', '\\infty', '\\kappa', '\\lambda', '\\leq', '\\mp', '\\mu',
     '\\nabla', '\\neq', '\\nu', '\\omega', '\\oplus', '\\otimes', '\\phi', '\\rho', '\\sigma', '\\sim', '\\tau',
     '\\theta', '\\times', '\\varepsilon', '\\varphi', '\\varpi', '\\varrho', '\\vartheta', '\\xi', '\\zeta', '\\{',
     '\\}'])


def generate_one_image(raw_tex, output_path):
    try:
        with tempfile.TemporaryDirectory() as temp_dir:
            tex_path = os.path.join(temp_dir, "temp.tex")
            with open(tex_path, 'w') as tex_file:
                tex_file.write(tex_template % raw_tex)
            pdf_filename = 'temp'
            cmd = "pdflatex -interaction=batchmode -output-directory='%s' -jobname='%s' %s" % (
                temp_dir, pdf_filename, tex_path)
            os.system(cmd)
            path = os.path.join(temp_dir, pdf_filename + '.pdf')
            output_filename = str(uuid.uuid4()) + '.png'
            cmd = 'convert -density 320 -trim -resize x32 "%s" "%s"' % (
            path, os.path.join(output_path, output_filename))
            print(cmd)
            os.system(cmd)
            return raw_tex.strip("$"), output_filename
    except Exception as e:
        print("Error in generating image")
        print(e)

def get_random_equation(length):
    letters = [random.choice(alphabet) for i in range(length)]
    return " ".join(letters)

def get_random_length_equation(min_length, max_length):
    length = random.randrange(min_length, max_length)
    return get_random_equation(length)


num_images = 20000
src_list = []
tgt_list = []
output_path = '/home/peter/academus/experimental/linear_20000'
if os.path.exists(os.path.join(output_path, 'images')):
    shutil.rmtree(os.path.join(output_path, 'images'))
os.mkdir(os.path.join(output_path, 'images'))
for i in tqdm.tqdm(range(num_images)):
    equation = "$%s$" % get_random_length_equation(3, 16)
    tgt, src = generate_one_image(equation, os.path.join(output_path, "images"))
    src_list.append(src)
    tgt_list.append(tgt)

mode = 'train'
with open(os.path.join(output_path, 'src-%s.txt' % mode), 'w') as file:
    file.write("\n".join(src_list))
with open(os.path.join(output_path, 'tgt-%s.txt' % mode), 'w') as file:
    file.write("\n".join(tgt_list))
with open(os.path.join(output_path, 'vocab.txt'), 'w') as file:
    file.write("\n".join(alphabet))