# Google Cloud Storage API wrapper
from google.cloud import storage
import tempfile
from app import util
from flask import send_file
import os
import logging


def check_initialized(func):
    def inner(self, *args, **kwargs):
        if not self.initialized:
            raise ValueError("Call self.initialize() before accessing the storage")
        else:
            return func(self, *args, **kwargs)

    return inner


class GoogleStorage:
    def __init__(self):
        self.client = None
        self.bucket = None
        self.initialized = False

    def initialize(self):
        self.client = storage.Client()
        self.bucket = self.client.get_bucket(util.STORAGE_BUCKET)
        self.initialized = True

    @check_initialized
    def get(self, file_path, mimetype='image/png'):
        remote_path = file_path
        blob = self.bucket.blob(remote_path)
        if not blob.exists():
            return None
        with tempfile.NamedTemporaryFile() as temp:
            blob.download_to_filename(temp.name)
            return send_file(temp.name, attachment_filename=os.path.basename(file_path), mimetype=mimetype)

    @check_initialized
    def download(self, file_path):
        remote_path = file_path
        blob = self.bucket.blob(remote_path)
        if not blob.exists():
            return None
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        logging.info("Downloading %s" % remote_path)
        blob.download_to_filename(file_path)
        return file_path

    @check_initialized
    def put(self, file_path):
        remote_path = file_path
        blob = self.bucket.blob(remote_path)
        blob.upload_from_filename(file_path)

    @check_initialized
    def upload_all(self, folder, extensions=None):
        for (path, _, files) in os.walk(folder):
            for file in files:
                basename, ext = os.path.splitext(file)
                if extensions is None or ext.lower() in extensions:
                    file_path = os.path.join(path, file)
                    self.put(file_path)

    @check_initialized
    def download_all(self, folder, extensions=None):
        folder = os.path.join(folder, "")  # folder(prefix) must ends with '/'
        blobs = self.bucket.list_blobs(prefix=folder, delimiter='/')
        for blob in blobs:
            _, ext = os.path.splitext(blob.name)
            if extensions is None or ext.lower() in extensions:
                self.download(blob.name)

    @check_initialized
    def delete_all(self, folder):
        folder = os.path.join(folder, "")  # folder(prefix) must ends with '/'
        try:
            self.bucket.delete_blobs(blobs=self.bucket.list_blobs(prefix=folder))
        except Exception as e:
            print(e)
