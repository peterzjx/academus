import unittest
from app.convert import bibliography

class BibliographyTest(unittest.TestCase):
    def test_bbl_text_to_bib_text_1(self):
        text = r'''%merlin.mbs apsrev4-1.bst 2010-07-25 4.21a (PWD, AO, DPC) hacked
\begin{thebibliography}{67}%

\bibitem{Chung06}
\bibinfo{author}{Chung, S.~B.} \& \bibinfo{author}{Stone, M.}
\newblock \bibinfo{title}{{Proposal for reading out anyon qubits in non-Abelian
  $\ensuremath{\nu}=12/5$ quantum Hall state}}.
\newblock \emph{\bibinfo{journal}{Phys. Rev. B}} \textbf{\bibinfo{volume}{73}},
  \bibinfo{pages}{245311} (\bibinfo{year}{2006}).
\end{thebibliography}%'''
        processed = bibliography.bbl_text_to_bib_text(text)
        print(processed)
        self.assertGreaterEqual(processed.find('Chung06,'), 0)

    def test_bbl_text_to_bib_text_2(self):
        text = r'''\bibitem [{\citenamefont {Xu}\ \emph {et~al.}(2014)\citenamefont {Xu},
  \citenamefont {Han}, \citenamefont {Sun}, \citenamefont {Xu}, \citenamefont
  {Tang}, \citenamefont {Li},\ and\ \citenamefont {Guo}}]{2014Xu}%
  \BibitemOpen
  \bibfield  {author} {\bibinfo {author} {\bibfnamefont {X.-Y.} \bibnamefont
  {Xu}}, \bibinfo {author} {\bibfnamefont {Y.-J.}\ \bibnamefont {Han}},
  \bibinfo {author} {\bibfnamefont {K.}~\bibnamefont {Sun}}, \bibinfo {author}
  {\bibfnamefont {J.-S.}\ \bibnamefont {Xu}}, \bibinfo {author} {\bibfnamefont
  {J.-S.}\ \bibnamefont {Tang}}, \bibinfo {author} {\bibfnamefont {C.-F.}\
  \bibnamefont {Li}}, \ and\ \bibinfo {author} {\bibfnamefont {G.-C.}\
  \bibnamefont {Guo}},\ }\href {\doibase 10.1103/PhysRevLett.112.035701}
  {\bibfield  {journal} {\bibinfo  {journal} {Phys. Rev. Lett.}\ }\textbf
  {\bibinfo {volume} {112}},\ \bibinfo {pages} {035701} (\bibinfo {year}
  {2014})}\BibitemShut {NoStop}%'''
        processed = bibliography.bbl_text_to_bib_text(text)
        print(processed)


    def test_bbl_text_to_bib_text_bibitem_multiple_rargs(self):
        text = r'''\bibitem[{{Aird} {et~al.}(2010){Aird}, {Nandra}, {Laird}, {Georgakakis},
  {Ashby}, {Barmby}, {Coil}, {Huang}, {Koekemoer}, {Steidel}, \&
  {Willmer}}]{Aird2010}
{Aird}, J., {Nandra}, K., {Laird}, E.~S., {et~al.} 2010, \mnras, 401, 2531'''
        processed = bibliography.bbl_text_to_bib_text(text)
        print(processed)
        self.assertGreaterEqual(processed.find('Aird2010,'), 0)
        self.assertGreaterEqual(processed.find('{Aird}'), 0)

if __name__ == '__main__':
    unittest.main()