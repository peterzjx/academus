from bson import ObjectId
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import db_interface
from datetime import datetime


class DatabaseModel(dict):
    __getattr__ = dict.get
    __delattr__ = dict.__delitem__
    schema = dict()
    collection = None

    def __setattr__(self, key, value):
        if key not in [*self.schema, '__dict__']:
            raise KeyError('{} is not in the schema of {}'.format(key, type(self).__name__))
        if type(value) != self.schema[key]:
            raise TypeError('Type of {} ({}) does not match with {}'.format(value, type(value), self.schema[key]))
        super().__setitem__(key, value)

    def __setitem__(self, key, value):
        if key not in self.schema:
            raise KeyError('{} is not in the schema of {}'.format(key, type(self).__name__))
        if type(value) != self.schema[key]:
            raise TypeError('Type of {} ({}) does not match with {}'.format(value, type(value), self.schema[key]))

        super().__setitem__(key, value)

    def __init__(self, d):
        for key, value in d.items():
            if key not in self.schema:
                raise KeyError('{} is not in the schema of {}'.format(key, type(self).__name__))
            if type(value) != self.schema[key]:
                raise TypeError('Type of {} ({}) does not match with {}'.format(value, type(value), self.schema[key]))

        super().__init__(d)

    def save(self):
        if not self._id:
            self.collection.insert_one(self)
        else:
            self.collection.update({
                "_id": ObjectId(self._id)
            }, self)

    def reload(self):
        if self._id:
            self.update(self.collection.find_one({"_id": ObjectId(self._id)}))

    def remove(self):
        if self._id:
            self.collection.remove({"_id": ObjectId(self._id)})
            self.clear()

    def get_string_id(self):
        """
        :return:
        """
        return str(self['_id'])

    @classmethod
    def get_from_id(cls, id):
        if id is not None:
            response = cls.collection.find_one({'_id': ObjectId(id)})
            if response:
                return cls(**response)
        return None

    @classmethod
    def find_one(cls, **kwargs):
        response = cls.collection.find_one(kwargs)
        if response:
            return cls(**response)
        return None


class User(UserMixin, DatabaseModel):
    collection = db_interface.db.users
    schema = {
        '_id': ObjectId,
        'username': str,
        'email': str,
        'password_hash': str,
        'avatar_url': str
    }
    MAX_COLLECTION_NUM = 512

    def __init__(self, **kwargs):
        super().__init__(kwargs)

    def save(self):
        super().save()
        if not self.get_default_collection():
            self.create_collection("", is_default=True)

    def set_password(self, password):
        self['password_hash'] = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.get('password_hash'), password)

    def get_id(self):
        """
        overrides method in UserMixin, converts ObjectId to str
        :return:
        """
        return self.get_string_id()

    def get_all_collections(self):
        found_collections = Collection.collection.find({'user_id': self['_id']})
        return [Collection(**item) for item in found_collections]

    def get_collection(self, collection_id):
        found_collection = Collection.collection.find_one({'user_id': self['_id'],
                                                           '_id': ObjectId(collection_id)})
        if not found_collection:
            return None
        return Collection(**found_collection)

    def get_default_collection(self):
        """

        :return: the only default collection. None if it does not exist (for new user).
        """
        found_collection = Collection.collection.find_one({'user_id': self['_id'],
                                                           'is_default': True})
        if not found_collection:
            return None
        return Collection(**found_collection)

    def create_collection(self, name: str, is_default=False):
        """
        :param name: display name
        :param is_default: If true, "name" will be ignored.
        :return:
        """
        if is_default:
            default_collection = self.get_default_collection()
            if default_collection is not None:
                raise ValueError("Default collection already exists")
        collection = Collection(user_id=self['_id'],
                                name=name,
                                is_default=is_default,
                                articles=[])
        collection.save()

    def get_collection_from_id_and_verify(self, string_id):
        """
        Get Collection object from string_id, verify current user owns the collection.
        :param string_id:
        :return: Collection obj or None
        """
        target_collection = Collection.get_from_id(string_id)
        if not target_collection:
            return None
        if target_collection['user_id'] != self['_id']:
            return None
        return target_collection

    def remove_collection(self, string_id):
        """

        :param string_id:
        :return: True if remove is success, otherwise False
        """
        target_collection = self.get_collection_from_id_and_verify(string_id)
        if not target_collection:
            return False
        Collection.collection.delete_one(target_collection)
        return True

    def rename_collection(self, string_id, new_name):
        """

        :param string_id:
        :param new_name:
        :return: True if rename is success, otherwise False
        """
        target_collection = self.get_collection_from_id_and_verify(string_id)
        if not target_collection:
            return False
        target_collection.set_name(new_name)
        target_collection.save()
        return True

    def get_all_articles(self):
        """
        Iterate through all collections of the current user and concat the content
        :return:
        """
        pass

    @classmethod
    def create_new_user(cls, username, email, password):
        """
        Check if user email exists before creating a new user
        :param username:
        :param email:
        :param password:
        :return:
        """
        # check if user exists
        if cls.find_one(email=email):
            # User exists
            return False
        else:
            user = User(username=username,
                        email=email,
                        avatar_url="")
            user.set_password(password)
            user.save()
            return True


class Article(DatabaseModel):
    collection = db_interface.db.articles
    schema = {
        '_id': ObjectId,
        'title': str,
        'authors': list,  # str
        'article_name': str,
        'url': str,
        'time_added': str,  # TODO: change to datetime
        'tags': list,  # str, TODO: change to ObjectId
        'description': str
    }

    def __init__(self, **kwargs):
        super().__init__(kwargs)

    @classmethod
    def update_or_create_article(cls, article):
        article_found = Article.find_one(article_name=article['article_name'])
        if article_found:
            article_found.update(article)
            article_found.save()
        else:
            article.save()


class Collection(DatabaseModel):
    collection = db_interface.db.collections
    schema = {
        '_id': ObjectId,
        'user_id': ObjectId,
        'is_default': bool,
        'name': str,
        'articles': list,  # {id (article._id): ObjectId, 'time_added': datetime}
    }
    MAX_ARTICLE_NUM = 512
    MAX_NAME_LEN = 128

    def __init__(self, **kwargs):
        super().__init__(kwargs)
        if not self.articles:
            self.articles = []
        if not self.name:
            self.name = "default"

    def add_article(self, article: Article):
        '''
        :param article:
        :return: True if successfully added, False if article already exists or exceed maximum number
        '''
        if article['_id'] in [item['id'] for item in self.articles]:
            return False

        # TODO: return status
        if len(self.articles) >= self.MAX_ARTICLE_NUM:
            return False

        self.articles.append({'id': article['_id'], 'time_added': datetime.utcnow()})
        self.save()
        return True

    def remove_article(self, article: Article):
        '''
        :param article:
        :return: True if successfully removed, False if article is not found
        '''
        to_be_removed = None
        for item in self.articles:
            if article['_id'] == item['id']:
                to_be_removed = item
        if not to_be_removed:
            return False
        self.articles.remove(to_be_removed)
        self.save()
        return True

    def copy_article_to_collection(self, article: Article, other):
        pass

    def set_name(self, name):
        name = name[:self.MAX_NAME_LEN]
        self['name'] = name


if __name__ == '__main__':
    # article = Article(title='Adversarial learning of cancer tissue representations',
    #                   url='article/2108.02223/',
    #                   authors=['Adalberto Claudio Quiros', 'Nicolas Coudray', 'Anna Yeaton'])
    article = Article(title='Dynamics and control of automobiles using nonholonomic vehicle models',
                      url='article/2108.02230/',
                      authors=['Wubing B. Qin', 'Yiming Zhang', 'Dénes Takács'])
    article.save()