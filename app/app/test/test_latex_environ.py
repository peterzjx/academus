import unittest
from app.convert.latex import LatexProcessor


class LatexFigureTest(unittest.TestCase):
    def test_subfigure_brackets(self):
        content = r'''\begin{figure}
\subfigure[\label:]{\includegraphics[width=0.47\textwidth]{FS2}}~~~
\subfigure[\label:]{\includegraphics[width=0.47\textwidth]{FS}}
\caption{Schematics of the Fermi surface. Blue and red correspond to different
valleys. Regardless of band structure details, these Fermi surfaces
display two prominent features: the symmetry-imposed intersection
points between the Fermi surfaces from the two valleys (the valley
hot-spots, highlighted with black dots in panel (a)), and the proximity
to a van Hove singularity for appropriate doping levels (the van Hove
hot-spots, highlighted with grey dots in panel (b)). Valley hot-spots are located along the $\bar{\Gamma}-\bar{K}$ and $\bar{\Gamma}-\bar{K}'$ lines, whereas the van Hove hot-spots are located along the $\bar{\Gamma}-\bar{M}$ lines of the moir\'e Brillouin zone.}
\label{fig:1} 
\end{figure}'''
        print(LatexProcessor(None).process_one_figure(content))

    def test_subfigure(self):
        content = r'''\begin{figure}[ht]
\centering
\begin{subfigure}{.49\textwidth}
  \centering
  \includegraphics[width=0.5]{fig1}
  \caption{sub 1}
  \label{fig:pretraing_all_weights}
\end{subfigure}
\begin{subfigure}{.49\textwidth}
  \centering
  \includegraphics[width=0.5]{fig2}
  \label{fig:pretraing_emb_only}
\end{subfigure}

\begin{subfigure}{.49\textwidth}
  \centering
  \includegraphics[width=0.5]{fig3}
  \caption{sub 3}
  \label{fig:asdf}
\end{subfigure}
\caption{Total caption}
\label{total}
\end{figure}'''
        processed = LatexProcessor(None).process_one_figure(content)
        print(processed)
        self.assertGreaterEqual(processed.find('metadata'), 0)
        self.assertGreaterEqual(processed.find('Total caption'), 0)
        self.assertGreaterEqual(processed.find('fig1'), 0)
        self.assertGreaterEqual(processed.find('|fig:pretraing_all_weights'), 0)
        self.assertGreaterEqual(processed.find('fig2'), 0)
        self.assertGreaterEqual(processed.find('|fig:pretraing_emb_only'), 0)
        self.assertGreaterEqual(processed.find('fig3'), 0)
        self.assertGreaterEqual(processed.find('|fig:asdf'), 0)

    def test_subfigure2(self):
        content = r'''\begin{figure}[t]
\centering
{
\subfigure{
\includegraphics[width=1.6in]{fig2a.pdf}\label{fig:fig2a}}
}
{
\subfigure{
\includegraphics[width=1.6in]{fig2b.pdf}\label{fig:fig2b}}
}
\caption{Ordering wave vector (a) $Q/\pi$ and (b) $(Q-Q_{cl})/\pi$ versus $J^\prime$ for spin-1/2 system. The black, blue, and red lines show  results for parameters $(D,\Delta)$ equal to $(0,1)$, $(0.05,1)$ and $(0,0.95)$, respectively.}
\label{fig:fig2}
\end{figure}'''
        processed = LatexProcessor(None).process_one_figure(content)
        print(processed)
        self.assertGreaterEqual(processed.find('metadata'), 0)
        self.assertGreaterEqual(processed.find('Ordering wave vector'), 0)
        self.assertGreaterEqual(processed.find('fig2a.pdf'), 0)
        self.assertGreaterEqual(processed.find('fig2b.pdf'), 0)

    def test_subfloat(self):
        content = r'''\begin{figure}[ht]
  \subfloat[Percentage storage utilization]{
	\begin{minipage}[c][1\width]{
	   0.3\textwidth}
	   \centering
	   \includegraphics[width=1\textwidth]{fig1}
	\end{minipage}}
 \hfill 	
  \subfloat[standard deviation]{
	\begin{minipage}[c][1\width]{
	   0.3\textwidth}
	   \centering
	   \includegraphics[width=1.1\textwidth]{fig2}
	\end{minipage}}
 \hfill	
  \subfloat[execution time]{
	\begin{minipage}[c][1\width]{
	   0.3\textwidth}
	   \centering
	   \includegraphics[width=1.2\textwidth]{fig3}
	\end{minipage}}
\caption{2}
\end{figure}'''
        print(LatexProcessor(None).process_one_figure(content))

    def test_additional_layer(self):
        """
        1902.02015
        """
        content = r'''\begin{figure}[th]
	\begin{center} \includegraphics[width=7cm]{aa}\caption{ {\small The labels a1, {\em etc.} denote the appropriate sections in each configuration separated by the plates.}}
		\label{geometry}
	\end{center}
\end{figure}'''
        print(LatexProcessor(None).process_one_figure(content))

    def test_figure_with_label(self):
        """
        2009.09808
        """
        content = r'''\begin{figure}[t!]
  \centering
  \includegraphics[width=\linewidth]{fig/fig_sdfOperations.pdf}
  \vspace{-6 pt}
  \caption{\repName s admit many trivial interactive manipulations, the models predicted distance's can be modified through boolean operations similar to any implicit field. See accompanying video for animation.}
  \label{fig:sdfops}
  \vspace{-6 pt}
\end{figure}'''
        print(LatexProcessor(None).process_one_figure(content))


class LatexTableTest(unittest.TestCase):
    def test_nested_tabular(self):
        """
        2002.11710
        """
        content = r'''\begin{table*}[]
\centering
\caption{Summary of Total Missions Times.}
\label{fig_times_table_scheduling}
\begin{tabular}{|l|l|l|l|l|l|}
\hline
\begin{tabular}[c]{@{}l@{}}Number of\\ \textbf{Missions}\end{tabular} & \begin{tabular}[c]{@{}l@{}}\textbf{Gurobi}\\ \textbf{Runtime}\end{tabular} & \begin{tabular}[c]{@{}l@{}}\textbf{Neighbourhood Search}\\ \textbf{Runtime}\end{tabular} & \begin{tabular}[c]{@{}l@{}}\textbf{Parallel Neighbourhood}\\ \textbf{Search Runtime}\end{tabular} & \begin{tabular}[c]{@{}l@{}}\textbf{Tabu Search}\\ \textbf{Runtime}\end{tabular} & \begin{tabular}[c]{@{}l@{}}\textbf{Parallel Tabu}\\ \textbf{Search Runtime}\end{tabular} \\ \hline
12                          & 1226                    & 0.0153                        & 0.548                                  & 0.0245                       & 0.578                                 \\ \hline
15                          & 2045                    & 0.0244                        & 0.532                                  & 0.0349                       & 0.615                                 \\ \hline
18                          & 2815                    & 0.0322                        & 0.544                                  & 0.0424                       & 0.575                                 \\ \hline
21                          & 5486                    & 0.0446                        & 0.519                                  & 0.0553                       & 0.507                                 \\ \hline
24                          & 9546                    & 0.0499                        & 0.611                                  & 0.0618                       & 0.642                                 \\ \hline
27                          & N/A                     & 0.0565                        & 0.618                                  & 0.0675                       & 0.561                                 \\ \hline
30                          & N/A                     & 0.0581                        & 0.599                                  & 0.0682                       & 0.645                                 \\ \hline
33                          & N/A                     & 0.0631                        & 0.686                                  & 0.0689                       & 0.611                                 \\ \hline
\end{tabular}
\end{table*}'''
        processed = LatexProcessor(None).process_one_table(content)
        self.assertEqual(1, processed.count(r'\begin{tabular}'))
        self.assertEqual(1, processed.count(r'\end{tabular}'))

    def test_multi_nested_tabular(self):
        """
        2108.04981
        """
        content = r'''\begin{table}
\begin{tabular}{llcc} 
\hline
\textbf{Category~}      & \textbf{Self-Assignment Factors} & \begin{tabular}[c]{@{}c@{}}\textbf{\begin{tabular}[c]{@{}c@{}}Devs\\ Prefs\end{tabular}}\end{tabular} & \textbf{\begin{tabular}[c]{@{}c@{}}Mgrs\\ Prefs\end{tabular}}\\
\hline
\multirow[t]{8}{*}{Task-based}      & Task's business priority      & \faCheck    & \faCheck  \\
                               & Task learning potential       & \faCheck    & \faCheck \\
                               & Technical complexity          & \faCheck    &      \\
                               & Task visibility  impact       & \faCheck    &      \\
                               & Task dependency               & \faCheck    & \faCheck \\
                               & Completion time               & \faCheck    & \faCheck  \\
                               & Understandability             & \faCheck    & \faCheck  \\
                               & Task desirability             & \faCheck    &      \\
\multirow[t]{5}{*}{Developer-based} & Previous experience           & \faCheck    & \faCheck  \\
                               & Technical expertise  ability  & \faCheck    & \faCheck  \\
                               & Resource availability         & \faCheck    &      \\
                               & Co-workers deference          & \faCheck    &      \\
                               & Co-workers preference         & \faCheck    &      \\
\multirow[t]{2}{*}{Opinion-based}   & Opinion of managers           & \faCheck    &      \\
                               & Opinion of team members       & \faCheck    &    \\ \hline
\multicolumn{4}{l}{Preferences \textbf{Prefs}, Developers' \textbf{Devs}, Managers' \textbf{Mgrs}} \\
\hline
\end{tabular}
\end{table}'''
        processed = LatexProcessor(None).process_one_table(content)
        self.assertEqual(1, processed.count(r'\begin{tabular}'))
        self.assertEqual(1, processed.count(r'\end{tabular}'))

    def test_nested_tabular_one_row(self):
        content = r'''\begin{table}[t]

\setlength\tabcolsep{1.5pt}
\caption{Common configuration error types and example constraints that lead to errors upon violation.}
\label{tab:misconfig-95types}
\vspace{-0.1in}
\begin{tabular}{cl}
\toprule
\begin{tabular}{c}Error type\end{tabular}                    & \multicolumn{1}{c}{Example configuration constraint}                                                                                                                                                                                                                 \\ \midrule
\multirow{2}{*}{Illegal entries}                                                   & 
\begin{tabular}{l} In PostgreSQL, parameter values that are not simple\\identifiers or numbers must be single-quoted.\end{tabular} \\ \cmidrule(l){2-2} 
& Variables must be in certain types (e.g., float). \\ \midrule
\multirow{2}{*}[-2em]{\begin{tabular}{c}Inconsistent\\entries\end{tabular}}
& \begin{tabular}{l} In PHP, \texttt{mysql.max\_persistent} must be no\\larger than the \texttt{max\_connections} in MySQL. \vspace{-0.05in}\end{tabular} \\ \cmidrule(l){2-2} 
& \begin{tabular}{l}In Cloudshare, service's \texttt{redis.host} entry\\(an IP address) must be a substring of Nginx's \\ \texttt{upstream.msg.server} entry (IP address:port).\end{tabular} \\ \midrule
\begin{tabular}{c} Invalid \\ ordering \end{tabular}
& \begin{tabular}{l} When using PHP in Apache, \texttt{recode.so} must\\be defined before \texttt{mysql.so}. \end{tabular} \\ \midrule
\multirow{2}{*}[-1em]{\begin{tabular}[c]{@{}c@{}}Environmental\\inconsistency\end{tabular}}
& \begin{tabular}{l} In MySQL, maximum allowed table size must be\\smaller than the memory available in the system \end{tabular} \\ \cmidrule(l){2-2} 
& \begin{tabular}{l} In httpd, Apache user permissions must be set\\correctly to enable file uploads for website visitors. \end{tabular} \\ \midrule
\begin{tabular}{c} Missing \\ parameter \end{tabular}
& \begin{tabular}{l} In OpenLDAP, a configuration entry must include\\\texttt{ppolicy.schema} to enable password policy. \end{tabular} \\ \midrule
\multirow{2}{*}[1em]{\begin{tabular}{c} Valid entries \Tstrut\\ that cause \\ performance\\or security\\issues \end{tabular}}
& \begin{tabular}{l} MySQL's \texttt{Autocommit} parameter must be set to\\ \texttt{False} to avoid poor performance under ``insert''\\intensive workloads. \end{tabular} \\ \cmidrule(l){2-2}
& \begin{tabular}{l} Debug-level logging must be disabled to avoid\\performance degradation.  \end{tabular} \\ \bottomrule
\end{tabular}
\vspace{-0.15in}
\end{table}'''
        processed = LatexProcessor(None).process_one_table(content)
        print(processed)

    def test_tabularx(self):
        content = r'''\begin{table}[t]
\caption {Low-temperature magnetic order (ferromagnetic, FM, or antiferromagnetic, AF) and stacking symmetry (rhombohedral or monoclinic) in bulk and multilayer chromium trihalides CrX\3 as experimentally reported in the literature.
\label{tab:exp}}
\vspace{2mm}
{\setcitestyle{super}
\begin{tabularx}{\textwidth}{l >{\arraybackslash}X
>{\arraybackslash}X >{\arraybackslash}X }
\toprule
 & CrCl\3 & CrBr\3 & CrI\3 \\
\cmidrule{2-4}
\multirow{2}{*}{bulk} & AF\cite{Cable1961,McGuire2017}  &  FM\cite{Tsubokawa1960} & FM\cite{McGuire2015}  \\
                                 & rhomb.\cite{Morosin1964} &  rhomb.\cite{Samuelsen1971} & rhomb.\cite{McGuire2015,Djurdjic2018} \\
\midrule
\multirow{2}{*}{multilayers} &AF\cite{Cai2019,Wang2019,Klein2019,Kim2019} & FM\cite{Ghazaryan2018,Kim2019hall,Kim2019}  &   AF\cite{Huang2017,Song2018,Klein2018,Wang2018,Kim2018} \\
& monocl.\cite{Klein2019} &  -- &   monocl.\cite{Sun2019,Ubrig2019}\\
\bottomrule
\end{tabularx}}
\end{table}'''
        processed = LatexProcessor(None).process_one_table(content)
        print(processed)

    def test_tabular_in_arg_of_label(self):
        # TODO: TexSoup has an error parsing "\right)$"
        content = r'''\begin{table}
\centering
\caption{Special cases of the  Tricomi confluent hypergeometric function. In this table, $K_a$ is modified Bessel function of the second kind, that is, $K_{\alpha}(x)=\frac{\pi}{2} \frac{I_{-\alpha}(x)-I_{\alpha}(x)}{\sin \alpha x}$, where $I_{\alpha}(x)=\sum_{m=0}^{\infty} \frac{1}{m! \Gamma (m+\alpha +1)} (\frac{x}{2})^{2m+\alpha}$ is the modified Bessel functions of the first kind.} \label{tricomi}
{\renewcommand{\arraystretch}{2.7}	
\begin{tabular} {| l ||c  |}
 \hline
        & $U(\frac{1}{2},\frac{k+1}{2},z)$    \\ \hline \hline
$k=1$   & $U(\frac{1}{2},1,z)=\frac{e^{\frac{z}{2}}K_0(\frac{z}{2})}{\sqrt{\pi}}$            \\ \hline
$k=2$   & $U(\frac{1}{2},\frac{3}{2},z)=\frac{1}{\sqrt{z}}$   \\ \hline
$k=3$   & $U(\frac{1}{2},2,z)=\frac{e^{\frac{z}{2}}\left( K_0(\frac{z}{2}) +K_1(\frac{z}{2})\right)}{2\sqrt{\pi}}$             \\ \hline
$k=4$   & $U(\frac{1}{2},\frac{5}{2},z)=\frac{2z+1}{2z^{\frac{3}{2}}}$    \\ \hline
$k=5$   & $U(\frac{1}{2},3,z)
=\frac{e^{\frac{z}{2}}\left( z K_0(\frac{z}{2}) +(z+1)K_1(\frac{z}{2})\right)}{2\sqrt{\pi}z}$            \\ \hline
$k=6$   & $U(\frac{1}{2},\frac{7}{2},z)=\frac{4z(z+1)+3}{4z^{\frac{5}{2}}}$   \\ \hline
$k=7$   & $U(\frac{1}{2},4,z)=\frac{e^{\frac{z}{2}}\left( z(2z+1) K_0(\frac{z}{2}) +(z(2z+3)+4)K_1(\frac{z}{2})\right)}{4\sqrt{\pi}z^2}$             \\ \hline
$k=8$   & $U(\frac{1}{2},\frac{9}{2},z)=\frac{2z(4z^2+6z+9)+15}{8z^{\frac{7}{2}}}$   \\ \hline
$k=9$   & $U(\frac{1}{2},5,z)=\frac{e^{\frac{z}{2}}\left( z(2z(z+1)+3) K_0(\frac{z}{2}) +2(z(z(z+2)+4)+6)K_1(\frac{z}{2})\right)}{4\sqrt{\pi}z^3}$            \\ \hline
$k=10$  & $U(\frac{1}{2},\frac{11}{2},z)=
\frac{8z(z(2z(z+2)+9)+15)+105}{16z^{\frac{9}{2}}}$   \\ \hline
$k=11$  & $U(\frac{1}{2},6,z)=
\frac{e^{\frac{z}{2}}\left( z(z(4z^2+6z+15)+24) K_0(\frac{z}{2}) +(z(z(2z(2z+5)+27)+60)+96)K_1(\frac{z}{2})\right)}{8\sqrt{\pi}z^4}
$            \\ \hline
\end{tabular}
}
\end{table}
'''
        processed = LatexProcessor(None).process_one_table(content)
        print(processed)

if __name__ == '__main__':
    unittest.main()
