import logging
import os
import re

from TexSoup import TexSoup, data

from app.convert import bibliography
from app import util
from app.convert.mathparser import MathParser
from app.convert.interface import ImageConvertLatexSnippet


def escape_regex_special_char(s):
    '''
    Escape special chars when generate regex pattern with external strings
    :param s: string to be processed
    :return: string processed
    '''
    escaped_char = r"[\^$.|?*+()_"
    for char in escaped_char:
        s = s.replace(char, "\\" + char)
    return s


def replace_texnode_with(old, new):
    '''
    :param old: TexNode
    :param new: String
    :return: new node
    '''
    # TODO: optimize
    # Texsoup bug workaround
    new = re.sub(r'\\def{\\}{(.*?)}', r'\\def\\\1', new, flags=re.DOTALL)
    if old.parent.args:
        for i, arg in enumerate(old.parent.args):
            # 1. Need to access _contents as calling contents[j] invokes the getter of _contents, not the setter.
            # 2. If written as enumerate(arg.contents), getter would remove empty space items in _contents, causing the
            #    index to mismatch
            for j, item in enumerate(arg._contents):
                if str(item) == str(old):
                    old.parent.expr.args[i]._contents[j] = TexSoup(new)
                    return old.parent.expr.args[i]._contents[j]
    new_node = TexSoup(new)
    old.replace_with(new_node)
    return new_node


class BracketMatcher:
    def __init__(self, content):
        self.content = content

    def match(self, prefix="", left='{', right='}', start=0):
        # return the range between 'prefix+left' (exclusive) and the corresponding 'right' (inclusive)
        # for example, \author{aaaa{bbbb}aaaa} would return (start=8, end=22)
        # TODO: support multiple matches
        start = self.content.find(prefix + left, start)
        if start == -1:
            return -1, -1
        start += len(prefix + left)
        nested_level = 1
        i = start
        end = start
        while i < len(self.content):
            left_pos = self.content.find(left, i)
            right_pos = self.content.find(right, i)
            if right_pos == -1:  # unmatched right bracket
                return -1, -1
            elif left_pos == -1 or right_pos < left_pos:  # close one level
                nested_level -= 1
                i = right_pos + len(right)
                if nested_level == 0:
                    end = i
                    break
            else:  # nest one level
                nested_level += 1
                i = left_pos + len(left)
        if nested_level != 0:
            return -1, -1
        return start, end - len(right)


class LatexProcessorV2:
    def __init__(self, meta):
        self.label_mapping = dict()
        self.theorem_like_environments = {'theorem', 'lemma', 'corollary', 'proposition', 'definition'}
        self.bibtexts = []
        self.meta = meta
        self.columns = 1
        self.filename_matcher = FilenameMatcher(self.meta) if meta else None
        self.image_convert_latex_snippets = []  # list of ImageConvertLatexSnippet
        self.current_path = ""
        self.preamble = ""
        self.label_replace_map = {}

    def recursive_read(self, input_path, ancestor_chain: set):
        """
        text content of the included file, expanded with all children includes
        :param input_path:
        :param ancestor_chain: the trace of all ancestors of this file, to prevent cyclical inclusion
        :return: string
        """
        if input_path in ancestor_chain:
            logging.error(f"Input file {input_path} has cyclical inclusion chain, aborting")
            return ""
        if not os.path.exists(input_path):
            logging.error(f"Input file {input_path} does not exist, ignored")
            return ""
        logging.info(f"Input {input_path}")
        ancestor_chain.add(input_path)
        content = util.read_file(input_path)
        # avoid files that are commented out
        content = self.remove_comments(content)
        # change \\input tex.tex to \\input{tex.tex}
        content = re.sub(r'\\input\s+(.*?)\s', lambda m: r"\input{%s}" % m.group(1), content)
        content = re.sub(r'\\include\s*{(.*?)}\s', lambda m: r"\input{%s}" % m.group(1), content)

        # actual read
        content = re.sub(r"\\input\s*?{(.*?)}",
                         lambda m: self.recursive_read(self.get_full_path(m.group(1)), ancestor_chain),
                         content)
        ancestor_chain.remove(input_path)
        return content

    def process_main_file(self, input_path):
        self.current_path = input_path
        output_path = input_path
        logging.info("Pre-processing tex file " + str(input_path))
        backup_path = input_path + '.bak'
        if not os.path.exists(backup_path):
            os.rename(input_path, backup_path)
        input_path = backup_path
        content = self.recursive_read(input_path, set())
        content = self.process_full_content(content)
        f = open(output_path, 'w', encoding='utf-8')
        f.write(content)
        f.close()

    def process_meta(self):
        """
        process all main files with the input of all tex files
        :return: None
        """
        for main_file in self.meta.main_files:
            file_path = os.path.join(self.meta.article_folder, main_file)
            self.process_main_file(file_path)

    def get_full_path(self, name):
        if not name.endswith('.tex'):
            name += '.tex'
        return os.path.join(self.meta.article_folder, name)

    def process_full_content(self, full_content):
        """
        Process the content of a single file, populate self.preamble and return a full content, and:
        1. TODO: move abstract into content
        2. TODO: move graphicspath into preamble
        :param full_content:
        :return:
        """
        full_content = self.remove_comments(full_content)
        self.preamble, content = self.split_preamble_content(full_content)
        self.preamble = self.process_preamble(self.preamble)
        bbl_text, content = self.extract_bibliography(content)
        if bbl_text != "":
            self.bibtexts.append(bibliography.bbl_text_to_bib_text(bbl_text))
        content = self.process_body(content)
        return self.preamble + content

    def split_preamble_content(self, full_content):
        """
        Split preamble with content
        :param full_content:
        :return: preamble, content
        """
        begin_document_pos = full_content.find('\\begin{document}')
        if begin_document_pos >= 0:
            preamble = full_content[:begin_document_pos]
            content = full_content[begin_document_pos:]
            return preamble, content
        else:
            return "", full_content

    def process_body(self, content):
        if content.find('twocolumn') >= 0:
            self.columns = 2
        content = self.simple_text_replace(content)
        content = self.process_environment(content)
        if self.label_replace_map:
            logging.info("Changing duplicating labels", self.label_replace_map)
            for key, val in self.label_replace_map:
                content = content.replace("{" + key + "}", "{" + val + "}")
        if self.bracket_unbalanced_number(content) != 0:
            content = self.try_match_brackets(content)
        return content

    @staticmethod
    def bracket_unbalanced_number(text):
        text = re.sub(r"\\.", "", text)
        return text.count('{') - text.count('}')

    def try_match_brackets(self, content):
        p_begin_end = re.compile(r"(\\begin *{|\\end *{)")
        p_section = re.compile(r"\\(sub)*?section")
        p_other = re.compile(r"(\\maketitle|\\title|\\author|\\abstract|\n\n)")
        ps = [p_begin_end, p_section, p_other]
        all_anchors = []
        for p in ps:
            for match in p.finditer(content):
                all_anchors.append(match.start())
        correction = 0
        insert_points = []
        for anchor in sorted(all_anchors):
            current_unbalanced_number = self.bracket_unbalanced_number(content[:anchor])
            if current_unbalanced_number - correction > 0:
                while current_unbalanced_number - correction > 0:
                    insert_points.append(anchor)
                    correction += 1
            elif current_unbalanced_number - correction < 0:
                while current_unbalanced_number - correction < 0 and len(insert_points) > 0:
                    c = insert_points.pop()
                    correction -= 1
            if current_unbalanced_number - correction < 0 and len(insert_points) == 0:
                correction = current_unbalanced_number
        if insert_points:
            logging.info("Missing '}', added at %s" % " ".join(
                ["pos " + str(i) + " before \"" + content[i: i + 16].replace('\n', '\\n') + "\"," for i in
                 insert_points]
            )
                         )
        for anchor in reversed(insert_points):
            content = content[:anchor] + '}' + content[anchor:]
        return content

    @staticmethod
    def extract_bibliography(content):
        pattern = r"(\\begin{(thebibliography|mcitethebibliography)}.*?\\end{(thebibliography|mcitethebibliography)})"
        bibliographies = re.findall(pattern, content, flags=re.DOTALL)
        if not bibliographies:
            logging.info("No bibliography found at the end of the article")
            return "", content
        if len(bibliographies) > 1:
            logging.warning("More than 1 bibliographies found within the tex file, returning the first one")
        content = re.sub(pattern, "", content, flags=re.DOTALL)
        logging.info("Found bibliography within the tex file, length %s" % len(bibliographies[0][0]))
        return bibliographies[0][0], content

    @staticmethod
    def remove_comments(content):
        content = re.sub(r"(?<!\\)%(.*?)$\n?", "", content, flags=re.MULTILINE)  # remove % except \%
        content = re.sub(r"\\\\%(.*?)$\n?", r"\\\\", content, flags=re.MULTILINE)  # remove \\%
        return content

    def simple_text_replace(self, content):
        def substitude_label(label):
            if label in self.label_mapping:
                processed = util.random_name()
            else:
                processed = util.change_special_char(label)
                self.label_mapping[label] = processed
            return r'\label{%s}' % processed

        '''graphicspath'''
        # content = re.sub(r'\\graphicspath.*?\n', "", content)
        # reverted 20220821

        '''firstpage'''
        content = re.sub(r'\\label{(first|last)page}', "", content)
        '''verb'''
        # TODO: implement this
        # content = self.regularize_verb(content)
        # TODO: regularize author
        '''additional header for theorems'''
        begin_document_pos = content.find(r'\begin{document}')
        try:
            header = util.read_file(os.path.join(util.CONVERT_PATH, "header.tex"))
            if begin_document_pos >= 0:
                content = content[:begin_document_pos] + header + content[begin_document_pos:]
        except:
            logging.error('Header.tex not found, ignore')
        '''DeclareRobustCommand can't have number in their names'''
        robust_commands = re.findall(r'\\(DeclareRobustCommand|DeclareMathOperator|DeclareMathOperator\*) *{(.*?)}',
                                     content)
        for matched in robust_commands:
            original_command = escape_regex_special_char(matched[1])
            modified_command = util.change_number(original_command)
            if original_command != modified_command:
                # \b (word boundary) only works here after the command
                content = re.sub(original_command + r'\b', modified_command, content)
        '''section label'''
        # TODO: refactor this horrible regex
        content = re.sub(
            r'\\((sub)*section|appendix){([^{}]*?|[^{}]*?{[^{}]*?}[^{}]*?)\\label{([^#{]*?|[^#]*?{[^#]*?}[^#]*?)}([^{}]*?|[^{}]*?{[^{}]*?}[^{}]*?)}',
            self.regularize_section_label,
            content)
        # Remove extra spaces between section and label
        # TODO: label can appear anywhere in the section and still be pointing at the section.
        # TODO: can't handle section name containing \n, as FLAG.dotall can't be used here
        content = re.sub(r'(\\((sub)*section|appendix){.*?})\s*?(\\label{.*?})',
                         lambda m: m.group(1) + " " + m.group(4), content)
        section_labels = re.findall(r'\\((sub)*section|appendix){.*?} \\label{(.*?)}', content)
        # TODO: Use bracketmatcher to handle section title containing {}
        if len(section_labels) > 0:
            pattern = r'{(%s)}' % "|".join([escape_regex_special_char(matched[2]) for matched in section_labels])
            content = re.sub(pattern, lambda m: "{%s}" % (util.SEC_PREFIX + m.group(1)), content)
        '''cite'''
        """temp fix for illegal chars in citations, for pandoc to work"""
        # TODO: remove nocite
        pattern = r'\\(online|no)?[cC]ite[pPtT]?(\[.*?\])?(?={)'
        content = self.substitude_with_bracket_matching(pattern, content,
                                                        lambda head, left, inner, right:
                                                        r'\cite{%s}' % self.process_citation_label(inner)
                                                        )

        '''remove unmatched paracol latex hack'''
        content = re.sub(r'\\begin{paracol}({\d*})?', '', content)
        content = re.sub(r'\\end{paracol}', '', content)

        '''remove titleformat'''
        content = re.sub(r'\\titleformat.*?\n', '', content)

        content = re.sub(r'\\autoref', r'\\ref', content)
        content = re.sub(r'\\cref{(.*?)}', lambda m: self.split_cref_labels(m.group(1)), content)
        '''fix of \ref followed by newline'''
        content = re.sub(r'\\ref\s*{', r'\\ref{', content)

        '''fix of \string'''
        content = re.sub(r'\\string_', '\\_', content)

        content = re.sub(r'\\checkmark', '✔', content)

        '''{\\verb ...} without || delimiter not supported in pandoc. Temp fix'''
        # TODO: proper iterate through all text
        content = re.sub(r'{\\verb([^a-zA-Z0-9]*?)}', lambda m: '{\\verb|%s|}' % m.group(1).replace('|', ""), content)
        content = re.sub(r'\\strut', '', content)

        '''replace {[} to avoid texsoup/pandoc unmatched bracket problem'''
        content = re.sub(r'{\[}', '[', content)
        content = re.sub(r'{]}', ']', content)

        '''Big letter in beginning of paragraph'''
        content = re.sub(r'\\IEEEPARstart', '', content)

        '''remove newcolumntype'''  # todo: fix the usage of such type
        content = re.sub(r'\\newcolumntype.*?#1.*?$', "", content, flags=re.MULTILINE)

        '''replace special chars in labels to avoid pandoc problem'''
        '''exclude #, because it can appear in newcommand definition'''
        # TODO: markdown module searches for all ref again.
        # Updated 20230326: moved to markdown filter, check if this can be deprecated
        pattern = r'\\label{([^#{]*?|[^#]*?{[^#]*?}[^#]*?)}'
        current_file_labels = re.findall(pattern, content)
        logging.debug("Labels found in current file:" + str(current_file_labels))

        # TODO: add cref prefix support here

        # TODO: better logic handling here
        candidate_labels = [label for label in set(current_file_labels) if label not in section_labels]
        content = re.sub(r'\\label{(%s)}' % "|".join([escape_regex_special_char(label) for label in candidate_labels]),
                         lambda m: substitude_label(m.group(1)), content)

        if len(self.label_mapping) > 0:
            # (?<!]) ensures {LABEL} is not following a ']', which is the case in includegraphics[]{FILE}.
            # this is to handle FILE and LABEL are duplicated names and both contains chars like _
            pattern = r'(?<!]){(%s)}' % "|".join([escape_regex_special_char(label) for label in self.label_mapping])
            content = re.sub(pattern, lambda m: '{%s}' % self.label_mapping[m.group(1)], content)

        ''' includegraphics*'''
        content = content.replace(r"\includegraphics*", r"\includegraphics")

        '''width'''
        content = content.replace(r"\linewidth", r"\textwidth")
        content = content.replace(r"\columnwidth", r"\textwidth")
        content = content.replace(r"\hsize", r"\textwidth")

        '''group'''

        def replace_after(content, replace_from, replace_to, pos):
            if pos == -1:
                return content.replace(replace_from, replace_to)
            return content[:pos] + content[pos:].replace(replace_from, replace_to)

        begin_document_pos = content.find('\\begin{document}')
        content = replace_after(content, r"\bgroup", r"{", begin_document_pos)
        content = replace_after(content, r"\begingroup", r"{", begin_document_pos)
        content = replace_after(content, r"\egroup", r"}", begin_document_pos)
        content = replace_after(content, r"\endgroup", r"}", begin_document_pos)

        '''temp fix of char code'''
        content = replace_after(content, r'\\char`\\', '', begin_document_pos)
        content = replace_after(content, r'\\char`', '', begin_document_pos)

        return content

    @staticmethod
    def process_citation_label(content):
        '''content is the label text'''
        content = content.replace('$', '-dollar-')  # temp fix, will be fixed in pandoc 2.14.1
        content = re.sub(r'\[.*\]', '', content, flags=re.DOTALL)  # greedy match for nested []
        content = content.replace('{', '').replace('}', '')
        # Handles ',' appearing as the first char
        content = ",".join(filter(None, content.split(",")))
        return content

    @staticmethod
    def regularize_section_label(m):
        label = '\\label{' + m.group(4) + '}'
        content = m.group(0).replace(label, "")
        content += label
        return content

    @staticmethod
    def process_section_label(m):
        content = m.group(0).replace(m.group(4), util.SEC_PREFIX + m.group(4))
        return content

    @staticmethod
    def split_cref_labels(content):
        # TODO: support prefixes and cref ranges
        """
        cref support multiple labels separated by comma. Split them into multiple refs
        :param content: inner content of "\cref{content}"
        :return: "\ref{label1}\ref{label2}"
        """
        labels = content.split(",")
        return " ".join("\\ref{%s}" % label for label in labels if label)

    @staticmethod
    def move_abstract_after_begin_document(content):
        begin_document_string = '\\begin{document}'
        begin_document_pos = content.find(begin_document_string)
        abstract_pos = content.find('\\begin{abstract}')
        if begin_document_pos == -1 or abstract_pos == -1:
            return content
        if abstract_pos < begin_document_pos:
            match = re.search(r'\\begin{abstract}.*?\\end{abstract}', content, flags=re.DOTALL)
            if match:
                abstract = match.group()
                insert_point = begin_document_pos + len(begin_document_string)
                content = content[:insert_point] + '\n' + abstract + content[insert_point:]
        return content

    @staticmethod
    def substitude_with_bracket_matching(pattern, string, replace_func, left='{', right='}'):
        """
            replace_func takes 4 args:
            matched pattern, left, bracket_content, right
            returns a string that replaces the entire pattern + left + bracket_content + right
        """
        matcher = BracketMatcher(string)
        last_anchor = 0
        buffer = []
        for m in re.finditer(pattern, string):
            buffer.append(string[last_anchor:m.start()])
            start, end = matcher.match(prefix="", left=left, right=right, start=m.start() + len(m.group()))
            if start != m.start() + len(m.group()) + len(left):
                logging.error("Bracket matcher meets unexpected character.")
                buffer.append(string[m.start():end])
                last_anchor = end
                continue
            buffer.append(replace_func(m.group(), left, string[start:end], right))
            last_anchor = end + len(right)
        buffer.append(string[last_anchor:])
        return "".join(buffer)

    @staticmethod
    def regularize_verb(content):
        """
        search for all \\verb* * pairs and replace the character * to |
        :param content:
        :return:
        """
        # TODO
        return content

    @staticmethod
    def remove_environment(content, environment):
        try:
            result = re.sub(r'\\begin{%s}.*?\\end{%s}' % (environment, environment), "", content, flags=re.DOTALL)
            if result != content:
                logging.info("TexSoup is removing environment %s" % environment)
            return result
        except Exception as e:
            logging.error(e)
            logging.error("TexSoup encounters an error in removing the environment %s, falling back." % environment)
            return content

    @staticmethod
    def rename_environment(content, env_from, env_to):
        '''
        :param content:
        :param env_from: regex pattern, environment name, e.g. figure
        :param env_to: environment name
        :return: content
        '''

        def rename_helper(inner):
            if re.findall(r'\\begin *{%s}' % env_to, inner):  # avoid nested duplicate-named envs
                logging.error('Nested duplicated environment name %s.' % env_to)
                return r'\begin{%s}' % env_from.replace("\\", "") + inner + r'\end{%s}' % env_from.replace("\\", "")
            return r'\begin{%s}' % env_to.replace("\\", "") + inner + r'\end{%s}' % env_to.replace("\\", "")

        try:
            result = re.sub(r'\\begin *{%s}(.*?)\\end *{%s}' % (env_from, env_from),
                            lambda m: rename_helper(m.group(1)),
                            content, flags=re.DOTALL)
            if result != content:
                logging.info("TexSoup is renaming environment %s to %s" % (env_from, env_to))
            return result
        except Exception as e:
            logging.error(e)
            logging.error(
                "TexSoup encounters an error in renaming the environment %s to %s, falling back." % (env_from, env_to))
            return content

    @staticmethod
    def strip_environment(content, environment):
        try:
            # TODO: Apply formatting to comments
            result = re.sub(r'\\begin *{%s}(.*?)\\end *{%s}' % (environment, environment), r"\1", content,
                            flags=re.DOTALL)
            # Removing unpaired environment as failsafe
            result = re.sub(r'\\begin *{%s}' % environment, "", result)
            result = re.sub(r'\\end *{%s}' % environment, "", result)
            return result
        except Exception as e:
            logging.error(e)
            logging.error("TexSoup encounters an error in stripping the environment %s, falling back." % environment)
            return content

    def process_environment(self, content):
        try:
            # TexSoup will eat a space after backslash
            content = re.sub(r"(?<!\\)\\ ", "\\;", content)  # avoid \_ except \\_ (here _ represents a space)
            content = re.sub(r"(?<!\\)\\\n", "\\;\n", content)

            # renaming environments
            content = self.rename_environment(content, "wrapfigure", "figure")
            content = self.rename_environment(content, "wraptable", "table")
            content = self.rename_environment(content, "mini", "verbatim")
            content = self.remove_environment(content, "comment")
            content = self.strip_environment(content, "keywords")

            # Other variants of tables and figures
            # replace {x*} environment with x as star has different meaning in latex and mathjax
            content = self.rename_environment(content, r"figure\*", "figure")
            content = self.rename_environment(content, r"SCfigure", "figure")
            content = self.rename_environment(content, r"sidewaysfigure", "figure")
            content = self.rename_environment(content, r"table\*", "table")
            content = self.rename_environment(content, r"SCtable", "table")
            content = self.rename_environment(content, r"sidewaystable", "table")
            '''Override default hide numbering behavior'''
            content = self.rename_environment(content, r"tabular\*", "tabular")

            ''' replace unsupported math environments. https://github.com/jgm/pandoc/issues/4337'''
            content = self.rename_environment(content, r"flalign\*", "align")
            content = self.rename_environment(content, "flalign", "align")

            logging.info("TexSoup is regularizing the figures")
            content = re.sub(r'\\begin{figure}.*?\\end{figure}',
                             lambda m: self.process_one_figure(m.group(0)),
                             content, flags=re.DOTALL)  # still can't deal with newcommand'ed figure environments
            content = re.sub(r'\\begin{minipage}.*?\\end{minipage}',
                             lambda m: self.process_one_latex_figure(m.group(0)),
                             content, flags=re.DOTALL)
            # TODO: minipage inside figure results to nested figure env.
            content = re.sub(r'\\begin{algorithm}.*?\\end{algorithm}',
                             lambda m: self.process_one_latex_figure(m.group(0)),
                             content, flags=re.DOTALL)
            logging.info("TexSoup is regularizing the tables")
            content = re.sub(r'\\begin{table}.*?\\end{table}', lambda m: self.process_one_table(m.group(0)),
                             content, flags=re.DOTALL)
            content = re.sub(r'\\begin{tikzpicture}.*?\\end{tikzpicture}',
                             lambda m: self.process_one_latex_figure(m.group(0)),
                             content, flags=re.DOTALL)
            '''Put at last as all previous environments fall back to verbatim'''
            content = re.sub(r'\\begin{verbatim}.*?\\end{verbatim}', lambda m: self.process_one_verbatim(m.group(0)),
                             content, flags=re.DOTALL)

            '''Fix TexSoup def bug'''
            content = re.sub(r'\\def{\\}{(.*?)}', r'\\def\\\1', content, flags=re.DOTALL)
            return content
        except Exception as e:
            logging.info("TexSoup can't parse the tex file, falling back, Error is", str(e))
            return content

    def process_one_figure(self, content):
        has_drawing: bool = False
        for trigger in util.FIGURE_ENVIRONMENT_DRAWING_TRIGGERS:
            if content.find(trigger) >= 0:
                has_drawing = True
                break
        if has_drawing:
            logging.info("Figure contains drawings")
            return self.process_one_latex_figure(content)
        try:
            # for two columns page, the parent width is set to self.columns, i.e. 200% so that a 0.5\\textwidth image
            # will now be displayed as 100%
            latex_figure_processor = LatexFigureProcessor(content, self.columns, self.filename_matcher)
            fig_soup = latex_figure_processor.process()
            self.label_replace_map = {**self.label_replace_map, **latex_figure_processor.label_replace_map}
            return str(fig_soup)
        except Exception as e:
            logging.error("TexSoup can't parse the figure.", str(e))
            return content

    def process_one_latex_figure(self, content):
        # value in figure_meta contains the command header "\\caption" and "\\label"
        figure_meta = {'caption': "", 'label': "\\label{%s}" % util.random_name()}
        try:
            soup = TexSoup(content)
            caption_node = soup.find('caption')
            if caption_node:
                caption_node, _ = split_caption_label(caption_node)
                caption_node = regularize_caption_node(caption_node)
            label_node = soup.find('label')
            if soup.find('label'):
                figure_meta['label'] = str(label_node)
                soup.find('label').delete()
            if soup.find('caption'):
                figure_meta['caption'] = str(caption_node)
                soup.find('caption').delete()
            content = str(soup)
            '''Fix TexSoup def bug'''
            content = re.sub(r'\\def{\\}{(.*?)}', r'\\def\\\1', content, flags=re.DOTALL)
        except Exception as e:
            logging.error("TexSoup can't parse tikzfigure, falling back.", str(e))
            return content
        return self.submit_convert_snippet_and_replace(content, figure_meta)

    def submit_convert_snippet_and_replace(self, content, figure_meta):
        id = util.random_name()
        output_latex = []
        output_filename = os.path.join(util.LATEX_GENERATED_IMAGE_FOLDER, id + '.png')
        logging.info("Replacing content %s... with generated figure %s" % (content[:16], id))
        self.image_convert_latex_snippets.append(
            ImageConvertLatexSnippet(
                id=id,
                content=content,
                path=self.current_path,
                preamble=self.preamble
            )
        )
        output_latex.append(r'\includegraphics[width=0.8\textwidth]{%s}' % output_filename)
        if figure_meta['caption']:
            output_latex.append(figure_meta['caption'])
        if figure_meta['label']:
            output_latex.append(figure_meta['label'])
        return r'\begin{figure}%s\end{figure}' % "".join(output_latex)

    def process_one_table(self, content):
        fallback_to_latex: bool = False
        for trigger in util.TABLE_ENVIRONMENT_DRAWING_TRIGGERS:
            if content.find(trigger) >= 0:
                fallback_to_latex = True
                break
        # for trigger in ("\\begin{tabular}",):
        #     if content.find(trigger) < 0:
        #         fallback_to_latex = True
        #         break
        if fallback_to_latex:
            logging.info("Table contains drawings, fallback to latex")
            return self.process_one_latex_figure(content)
        try:
            content = re.sub(r'\n\s*\n', '\n', content, re.MULTILINE)  # remove whitespace lines
            tab_soup = TexSoup(content)
            tab_soup = LatexTableProcessor(tab_soup).process()
            # return "\\begin{verbatim}\n%NOPARSE\n" + str(tab_soup) + "\n\\end{verbatim}"
            return str(tab_soup)
        except Exception as e:
            logging.error("TexSoup can't parse the table", str(e))
            return self.rename_environment(content, "table", "verbatim")

    @staticmethod
    def process_one_verbatim(content):
        """Avoid insert \begin{equation} where it should be inline math"""
        return content.replace("$$", "$ $")

    def generate_image_convert_requests(self):
        if not self.image_convert_latex_snippets:
            return
        logging.info("Appending %d image convert requests to article meta" % len(self.image_convert_latex_snippets))
        for snippet in self.image_convert_latex_snippets:
            target_folder, _ = os.path.split(snippet.path)
            full_tex = util.latex_template % (snippet.preamble, snippet.content)
            fallback_tex = util.tikz_template % snippet.content
            try:
                with open(os.path.join(target_folder, snippet.id), 'w') as file:
                    file.write(full_tex)
                with open(os.path.join(target_folder, snippet.id + '_fallback'), 'w') as file:
                    file.write(fallback_tex)
                self.meta.latex_image_requests.append(
                    os.path.relpath(os.path.join(target_folder, snippet.id), self.meta.article_folder))
            except Exception as e:
                logging.error("Error in writing image to file %s" % snippet.id)
                logging.error(e)
        self.meta.store()

    def process_bbl_file(self, input_path):
        self.bibtexts.append(bibliography.bbl_file_to_bib_text(self.meta, input_path))

    def has_bbl_text_or_file(self):
        return len(self.bibtexts) > 0

    def generate_bib_file(self):
        bibliography.bib_text_to_bib_file(self.meta, "\n".join(self.bibtexts))

    def clean_nested_tabular(self, string):
        if string.find("tabular") >= 0:
            soup = TexSoup(string)
            for node in soup.find_all('tabular'):
                node.args = data.TexArgs()
                replace_texnode_with(node,
                                     self.clean_nested_tabular(LatexTableProcessor.clean_tabular_environment(node)))
            return str(soup)
        else:
            return string

    def process_preamble(self, preamble):
        # latex compatibility hack
        preamble = preamble.replace("revtex4-2", "revtex4-1")
        # pandoc does not recognize \global\long\def
        preamble = preamble.replace(r"\global\long\def", r"\def")
        # def with number followed by space
        preamble = re.sub(r'(\\def.*?\d) +(?={)', lambda m: m.group(1), preamble)
        # remove extra space after newcommand*
        preamble = re.sub(r'\\newcommand\* +', r'\\newcommand\*', preamble)
        # remove newcolumntype # todo: fix the usage of such type
        preamble = re.sub(r'\\newcolumntype.*?#1.*?$', "", preamble, flags=re.MULTILINE)
        # nested tabular defined within newcommand
        pattern = r'\\newcommand{?[^{]*?}?(\[.*?\])+'
        preamble = self.substitude_with_bracket_matching(pattern, preamble,
                                                         lambda head, left, inner, right:
                                                         head + left + self.clean_nested_tabular(inner) + right
                                                         )
        '''graphicspath'''
        preamble = re.sub(r'\\graphicspath.*?\n', "", preamble)
        return preamble


# class LatexProcessor:
#     """Pre-process the latex file to ensure pandoc can read without crashing"""
#
#     def __init__(self, meta):
#         self.all_label_mapping = dict()
#         self.all_theorem_like_environments = {'theorem', 'lemma', 'corollary', 'proposition', 'definition'}
#         self.all_bibtexts = []
#         self.meta = meta
#         self.columns = 1
#         self.filename_matcher = FilenameMatcher(self.meta) if meta else None
#         self.image_convert_latex_snippets = []
#         self.current_path = ""
#
#     @staticmethod
#     def bracket_unbalanced_number(text):
#         text = re.sub(r"\\.", "", text)
#         return text.count('{') - text.count('}')
#
#     def try_match_brackets(self, content):
#         # p_begin_end = re.compile(r"(?=\\begin{(.*?)}.*?\\end{\1})", flags=re.DOTALL)
#         p_begin_end = re.compile(r"(\\begin *{|\\end *{)")
#         p_section = re.compile(r"\\(sub)*?section")
#         p_other = re.compile(r"(\\maketitle|\\title|\\author|\\abstract|\n\n)")
#         # p_eq = re.compile(r"(\$|\$\$|\\\[|\\]|)")
#         ps = [p_begin_end, p_section, p_other]
#         all_anchors = []
#         begin_document_pos = content.find('\\begin{document}')
#         for p in ps:
#             for match in p.finditer(content):
#                 if match.start() >= begin_document_pos:
#                     all_anchors.append(match.start())
#         correction = 0
#         insert_points = []
#         for anchor in sorted(all_anchors):
#             current_unbalanced_number = self.bracket_unbalanced_number(content[:anchor])
#             if current_unbalanced_number - correction > 0:
#                 while current_unbalanced_number - correction > 0:
#                     insert_points.append(anchor)
#                     correction += 1
#             elif current_unbalanced_number - correction < 0:
#                 while current_unbalanced_number - correction < 0 and len(insert_points) > 0:
#                     c = insert_points.pop()
#                     correction -= 1
#             if current_unbalanced_number - correction < 0 and len(insert_points) == 0:
#                 correction = current_unbalanced_number
#         if insert_points:
#             logging.info("Missing '}', added at %s" % " ".join(
#                 ["pos " + str(i) + " before \"" + content[i: i + 16].replace('\n', '\\n') + "\"," for i in
#                  insert_points]
#             )
#                          )
#         for anchor in reversed(insert_points):
#             content = content[:anchor] + '}' + content[anchor:]
#         return content
#
#     @staticmethod
#     def extract_bibliography(content):
#         pattern = r"(\\begin{(thebibliography|mcitethebibliography)}.*?\\end{(thebibliography|mcitethebibliography)})"
#         # TODO: support mcitethebibliography
#         bibliographies = re.findall(pattern, content, flags=re.DOTALL)
#         if not bibliographies:
#             logging.info("No bibliography found at the end of the article")
#             return "", content
#         if len(bibliographies) > 1:
#             logging.warning("More than 1 bibliographies found within the tex file, returning the first one")
#         content = re.sub(pattern, "", content, flags=re.DOTALL)
#         logging.info("Found bibliography within the tex file, length %s" % len(bibliographies[0][0]))
#         return bibliographies[0][0], content
#
#     @staticmethod
#     def remove_comments(content):
#         content = re.sub(r"(?<!\\)%(.*?)\n", "", content)  # remove % except \%
#         content = re.sub(r"\\\\%(.*?)\n", r"\\\\", content)  # hack with \\%
#         return content
#
#     @staticmethod
#     def process_citation_label(content):
#         '''content is the label text'''
#         content = content.replace('$', '-dollar-')  # temp fix, will be fixed in pandoc 2.14.1
#         content = re.sub(r'\[.*\]', '', content, flags=re.DOTALL)  # greedy match for nested []
#         content = content.replace('{', '').replace('}', '')
#         # Handles ',' appearing as the first char
#         content = ",".join(filter(None, content.split(",")))
#         return content
#
#     @staticmethod
#     def regularize_section_label(m):
#         label = '\\label{' + m.group(4) + '}'
#         content = m.group(0).replace(label, "")
#         content += label
#         return content
#
#     @staticmethod
#     def process_section_label(m):
#         content = m.group(0).replace(m.group(4), util.SEC_PREFIX + m.group(4))
#         return content
#
#     @staticmethod
#     def split_cref_labels(content):
#         # TODO: support prefixes and cref ranges
#         """
#         cref support multiple labels separated by comma. Split them into multiple refs
#         :param content: inner content of "\cref{content}"
#         :return: "\ref{label1}\ref{label2}"
#         """
#         labels = content.split(",")
#         return " ".join("\\ref{%s}" % label for label in labels if label)
#
#     @staticmethod
#     def move_abstract_after_begin_document(content):
#         begin_document_string = '\\begin{document}'
#         begin_document_pos = content.find(begin_document_string)
#         abstract_pos = content.find('\\begin{abstract}')
#         if begin_document_pos == -1 or abstract_pos == -1:
#             return content
#         if abstract_pos < begin_document_pos:
#             match = re.search(r'\\begin{abstract}.*?\\end{abstract}', content, flags=re.DOTALL)
#             if match:
#                 abstract = match.group()
#                 insert_point = begin_document_pos + len(begin_document_string)
#                 content = content[:insert_point] + '\n' + abstract + content[insert_point:]
#         return content
#
#     @staticmethod
#     def substitude_with_bracket_matching(pattern, string, replace_func, left='{', right='}'):
#         """
#             replace_func takes 4 args:
#             matched pattern, left, bracket_content, right
#             returns a string that replaces the entire pattern + left + bracket_content + right
#         """
#         matcher = BracketMatcher(string)
#         last_anchor = 0
#         buffer = []
#         for m in re.finditer(pattern, string):
#             buffer.append(string[last_anchor:m.start()])
#             start, end = matcher.match(prefix="", left=left, right=right, start=m.start() + len(m.group()))
#             if start != m.start() + len(m.group()) + len(left):
#                 logging.error("Bracket matcher meets unexpected character.")
#                 buffer.append(string[start:end])
#                 last_anchor = end
#                 continue
#             buffer.append(replace_func(m.group(), left, string[start:end], right))
#             last_anchor = end + len(right)
#         buffer.append(string[last_anchor:])
#         return "".join(buffer)
#
#     @staticmethod
#     def regularize_verb(content):
#         """
#         search for all \\verb* * pairs and replace the character * to |
#         :param content:
#         :return:
#         """
#         # TODO
#         return content
#
#     def simple_text_replace(self, content):
#         def substitude_label(label):
#             if label in self.all_label_mapping:
#                 processed = util.random_name()
#             else:
#                 processed = util.change_special_char(label)
#                 self.all_label_mapping[label] = processed
#             return r'\label{%s}' % processed
#
#         def clean_nested_tabular(string):
#             if string.find("tabular") >= 0:
#                 soup = TexSoup(string)
#                 for node in soup.find_all('tabular'):
#                     node.args = data.TexArgs()
#                     replace_texnode_with(node, clean_nested_tabular(LatexTableProcessor.clean_tabular_environment(node)))
#                 return str(soup)
#             else:
#                 return string
#
#         '''change \\input tex.tex to \\input{tex.tex}'''
#         content = re.sub(r'\\input\s+(.*?)\s', lambda m: r"\input{%s}" % m.group(1), content)
#         '''graphicspath'''
#         content = re.sub(r'\\graphicspath.*?\n', "", content)
#         '''firstpage'''
#         content = re.sub(r'\\label{(first|last)page}', "", content)
#         '''verb'''
#         # TODO: implement this
#         # content = LatexProcessor.regularize_verb(content)
#         # TODO: regularize author
#         '''additional header for theorems'''
#         begin_document_pos = content.find(r'\begin{document}')
#         try:
#             header = util.read_file(os.path.join(util.CONVERT_PATH, "header.tex"))
#             if begin_document_pos >= 0:
#                 content = content[:begin_document_pos] + header + content[begin_document_pos:]
#         except:
#             logging.error('Header.tex not found, ignore')
#         '''DeclareRobustCommand can't have number in their names'''
#         robust_commands = re.findall(r'\\(DeclareRobustCommand|DeclareMathOperator|DeclareMathOperator\*) *{(.*?)}',
#                                      content)
#         for matched in robust_commands:
#             original_command = escape_regex_special_char(matched[1])
#             modified_command = util.change_number(original_command)
#             if original_command != modified_command:
#                 # \b (word boundary) only works here after the command
#                 content = re.sub(original_command + r'\b', modified_command, content)
#         '''section label'''
#         # TODO: refactor this horrible regex
#         content = re.sub(
#             r'\\((sub)*section|appendix){([^{}]*?|[^{}]*?{[^{}]*?}[^{}]*?)\\label{([^#{]*?|[^#]*?{[^#]*?}[^#]*?)}([^{}]*?|[^{}]*?{[^{}]*?}[^{}]*?)}',
#             self.regularize_section_label,
#             content)
#         # Remove extra spaces between section and label
#         # TODO: label can appear anywhere in the section and still be pointing at the section.
#         # TODO: can't handle section name containing \n, as FLAG.dotall can't be used here
#         content = re.sub(r'(\\((sub)*section|appendix){.*?})\s*?(\\label{.*?})',
#                          lambda m: m.group(1) + " " + m.group(4), content)
#         section_labels = re.findall(r'\\((sub)*section|appendix){.*?} \\label{(.*?)}', content)
#         # TODO: Use bracketmatcher to handle section title containing {}
#         if len(section_labels) > 0:
#             pattern = r'{(%s)}' % "|".join([escape_regex_special_char(matched[2]) for matched in section_labels])
#             content = re.sub(pattern, lambda m: "{%s}" % (util.SEC_PREFIX + m.group(1)), content)
#         '''cite'''
#         """temp fix for illegal chars in citations, for pandoc to work"""
#         # TODO: remove nocite
#         pattern = r'\\(online|no)?[cC]ite[pPtT]?(\[.*?\])?(?={)'
#         content = self.substitude_with_bracket_matching(pattern, content,
#                                                         lambda head, left, inner, right:
#                                                         r'\cite{%s}' % self.process_citation_label(inner)
#                                                         )
#
#         '''remove unmatched paracol latex hack'''
#         content = re.sub(r'\\begin{paracol}({\d*})?', '', content)
#         content = re.sub(r'\\end{paracol}', '', content)
#         '''remove titleformat'''
#         content = re.sub(r'\\titleformat.*?\n', '', content)
#
#         content = re.sub(r'\\autoref', r'\\ref', content)
#         content = re.sub(r'\\cref{(.*?)}', lambda m: self.split_cref_labels(m.group(1)), content)
#         '''fix of \ref followed by newline'''
#         content = re.sub(r'\\ref\s*{', r'\\ref{', content)
#
#         '''fix of \string'''
#         content = re.sub(r'\\string_', '\\_', content)
#
#         content = re.sub(r'\\checkmark', '✔', content)
#
#         '''{\\verb ...} without || delimiter not supported in pandoc. Temp fix'''
#         # TODO: proper iterate through all text
#         content = re.sub(r'{\\verb([^a-zA-Z0-9]*?)}', lambda m: '{\\verb|%s|}' % m.group(1).replace('|', ""), content)
#         content = re.sub(r'\\strut', '', content)
#
#         '''replace {[} to avoid texsoup/pandoc unmatched bracket problem'''
#         content = re.sub(r'{\[}', '[', content)
#         content = re.sub(r'{]}', ']', content)
#
#         '''remove extra space after newcommand*'''
#         content = re.sub(r'\\newcommand\* +', r'\\newcommand\*', content)
#
#         '''remove newcolumntype''' # todo: fix the usage of such type
#         content = re.sub(r'\\newcolumntype.*?#1.*?$', "", content, flags=re.MULTILINE)
#
#         '''nested tabular defined within newcommand'''
#         pattern = r'\\newcommand{.*?}\[.*?\](\[.*?\])?'
#         content = self.substitude_with_bracket_matching(pattern, content,
#                                                         lambda head, left, inner, right:
#                                                         head + left + clean_nested_tabular(inner) + right
#                                                         )
#
#         '''replace special chars in labels to avoid pandoc problem'''
#         '''exclude #, because it can appear in newcommand definition'''
#         # TODO: markdown module searches for all ref again. Maybe move these two together
#         pattern = r'\\label{([^#{]*?|[^#]*?{[^#]*?}[^#]*?)}'
#         current_file_labels = re.findall(pattern, content)
#         logging.debug("Labels found in current file:" + str(current_file_labels))
#
#         # TODO: add cref prefix support here
#
#         # TODO: better logic handling here
#         candidate_labels = [label for label in set(current_file_labels) if label not in section_labels]
#         content = re.sub(r'\\label{(%s)}' % "|".join([escape_regex_special_char(label) for label in candidate_labels]),
#                          lambda m: substitude_label(m.group(1)), content)
#
#         if len(self.all_label_mapping) > 0:
#             # (?<!]) ensures {LABEL} is not following a ']', which is the case in includegraphics[]{FILE}.
#             # this is to handle FILE and LABEL are duplicated names and both contains chars like _
#             pattern = r'(?<!]){(%s)}' % "|".join([escape_regex_special_char(label) for label in self.all_label_mapping])
#             content = re.sub(pattern, lambda m: '{%s}' % self.all_label_mapping[m.group(1)], content)
#
#         ''' includegraphics*'''
#         content = content.replace(r"\includegraphics*", r"\includegraphics")
#
#         '''width'''
#         content = content.replace(r"\linewidth", r"\textwidth")
#         content = content.replace(r"\columnwidth", r"\textwidth")
#         content = content.replace(r"\hsize", r"\textwidth")
#
#         '''group'''
#
#         def replace_after(content, replace_from, replace_to, pos):
#             if pos == -1:
#                 return content.replace(replace_from, replace_to)
#             return content[:pos] + content[pos:].replace(replace_from, replace_to)
#
#         begin_document_pos = content.find('\\begin{document}')
#         content = replace_after(content, r"\bgroup", r"{", begin_document_pos)
#         content = replace_after(content, r"\begingroup", r"{", begin_document_pos)
#         content = replace_after(content, r"\egroup", r"}", begin_document_pos)
#         content = replace_after(content, r"\endgroup", r"}", begin_document_pos)
#
#         '''temp fix of char code'''
#         content = replace_after(content, r'\\char`\\', '', begin_document_pos)
#         content = replace_after(content, r'\\char`', '', begin_document_pos)
#
#         return content
#
#     @staticmethod
#     def remove_environment(content, environment):
#         try:
#             result = re.sub(r'\\begin{%s}.*?\\end{%s}' % (environment, environment), "", content, flags=re.DOTALL)
#             if result != content:
#                 logging.info("TexSoup is removing environment %s" % environment)
#             return result
#         except Exception as e:
#             logging.error(e)
#             logging.error("TexSoup encounters an error in removing the environment %s, falling back." % environment)
#             return content
#
#     @staticmethod
#     def rename_environment(content, env_from, env_to):
#         '''
#         :param content:
#         :param env_from: regex pattern, environment name, e.g. figure
#         :param env_to: environment name
#         :return: content
#         '''
#
#         def rename_helper(inner):
#             if re.findall(r'\\begin *{%s}' % env_to, inner):  # avoid nested duplicate-named envs
#                 logging.error('Nested duplicated environment name %s.' % env_to)
#                 return r'\begin{%s}' % env_from + inner + r'\end{%s}' % env_from
#             return r'\begin{%s}' % env_to + inner + r'\end{%s}' % env_to
#
#         try:
#             result = re.sub(r'\\begin *{%s}(.*?)\\end *{%s}' % (env_from, env_from),
#                             lambda m: rename_helper(m.group(1)),
#                             content, flags=re.DOTALL)
#             if result != content:
#                 logging.info("TexSoup is renaming environment %s to %s" % (env_from, env_to))
#             return result
#         except Exception as e:
#             logging.error(e)
#             logging.error(
#                 "TexSoup encounters an error in renaming the environment %s to %s, falling back." % (env_from, env_to))
#             return content
#
#     @staticmethod
#     def strip_environment(content, environment):
#         try:
#             # TODO: Apply formatting to comments
#             result = re.sub(r'\\begin *{%s}(.*?)\\end *{%s}' % (environment, environment), r"\1", content,
#                             flags=re.DOTALL)
#             # Removing unpaired environment as failsafe
#             result = re.sub(r'\\begin *{%s}' % environment, "", result)
#             result = re.sub(r'\\end *{%s}' % environment, "", result)
#             return result
#         except Exception as e:
#             logging.error(e)
#             logging.error("TexSoup encounters an error in stripping the environment %s, falling back." % environment)
#             return content
#
#     def process_environment(self, content):
#         try:
#             # TexSoup will eat a space after backslash
#             content = re.sub(r"(?<!\\)\\ ", "\\;", content)  # avoid \_ except \\_ (here _ represents a space)
#             content = re.sub(r"(?<!\\)\\\n", "\\;\n", content)
#
#             # renaming environments
#             content = self.rename_environment(content, "wrapfigure", "figure")
#             content = self.rename_environment(content, "wraptable", "table")
#             content = self.rename_environment(content, "mini", "verbatim")
#             content = self.remove_environment(content, "comment")
#             content = self.strip_environment(content, "keywords")
#             # content = self.strip_environment(content, "minipage")  # Should handle in figure and tables
#
#             # Other variants of tables and figures
#             # replace {x*} environment with x as star has different meaning in latex and mathjax
#             content = self.rename_environment(content, r"figure\*", "figure")
#             content = self.rename_environment(content, r"SCfigure", "figure")
#             content = self.rename_environment(content, r"sidewaysfigure", "figure")
#             content = self.rename_environment(content, r"table\*", "table")
#             content = self.rename_environment(content, r"SCtable", "table")
#             content = self.rename_environment(content, r"sidewaystable", "table")
#             '''Override default hide numbering behavior'''
#             content = self.rename_environment(content, r"tabular\*", "tabular")
#
#             ''' replace unsupported math environments. https://github.com/jgm/pandoc/issues/4337'''
#             content = self.rename_environment(content, r"flalign\*", "align")
#             content = self.rename_environment(content, "flalign", "align")
#
#             logging.info("TexSoup is regularizing the figures")
#             content = re.sub(r'\\begin{figure}.*?\\end{figure}',
#                              lambda m: self.process_one_figure(m.group(0)),
#                              content, flags=re.DOTALL)  # still can't deal with newcommand'ed figure environments
#             content = re.sub(r'\\begin{algorithm}.*?\\end{algorithm}',
#                              lambda m: self.process_one_tikzfigure(m.group(0)),
#                              content, flags=re.DOTALL)
#             logging.info("TexSoup is regularizing the tables")
#             content = re.sub(r'\\begin{table}.*?\\end{table}', lambda m: self.process_one_table(m.group(0)),
#                              content, flags=re.DOTALL)
#             content = re.sub(r'\\begin{tikzpicture}.*?\\end{tikzpicture}',
#                              lambda m: self.process_one_tikzfigure(m.group(0)),
#                              content, flags=re.DOTALL)
#             '''Put at last as all previous environments fall back to verbatim'''
#             content = re.sub(r'\\begin{verbatim}.*?\\end{verbatim}', lambda m: self.process_one_verbatim(m.group(0)),
#                              content, flags=re.DOTALL)
#
#             '''Fix TexSoup def bug'''
#             content = re.sub(r'\\def{\\}{(.*?)}', r'\\def\\\1', content, flags=re.DOTALL)
#             return content
#         except Exception as e:
#             logging.info("TexSoup can't parse the tex file, falling back, Error is", str(e))
#             return content
#
#     def process_one_figure(self, content):
#         has_drawing: bool = False
#         for trigger in util.FIGURE_ENVIRONMENT_DRAWING_TRIGGERS:
#             if content.find(trigger) >= 0:
#                 has_drawing = True
#                 break
#         if has_drawing:
#             logging.info("Figure contains drawings")
#             # TODO: need to process caption and label attached
#             return self.process_one_tikzfigure(content)
#         try:
#             fig_soup = TexSoup(content)
#             # for two columns page, the parent width is set to self.columns, i.e. 200% so that a 0.5\\textwidth image
#             # will now be displayed as 100%
#             fig_soup = LatexFigureProcessor(fig_soup, self.columns, self.filename_matcher).process()
#             return str(fig_soup)
#         except Exception as e:
#             logging.error("TexSoup can't parse the figure.", str(e))
#             return content
#
#     def process_one_tikzfigure(self, content):
#         # value in figure_meta contains the command header "\\caption" and "\\label"
#         figure_meta = {'caption': "", 'label': "\\label{%s}" % util.random_name()}
#         try:
#             soup = TexSoup(content)
#             caption_node = soup.find('caption')
#             if caption_node:
#                 caption_node, _ = split_caption_label(caption_node)
#                 caption_node = regularize_caption_node(caption_node)
#             label_node = soup.find('label')
#             if soup.find('label'):
#                 figure_meta['label'] = str(label_node)
#                 soup.find('label').delete()
#             if soup.find('caption'):
#                 figure_meta['caption'] = str(caption_node)
#                 soup.find('caption').delete()
#             content = str(soup)
#             '''Fix TexSoup def bug'''
#             content = re.sub(r'\\def{\\}{(.*?)}', r'\\def\\\1', content, flags=re.DOTALL)
#         except Exception as e:
#             logging.error("TexSoup can't parse tikzfigure, falling back.", str(e))
#             return content
#         return self.replace_snippet_with_image_convert_placeholder(content, figure_meta)
#
#     def replace_snippet_with_image_convert_placeholder(self, content, figure_meta):
#         id = util.random_name()
#         output_latex = []
#         output_filename = os.path.join(util.LATEX_GENERATED_IMAGE_FOLDER, id + '.png')
#         logging.info("Replacing content %s... with generated figure %s" % (content[:16], id))
#         self.image_convert_latex_snippets.append(
#             {'id': id,
#              'content': content,
#              'path': self.current_path}
#         )
#         output_latex.append(r'\includegraphics[width=0.8\textwidth]{%s}' % output_filename)
#         if figure_meta['caption']:
#             output_latex.append(figure_meta['caption'])
#         if figure_meta['label']:
#             output_latex.append(figure_meta['label'])
#         return r'\begin{figure}%s\end{figure}' % "".join(output_latex)
#
#     def process_one_table(self, content):
#         fallback_to_latex: bool = False
#         for trigger in util.TABLE_ENVIRONMENT_DRAWING_TRIGGERS:
#             if content.find(trigger) >= 0:
#                 fallback_to_latex = True
#                 break
#         # for trigger in ("\\begin{tabular}",):
#         #     if content.find(trigger) < 0:
#         #         fallback_to_latex = True
#         #         break
#         if fallback_to_latex:
#             logging.info("Table contains drawings, fallback to latex")
#             return self.process_one_tikzfigure(content)
#         try:
#             content = re.sub(r'\n\s*\n', '\n', content, re.MULTILINE)  # remove whitespace lines
#             tab_soup = TexSoup(content)
#             tab_soup = LatexTableProcessor(tab_soup).process()
#             return str(tab_soup)
#         except Exception as e:
#             logging.error("TexSoup can't parse the table", str(e))
#             return self.rename_environment(content, "table", "verbatim")
#
#     @staticmethod
#     def process_one_verbatim(content):
#         '''Avoid insert \begin{equation} where it should be inline math'''
#         return content.replace("$$", "$ $")
#
#     def process(self, input_path):
#         self.current_path = input_path
#         output_path = input_path
#         logging.info("Pre-processing tex file " + str(input_path))
#         backup_path = input_path + '.bak'
#         if not os.path.exists(backup_path):
#             os.rename(input_path, backup_path)
#         input_path = backup_path
#         content = util.read_file(input_path)
#         if content.find('twocolumn') >= 0:
#             self.columns = 2
#         content = self.remove_comments(content)
#         biblio_text, content = self.extract_bibliography(content)
#         if biblio_text != "":
#             self.all_bibtexts.append(bibliography.bbl_text_to_bib_text(biblio_text))
#         content = self.simple_text_replace(content)
#         content = self.process_environment(content)
#         if self.bracket_unbalanced_number(content) != 0:
#             content = self.try_match_brackets(content)
#         f = open(output_path, 'w', encoding='utf-8')
#         f.write(content)
#         f.close()
#
#     def generate_image_convert_requests(self):
#         if not self.image_convert_latex_snippets:
#             return
#         logging.info("Appending %d image convert requests to article meta" % len(self.image_convert_latex_snippets))
#         for item in self.image_convert_latex_snippets:
#             id = item['id']
#             content = item['content']
#             path = item['path']
#             target_folder, _ = os.path.split(path)
#             full_tex = util.tikz_template % content
#             try:
#                 with open(os.path.join(target_folder, id), 'w') as file:
#                     file.write(full_tex)
#                 self.meta.latex_image_requests.append(
#                     os.path.relpath(os.path.join(target_folder, id), self.meta.article_folder))
#             except Exception as e:
#                 logging.error("Error in writing image to file %s" % id)
#                 logging.error(e)
#         self.meta.store()
#
#     def process_bbl_file(self, input_path):
#         self.all_bibtexts.append(bibliography.bbl_file_to_bib_text(self.meta, input_path))
#
#     def has_bbl_text_or_file(self):
#         return len(self.all_bibtexts) > 0
#
#     def generate_bib_file(self):
#         bibliography.bib_text_to_bib_file(self.meta, "\n".join(self.all_bibtexts))


def get_width_value_from_string(s):
    """
    :param s: latex width string (e.g. width=0.5\\textwidth, or 0.5\\textwidth)
    :return: numeric value for width (in units of textwidth), or None if no width specified
    """
    s = s.replace(" ", "")
    matched = re.search(r'(?:width=)?"?(\d*\.?\d*)(%|cm|in|\\textwidth)?"?', s)
    if not matched:
        return None
    try:
        # default width 5.92 in == 15 cm
        value = matched.group(1)
        unit = matched.group(2)
        if value == "":
            value = "1"

        if unit == 'cm':
            width = float(value) / 15.0
        elif unit == 'in':
            width = float(value) / 5.92
        elif unit == '%':
            width = float(value) / 100
        else:
            width = float(value)
        return width
    except:
        return None


def regularize_width_string(s, parent_width=1.0):
    width = get_width_value_from_string(s)
    if width is not None:
        width *= parent_width
        width = min(width, 1.0)
        return "width=%s\\textwidth" % "{:.2f}".format(width)
    else:
        return ""


def regularize_unbalanced_math_brackets(s):  # todo: use bracket matcher
    if s.count("[") != s.count("]"):
        return s.replace("[", "[\\vphantom{]}")
    return s


def regularize_caption_node(node):
    # Delete the [] in \caption[]{}
    extra_string = ""
    for arg in node.args:
        if isinstance(arg, data.BracketGroup):
            extra_string += arg.string
            node.args.remove(arg)
    node.string = re.sub(r"\s*\n", "", node.string, flags=re.MULTILINE)
    if extra_string != "":
        extra_string += " "
    node.string = extra_string + node.string
    if node.string == "":
        node.string = "No caption."
    return node


def split_caption_label(node):
    '''
    Split caption node with label (i.e. \\caption{something \\label{somelabel}}) to (caption_with_no_label, label)
    :param node: caption node
    :return: node, label_node
    '''
    if node.label is None:
        return node, None
    label_node = node.label.copy()
    label_node.args = data.TexArgs([label_node.args[0]])
    # This handles the rare case of \label{LABEL} {some other text} being parsed as two arguments
    # https://github.com/alvinwan/TexSoup/issues/73
    if len(label_node.args) > 1:
        replace_texnode_with(node.label, str(node.label.args[1:]))
    else:
        node.label.delete()
    return node, label_node


class FigurePack:
    def __init__(self, id):
        self.fig = None
        self.caption = None
        self.label = None
        self.id = id

    def empty(self):
        return self.fig is None and self.caption is None and self.label is None

    def pack_up(self):
        if not self.caption or len(self.caption.args) == 0:
            self.caption = TexSoup(r'\caption{}').caption
        if not self.label or len(self.label.args) == 0:  # rare cases where \label does not have an argument
            self.label = TexSoup(r'\label{}').label
        self.caption.string = re.sub(r'\n+', '\n', self.caption.string)
        self.label.string = self.label.string
        return self


class LatexFigureProcessor:
    def __init__(self, content, parent_width, filename_matcher):
        content = self.pre_process(content)
        self.soup = TexSoup(content)
        self.parent_width = parent_width
        self.filename_matcher = filename_matcher
        self.id = util.random_name()
        self.label = None
        self.default_label = TexSoup(r'\label{%s}' % self.id).label
        self.caption = None
        self.default_caption = TexSoup(r'\caption{No caption}').caption
        self.label_replace_map = {}

        self.current = FigurePack(self.id)
        self.fig_list = []

    def pre_process(self, content):
        # def patch_unmatched_brackets(math):
        #     if math.count("[") == math.count("]"):
        #         return math
        # content = re.sub(r"$(.*?)$", lambda m: patch_unmatched_brackets(m.group(0)), content)
        return content

    def pack(self, fig_pack):
        fig_pack.pack_up()
        content = [fig_pack.fig, fig_pack.caption, fig_pack.label]
        return data.TexNamedEnv('subfigure', content, ['{}'])  # attribute string must present to provide an empty {}

    def process(self):
        '''
        Step 1:
            Find graphics, including includegraphics and other variants. If there is only one graphic, a regular figure
            will be used.
        Step 2:
            Associate includegraphics with the next non-ancestral label and caption nodes, group them into subfigures.
        Step 3:
            Any stray caption and label nodes will be assigned to the major label/caption properties. After this stage,
            if the major label/caption is still missing, a default one will be assigned.
        :return:
        '''
        self.iterate_node(self.soup, self.parent_width)
        self.flush()
        if len(self.fig_list) == 1:  # Only one figure
            pack = self.fig_list[0]
            if not pack.caption:
                pack.caption = self.caption if self.caption else self.default_caption
            if not pack.label:
                pack.label = self.label if self.label else self.default_label
            collect = [pack.fig, pack.caption, pack.label]
        else:
            collect = []
            for pack in self.fig_list:
                if pack.fig is None:  # stray labels
                    if pack.label:
                        if self.label:  # when multiple stray labels exist
                            self.label_replace_map[str(pack.label.string)] = str(self.label.string)
                        else:
                            self.label = pack.label
                    if pack.caption:  # TODO: consider combining them
                        self.caption = pack.caption

            # multiple images in one figure would only have the last subfigure absorbing the main caption (and label)
            if self.fig_list and (not self.caption and self.fig_list[-1].caption) or (
                    not self.label and self.fig_list[-1].label):
                self.caption = self.fig_list[-1].caption
                self.label = self.fig_list[-1].label
                self.fig_list[-1].caption = None
                self.fig_list[-1].label = None

            self.label = self.default_label if not self.label else self.label
            self.caption = self.default_caption if not self.caption else self.caption

            for pack in self.fig_list:
                if pack.fig:
                    collect.append(self.pack(pack))
            collect = collect + [self.caption, self.label]
        return data.TexNamedEnv('figure', collect)

    def flush(self):
        if not self.current.empty():
            self.fig_list.append(self.current)
        self.current = FigurePack(self.id)

    @staticmethod
    def clean_url(url):
        '''
        Clean up excessive { and } and " in urls
        resolve url
        :param url:
        :return:
        '''
        url = re.sub(r'(?<!\\)[{\"}]', "", url)
        url = os.path.normpath(url)
        return url

    def extract_psfig_argument(self, content):
        """
        extract filename and width string from \\psfig{content}. Other properties are discarded
        :param content: str "file=file.ps,width=5in,height=3in"
        :return: filename, width_string
        """
        assign_pairs = content.split(",")
        args = dict([pair.split("=") for pair in assign_pairs])
        filename = args.get("file")
        if not filename:
            filename = args.get("figure", "")
        if not filename:
            return "", ""
        filename = self.filename_matcher.search(filename)
        width_string = args.get("width", "")
        return filename, width_string

    def iterate_node(self, node, parent_width=1.0):
        # TODO: support subfigure, label and caption
        if not isinstance(node, data.TexNode):
            return []
        width = parent_width
        if node.expr.name in ('includegraphics', 'epsfbox', 'psfragfig', 'plotone'):
            if node.expr.name != 'includegraphics':
                node = TexSoup(r"\includegraphics{%s}" % list(node.contents)[0]).includegraphics
            for arg in node.args:
                if isinstance(arg, data.BracketGroup):
                    arg.string = regularize_width_string(arg.string, parent_width)
                else:  # BraceGroup, Image url
                    arg.string = self.clean_url(arg.string)  # normalize url to be a valid path
                    if self.filename_matcher is not None:
                        arg.string = self.filename_matcher.search(arg.string)  # find best matching image file from meta
            if self.current.fig:
                self.flush()
            self.current.fig = node
        elif node.expr.name in ('epsfig', 'psfig'):
            filename, width_string = self.extract_psfig_argument(list(node.contents)[0])
            if not filename:
                return []
            if width_string:
                width_string = "[%s]" % regularize_width_string(width_string, parent_width)
            # if there is no width information in psfig argument, width_string is empty.
            node = TexSoup(r"\includegraphics%s{%s}" % (width_string, filename)).includegraphics
            if self.current.fig:
                self.flush()
            self.current.fig = node
        elif node.expr.name == 'label':
            if self.current.label:
                self.flush()
            self.current.label = node
        elif node.expr.name in ('caption', 'subcaption'):
            node.expr.name = 'caption'
            if node.label:
                caption_node, label_node = split_caption_label(node)
                if self.current.caption or self.current.label:
                    self.flush()
                self.current.caption = regularize_caption_node(caption_node)
                self.current.label = label_node
            else:
                if self.current.caption:
                    self.flush()
                self.current.caption = node
            return []

        if node.expr.name == 'subfigure' and len(node.args) >= 1:
            width = get_width_value_from_string(node.args[0].string)
            if width is None:
                width = 1.0
            width *= parent_width
        if node.contents:
            for c in node.contents:
                self.iterate_node(c, width)

        if node.find('includegraphics'):  # Closing one figure
            self.flush()

    def repack(self):
        output = []
        meta_node = None
        for node in self.fig_list:
            if not node.fig:
                meta_node = node
            else:
                output.append(node)
        if meta_node:
            output = [meta_node] + output
        return output


class LatexTableProcessor:
    def __init__(self, soup):
        self.soup = soup
        self.label_replace_map = {}

    @staticmethod
    def clean_tabular_environment(node):
        text = "".join([str(x) for x in node.contents])
        text = text.replace(r"\\", " ")
        return text

    def regularize_table_label(self, node):
        # After Pandoc 2.10, label must be outside caption the same way as in a figure.
        if node.table:
            label_node = None
            for label in node.find_all('label'):
                if label_node:
                    self.label_replace_map[str(label.string)] = str(label_node.string)
                else:
                    label_node = label.copy()
                label.delete()
            if not node.table.caption:
                new_caption = data.TexNode(data.TexCmd('caption', args=[data.BraceGroup("No caption")]))
                node.table.insert(-1, new_caption)
            else:
                caption_node = regularize_caption_node(node.caption).copy()
                node.caption.delete()
                node.table.insert(-1, caption_node)
            if label_node is None:
                random_name = util.random_name()
                label_node = r"\label{%s}" % random_name
            node.table.insert(-1, label_node)
        return node

    @staticmethod
    def multicolumn_tag(content: str, col: int):
        return "<multicolumn,%s>" % col + content

    @staticmethod
    def regularize_preamble_string(content: str):
        """

        :param content: table preamble without {}, e.g. "c | ccc"
        :return: replace all unknown column type with "c" in content
        """
        i = 0
        tokens = MathParser(content).nodes
        result = []
        while i < len(tokens):
            if tokens[i]:
                if len(str(tokens[i])) == 1:
                    if str(tokens[i]) in "clrp|@Xmb>< ":
                        result.append(str(tokens[i]))
                    elif str(tokens[i]) == "*":  # keep shorthands such as l*{12}
                        result.append("*")
                        if i + 1 < len(tokens) and str(tokens[i + 1]).startswith('{'):
                            result.append(str(tokens[i + 1]))
                            i += 1
                    else:  # ignore anything defined in the form of X{}{} and replace with center
                        result.append('c')
                        while i + 1 < len(tokens) and str(tokens[i + 1]).startswith('{'):
                            i += 1
                else:
                    result.append(str(tokens[i]))
            i += 1
        return "".join(result)

    @staticmethod
    def iterate_node(node, depth):
        # TODO: Wait for TexSoup 0.3 for support {} as a TexNode to handle nested tabular within {}
        if isinstance(node, data.TexNode):
            # TODO: testing to be deprecated 20210724
            # TODO: Fails CI due to multirow not having children in replace
            # if node.expr.name == 'multirow':  # Ignore and extract content
            #     if len(node.args) == 3 or len(node.args) == 4:  # \multirow{2}{*}{} or \multirow{2}{*}[]{}
            #         node = replace_texnode_with(node, node.args[-1].string.replace(r"\\", ""))
            #         LatexTableProcessor.iterate_node(node, depth)
            #         return
            #     else:  # \multirow{2}*{}
            #         # TODO: fix this
            #         node.delete()
            #         return
            # Extract content and place in the first cell
            # if node.expr.name == 'multicolumn' and len(node.args) == 3:
            #     column_num = int(node.args[0].string) if node.args[0].string.isdigit() else 1
            #     node = replace_texnode_with(node, LatexTableProcessor.multicolumn_tag(node.args[2].string,
            #                                                                           column_num) + " &" * (
            #                                             column_num - 1))
            #     LatexTableProcessor.iterate_node(node, depth)
            #     return
            # TODO: end of testing to be deprecated
            # extract content
            if node.expr.name in ('resizebox', 'adjustbox', 'makebox', 'parbox', 'scalebox', 'setlength') and len(node.args) <= 3:
                node = replace_texnode_with(node, node.args[-1].string)
                LatexTableProcessor.iterate_node(node, depth)
                return
            elif node.expr.name in ('centerline', 'reflectbox', 'texttt', 'tablehead', 'colhead', 'mbox', 'centering') and len(node.args) == 1:
                node = replace_texnode_with(node, node.args[0].string)
                LatexTableProcessor.iterate_node(node, depth)
                return
            elif node.expr.name in ('rule', 'cline'):
                node.delete()
                return
            elif node.expr.name in ('label', 'caption') and len(
                    node.args) > 1:  # assuming only one valid arg, separate the remaining arguments
                for i, arg in enumerate(node.args):
                    if isinstance(arg, data.BraceGroup):
                        node.args = data.TexArgs([node.args[i]])
                        extra_string = " ".join(arg.string for arg in node.args[i:])
                        node = replace_texnode_with(node, str(node) + ' ' + extra_string)
                        LatexTableProcessor.iterate_node(node, depth)
                        return

            elif node.expr.name == 'tabular' and len(node.args) >= 1:
                node.args = data.TexArgs(
                    [data.BraceGroup(LatexTableProcessor.regularize_preamble_string(node.args[-1].string))]
                )
            elif node.expr.name == 'tabularx' and len(node.args) >= 2:
                node.args = data.TexArgs(
                    [data.BraceGroup(LatexTableProcessor.regularize_preamble_string(node.args[-1].string))]
                )
            # elif len(node.args) >= 1 and "\\\\" in node.args[-1].string:
            #     node = replace_texnode_with(node, node.args[-1].string.replace("\\\\", " "))
            #     LatexTableProcessor.iterate_node(node, depth)
            #     return

            # process tabular
            if node.expr.name in ('tabular', 'tabularx', 'tabu'):
                node.expr.name = 'tabular'
                depth += 1
                if depth != 1:  # Clean up the content
                    node.args = data.TexArgs()
                    node = replace_texnode_with(node, LatexTableProcessor.clean_tabular_environment(node))

            # iterate
            if node.contents:
                for c in node.contents:
                    LatexTableProcessor.iterate_node(c, depth)

    def process(self):
        self.iterate_node(self.soup, 0)
        self.soup = self.regularize_table_label(self.soup)
        return self.soup


class LatexDeluxeTableProcessor:
    def __init__(self, soup):
        self.soup = soup

    @staticmethod
    def clean_tabular_environment(node):
        text = "".join([str(x) for x in node.contents])
        text = text.replace(r"\\", " ")
        return text

    @staticmethod
    def regularize_table_label(node):
        # After Pandoc 2.10, label must be outside caption the same way as in a figure.
        if node.table:
            if not node.table.caption:
                new_caption = data.TexNode(data.TexCmd('caption', args=[data.BraceGroup("No caption")]))
                node.table.insert(-1, new_caption)
            else:
                if node.caption.label:
                    node.caption, label_node = split_caption_label(node.caption)
                else:
                    if node.label:
                        label_node = node.label.copy()
                        node.label.delete()
                    else:
                        random_name = util.random_name()
                        label_node = r"\label{%s}" % random_name
                caption_node = regularize_caption_node(node.caption).copy()
                node.caption.delete()
                node.table.insert(-1, caption_node)
                node.table.insert(-1, label_node)
        return node

    @staticmethod
    def regularize_preamble_string(content: str):
        """

        :param content: table preamble without {}, e.g. "c | ccc"
        :return: replace all unknown column type with "c" in content
        """
        i = 0
        tokens = MathParser(content).nodes
        print(tokens)
        result = []
        while i < len(tokens):
            if tokens[i]:
                if len(str(tokens[i])) == 1:
                    if str(tokens[i]) in "clrp|@Xmb>< ":
                        result.append(str(tokens[i]))
                    elif str(tokens[i]) == "*":  # l*{12}
                        result.append("*")
                        while i + 1 < len(tokens) and str(tokens[i + 1]).startswith('{'):
                            result.append(tokens[i + 1])
                            i += 1
                    else:  # ignore anything defined in the form of X{}{} and replace with center
                        result.append('c')
                        while i + 1 < len(tokens) and str(tokens[i + 1]).startswith('{'):
                            i += 1
                else:
                    result.append(str(tokens[i]))
            i += 1
        return "".join(result)

    @staticmethod
    def iterate_node(node, depth):
        # TODO: Wait for TexSoup 0.3 for support {} as a TexNode to handle nested tabular within {}
        if isinstance(node, data.TexNode):
            # TODO: testing to be deprecated 20210724
            # TODO: Fails CI due to multirow not having children in replace
            if node.expr.name == 'multirow':  # Ignore and extract content
                if len(node.args) == 3 or len(node.args) == 4:  # \multirow{2}{*}{} or \multirow{2}{*}[]{}
                    node = replace_texnode_with(node, node.args[-1].string.replace(r"\\", ""))
                    LatexTableProcessor.iterate_node(node, depth)
                    return
                else:  # \multirow{2}*{}
                    # TODO: fix this
                    node.delete()
                    return
            # Extract content and place in the first cell
            if node.expr.name == 'multicolumn' and len(node.args) == 3:
                column_num = int(node.args[0].string) if node.args[0].string.isdigit() else 1
                node = replace_texnode_with(node, node.args[2].string + " &" * (column_num - 1))
                LatexTableProcessor.iterate_node(node, depth)
                return
            # TODO: end of testing to be deprecated
            # extract content
            if node.expr.name == 'makebox' and len(node.args) <= 3:
                node = replace_texnode_with(node, node.args[-1].string)
                LatexTableProcessor.iterate_node(node, depth)
                return
            if node.expr.name == 'resizebox' and len(node.args) == 3:
                node = replace_texnode_with(node, node.args[2].string)
                LatexTableProcessor.iterate_node(node, depth)
                return
            if node.expr.name in ('parbox', 'scalebox') and len(node.args) in (2, 3):
                node = replace_texnode_with(node, node.args[-1].string)
                LatexTableProcessor.iterate_node(node, depth)
                return
            if node.expr.name in ('centerline', 'reflectbox', 'texttt', 'tablehead', 'colhead') and len(node.args) == 1:
                node = replace_texnode_with(node, node.args[0].string)
                LatexTableProcessor.iterate_node(node, depth)
                return
            if node.expr.name == 'rule':
                node.delete()
                return
            if node.expr.name in ('label', 'caption') and len(
                    node.args) > 1:  # assuming only one valid arg, separate the remaining arguments
                for i, arg in enumerate(node.args):
                    if isinstance(arg, data.BraceGroup):
                        node.args = data.TexArgs([node.args[i]])
                        extra_string = " ".join(arg.string for arg in node.args[i:])
                        node = replace_texnode_with(node, str(node) + ' ' + extra_string)
                        LatexTableProcessor.iterate_node(node, depth)
                        return

            if node.expr.name == 'tabular' and len(node.args) >= 1:
                node.args = data.TexArgs(
                    [data.BraceGroup(LatexTableProcessor.regularize_preamble_string(node.args[-1].string))]
                )
            if node.expr.name == 'tabularx' and len(node.args) >= 2:
                node.args = data.TexArgs(
                    [data.BraceGroup(LatexTableProcessor.regularize_preamble_string(node.args[-1].string))]
                )
            if node.expr.name in ('tabular', 'tabularx', 'tabu'):
                node.expr.name = 'tabular'
                depth += 1
                if depth != 1:  # Clean up the content
                    node.args = data.TexArgs()
                    node = replace_texnode_with(node, LatexTableProcessor.clean_tabular_environment(node))
            if node.expr.name == 'makecell' and len(node.args) >= 1:
                node = replace_texnode_with(node, node.args[-1].string.replace("\\\\", ""))
                LatexTableProcessor.iterate_node(node, depth)
                return
            if node.contents:
                for c in node.contents:
                    LatexTableProcessor.iterate_node(c, depth)

    def process(self):
        self.iterate_node(self.soup, 0)
        self.soup = self.regularize_table_label(self.soup)
        return self.soup


class FilenameMatcher:
    def __init__(self, meta):
        self.meta = meta
        self.ext = '.png'
        self.rootname_dict = {}  # name
        self.rootname_casefree_dict = {}
        self.path_dict = {}  # dir/name
        self.path_casefree_dict = {}
        self.load_files()

    def load_files(self):
        for path in self.meta.image_files:
            dirname, basename = os.path.split(path)
            rootname, extension = os.path.splitext(basename)
            extension = extension.lower()
            if extension in util.IMAGE_EXTENSIONS:
                value = os.path.join(dirname, rootname)
                self.rootname_dict[rootname] = value
                self.rootname_casefree_dict[rootname.lower()] = value
                self.path_dict[value] = value
                self.path_casefree_dict[value.lower()] = value
            else:
                value = path
                self.rootname_dict[basename] = value
                self.rootname_casefree_dict[basename.lower()] = value
                key = os.path.normpath(path)
                self.path_dict[key] = value
                self.path_casefree_dict[key.lower()] = value

    def search(self, path):
        dirname, basename = os.path.split(path)
        if not dirname:
            dirname = '.'
        rootname, extension = os.path.splitext(basename)

        #  url = a.b, file = a.b.eps, looking for a.b.png
        #  url = ab, file = ab.eps, looking for ab.png
        #  url = a.b, file = a.b, looking for a.b.png
        key = os.path.normpath(os.path.join(dirname, basename))
        if key in self.path_dict:
            return self.path_dict[key] + self.ext
        elif key.lower() in self.path_casefree_dict:
            return self.path_casefree_dict[key.lower()] + self.ext

        # url = a.eps, file = a.eps, looking for a.png
        key = os.path.normpath(os.path.join(dirname, rootname))
        if key in self.path_dict:
            return self.path_dict[key] + self.ext
        elif key.lower() in self.path_casefree_dict:
            return self.path_casefree_dict[key.lower()] + self.ext

        # url = a.eps, file = fig/a.eps, looking for fig/a.png
        if basename in self.rootname_dict:
            return self.rootname_dict[basename] + self.ext
        elif basename.lower() in self.rootname_casefree_dict:
            return self.rootname_casefree_dict[basename.lower()] + self.ext
        elif rootname in self.rootname_dict:
            return self.rootname_dict[rootname] + self.ext
        elif rootname.lower() in self.rootname_casefree_dict:
            return self.rootname_casefree_dict[rootname.lower()] + self.ext

        # found nothing, try removing any mention of .pdf or .eps to avoid embedding a pdf file
        return path.replace(".pdf", self.ext).replace(".eps", self.ext)


if __name__ == '__main__':
    import TexSoup as tx

    print(tx.__version__)
