import unittest
from app.convert import arxiv

class ArxivTest(unittest.TestCase):
    def test_query_page(self):
        for entry_dict in arxiv.query_page(search_query="nice"):
            print(entry_dict)


if __name__ == '__main__':
    unittest.main()