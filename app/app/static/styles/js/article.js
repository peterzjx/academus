//Formatting all tables
function addSroll() {
$('table').parent().addClass('table-scroll');  
}
addSroll();

//Formatting image heights
$(document).ready(function() {
  var imagesLoaded = 0;
  var totalImages = $('img').length;
  $('img').each(function(idx, img) {
    $('<img>').on('load', imageLoaded).attr('src', $(img).attr('src'));
  });
  function imageLoaded() {
    imagesLoaded++;
    if (imagesLoaded == totalImages) {
      allImagesLoaded();
    }
  }

  function allImagesLoaded() {
  var allimages = $('.subfigures img');
  for (var i=0; i<allimages.length; i++) {
      if (allimages[i].clientHeight>350) {
         allimages[i].classList.add("imgcss-placeholder");
         if ($(window).width()<500) {
          allimages[i].style.width= "60%";  
         } else {
             allimages[i].style.width= "50%";
        }
      }
  }
} 
imageLoaded();
});

//Modal
var modalbody = document.querySelector('.modal-body');
function showImage(event) {
    modalbody.innerHTML='';
    var id = event.href.match(/#.*/)[0].replace('#','');
    console.log('id is '+id);
    var subfigure = document.getElementById(id);
    var images = subfigure.querySelector('p').innerHTML;
    modalbody.insertAdjacentHTML('afterbegin',images);   
}