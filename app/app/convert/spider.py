import codecs
import logging
import os
import tarfile
import threading
import gzip
from tarfile import ReadError
from urllib.request import urlopen, HTTPError
from socket import error as SocketError

from app.convert import arxiv, interface
from app import database
from app import url
from app import util

# experimental use only
def download_pdf(article_name):
    target_folder = os.path.join(util.ARTICLE_PATH, 'nonlatexpdf')
    if not os.path.exists(target_folder):
        os.makedirs(target_folder)
    pdf_url = 'https://arxiv.org/pdf/%s.pdf' % url.article_name_convention(article_name, 'url')
    with urlopen(pdf_url) as response, open(os.path.join(target_folder, article_name) + '.pdf', 'wb') as out_file:
        data = response.read()
        out_file.write(data)

def download_tar(tar_url, folder):
    ftpstream = urlopen(tar_url)
    with tarfile.open(fileobj=ftpstream, mode="r|gz") as tempfile:
        tempfile.extractall(path=folder)

def download_gzip(target_url, folder):
    ftpstream = urlopen(target_url)
    with open(os.path.join(folder, 'main.tex'), 'wb') as out_file:
        out_file.write(gzip.decompress(ftpstream.read()))

def download_source(article_name):
    logging.info("Downloading article")
    page_info = {}
    # TODO: separate this part
    is_legacy_name = False
    article_folder = ""
    target_url = ""
    page_thread = threading.Thread(target=arxiv.arxiv_page, args=(url.article_name_convention(article_name, 'display'), page_info))
    try:
        page_thread.start()
        if not os.path.exists(util.ARTICLE_PATH):
            os.makedirs(util.ARTICLE_PATH)
        # TODO: support legacy pure tex file download
        article_folder = util.get_path(article_name)  # make dir in local storage location
        if not os.path.exists(article_folder):
            os.makedirs(article_folder)
        database.ArticleStatusQuery(article_name).set(database.ArticleStatus.DOWNLOADING)

        # download the tar.gz file, extract
        target_url = util.ARXIV_ARCHIVE_URL + url.article_name_convention(article_name, 'display')  # source file url
        is_legacy_name = '__' in article_name
        if is_legacy_name:
            download_gzip(target_url, article_folder)
        else:
            download_tar(target_url, article_folder)
    except HTTPError:  # HTTPError should be above SocketError as "404 not found" will be captured by both
        os.rmdir(article_folder)
        raise interface.ArticleNotFoundError("The article id does not exist. Please check again with the input.")
    except SocketError as e:
        raise interface.ArticleConnectionError("Connection error %s" % e.errno)
    except ReadError as e:
        if str(e) == "invalid header" and not is_legacy_name:  # non-legacy articles could only have one file, especially math papers
            download_gzip(target_url, article_folder)
        else:
            raise interface.NotInLatexError('https://arxiv.org/pdf/%s.pdf' % article_name)
    finally:
        page_thread.join()
        generate_meta(article_name, page_info)

def generate_meta(article_name, page_info):
    article_folder = util.get_path(article_name)
    '''Pre-processing files, generating meta file'''
    tex_files = []
    image_files = []
    main_files = []
    bib_files = []
    bbl_files = []
    for (path, _, files) in os.walk(article_folder):
        for file in sorted(files):
            filename, file_extension = os.path.splitext(file)
            file_extension = file_extension.lower()
            filepath = os.path.join(path, file)
            relative_path = os.path.relpath(path, article_folder)
            relative_filename = os.path.normpath(os.path.join(relative_path, file))
            if file_extension == '.tex':
                if relative_filename.lower() in ('ms.tex', 'main.tex'):  # arxiv processes ms.tex first, but main.tex is unofficially popular
                    tex_files.insert(0, relative_filename)
                else:
                    tex_files.append(relative_filename)
            elif file_extension == '.bib':
                bib_files.append(relative_filename)
            elif file_extension == '.bbl':
                bbl_files.append(relative_filename)
            elif file_extension in util.IMAGE_EXTENSIONS:
                image_files.append(relative_filename)
            elif file_extension in util.DELETE_EXTENSIONS:
                logging.warning("Removing file %s" % file)
                os.rename(filepath, filepath + '.bak')
    if len(tex_files) == 1:
        main_files.append(tex_files[0])
    # elif len(tex_files) == 0:
    #     raise interface.NotInLatexError('https://arxiv.org/pdf/%s.pdf' % article_name)
    else:
        for relative_filepath in tex_files:  # arxiv defaults using lexically smallest name as the main tex
            filepath = os.path.join(article_folder, relative_filepath)
            with codecs.open(filepath, 'rU', encoding="utf-8", errors='ignore') as f:
                '''Find the main tex file as the entrance, currently only supporting with 1 main file'''
                # TODO: use heuristics to find the main file
                # TODO: possible hint: filename, title, includegraphics, author name, bibliography
                if not main_files and f.read().strip().find(
                        r'\begin{document}') >= 0:  # TODO: not compatible with documented subfile usage
                    main_files.append(relative_filepath)
        # if not main_files:
        #     database.ArticleStatusQuery(article_name).set(database.ArticleStatus.FAILED, database.ArticleErrorInfo.FILENOTFOUND)
        #     raise FileNotFoundError("Can't find the main tex file.")
    interface.ArticleMeta(article_name=article_name,
                     article_folder=article_folder,
                     main_files=main_files,
                     tex_files=tex_files,
                     image_files=image_files,
                     meta_version=util.current_meta_version(),
                     bib_files=bib_files,
                     bbl_files=bbl_files,
                     title=page_info.get('title'),
                     authors=page_info.get('author_name'),
                     update_time="19700101000000" if page_info.get('update_time') is None else page_info['update_time'].strftime("%Y%m%d%H%M%S"),
                     subject=page_info.get('subject')
                     ).store()

def generate_mock_meta(article_name):
    generate_meta(article_name, {})

if __name__ == "__main__":
    # arxiv_id = input()  # 17xx.xxxxx
    pass
